#!/bin/env python

from array import array
import os
import sys

file_to_open = open('binning/Binning.txt', 'r')
lines_to_read = file_to_open.readlines()

# List of regions we want to check to derive a common binning
list_of_regions = ['4jex3bex','5jex3bex','6jex3bex']

def get_edges(region): 
    list_of_elements = []
    size = 0
    for line in lines_to_read:
        region_found=line.split(':')[0]
        if region_found == region:
            line = line.replace('|','-').replace('\n','').replace(':','-').replace(' ','')
            elements = line.split('-')
            list_of_elements.append(elements)
            # assumes all elements have same lenght 
            size = len(elements)
    return list_of_elements,size

def derive_binning(edges,size):
    bins = []
    count = 0 
    # Exclude first element of the list (region name label)
    # Loop over number of entries in each edge
    for count in range(1,size-1):
        bin_tmp = -1
        counter_elements = 0        
        # Loop over edges
        for counter_elements in range(0,len(edges)): 
            # If it's first time we check for the bins, we know what to take, last element of any of the edges
            if count == 1 and bin_tmp == -1:
                bin_tmp = edges[counter_elements][size-1]
                print 'Assigning first bin '+str(bin_tmp)            
            if count != 1:
                if counter_elements == 0 :
                    bin_tmp = edges[counter_elements][count]
                elif counter_elements != 0 :
                    bin_tmp = edges[counter_elements][count] 
                    bin_tmp_prev = edges[counter_elements-1][count] 
                    if bin_tmp < bin_tmp_prev:
                        bin_tmp = bin_tmp_prev
 
            counter_elements=counter_elements+1
        bins.append(bin_tmp)
        count = count+1
        
    # Adjust ordering 
    bins.append(bins.pop(bins.index('1.000000')))
    print bins


# Run the actual code
for region in list_of_regions:
    list_of_elements,size = get_edges(region)
    derive_binning(list_of_elements,size)


