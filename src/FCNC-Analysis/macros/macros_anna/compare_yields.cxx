void compare_yields(TFile *file1, TFile *file2, TFile *file3, TFile *file4,TFile *file5,TFile *file6,TFile *file7,TFile *file8)
{

    gStyle->SetOptStat("");
    gStyle->Reset();
    SetAtlasStyle();
    gStyle->SetErrorX(0.5);
	const int cats = 4;
	const int col[12]={kRed,kBlue,kBlack,kGreen-2,kMagenta+2,kCyan+2,kOrange+10,kViolet-1,kPink-7,kGreen-6,kAzure+10,kPink-9};
	const char fnames[cats][200] = {"4jex3bex", "5jex3bex","6jin3bex","6jin4bin"};
	TCanvas *cv[cats];
	TPad              *pad1[cats];
 	TPad              *pad2[cats];
 	TLegend           *leg[cats];
 	TLegend           *legA[cats];

	//nominal
	TH1D *bins_4jex3bex_1  = (TH1D*)file1->Get("h_tot_postFit"); //no jet
	TH1D *bins_5jex3bex_1  = (TH1D*)file2->Get("h_tot_postFit"); //no jet
	TH1D *bins_6jin3bex_1  = (TH1D*)file3->Get("h_tot_postFit"); //no jet
	TH1D *bins_6jin4bin_1  = (TH1D*)file4->Get("h_tot_postFit"); //no jet
	//without 2b loose
	TH1D *bins_4jex3bex_2  = (TH1D*)file5->Get("h_tot_postFit"); //no jet
	TH1D *bins_5jex3bex_2  = (TH1D*)file6->Get("h_tot_postFit"); //no jet
	TH1D *bins_6jin3bex_2  = (TH1D*)file7->Get("h_tot_postFit"); //no jet
	TH1D *bins_6jin4bin_2  = (TH1D*)file8->Get("h_tot_postFit"); //no jet


	TH1D *histos_nominal[cats]   = {bins_4jex3bex_1,bins_5jex3bex_1,bins_6jin3bex_1,bins_6jin4bin_1};
	TH1D *histos_no2blos[cats]   = {bins_4jex3bex_2,bins_5jex3bex_2,bins_6jin3bex_2,bins_6jin4bin_2};
	TH1D *ratio[cats];


	for(int i = 0; i < cats; i++){
		cv[i] = new TCanvas(Form("Post-Fit: nominal vs no2bloose %d",i),Form("Fost-Fit: nominal vs no2bloose %d",i),600,600);
		cv[i]->cd();
		pad1[i] = new TPad(Form("pad1_%d",i), Form("pad1_%d",i), 0, 0.3, 1, 1.0);
 		pad1[i]->SetBottomMargin(0.01); // Upper and lower plot are joined
 		pad1[i]->SetLeftMargin(0.15);
 		pad1[i]->Range(-3.454024,-657.2018,3.135956,3683.141);
 		pad1[i]->SetLeftMargin(0.15);
 		pad1[i]->Draw();             // Draw the upper pad: pad1
 		pad1[i]->cd();


 		histos_nominal[i]->SetLineColor(col[0]);
 		histos_nominal[i]->SetMarkerColor(col[0]);
 		histos_nominal[i]->SetTitle(";NN score;Events");
 		histos_nominal[i]->GetYaxis()->SetRangeUser(0.,6500.);
 		histos_nominal[i]->Draw("e0");

 		histos_no2blos[i]->SetLineColor(col[1]);
 		histos_no2blos[i]->SetMarkerColor(col[1]);
 		histos_no2blos[i]->Draw("same");

		leg[i] = new TLegend(0.768,0.766,0.867,0.883);
 		leg[i]->SetBorderSize(0);
 		leg[i]->SetFillStyle(0);
 		leg[i]->SetTextSize(0.04);
    	leg[i]->SetTextFont(42);
    	leg[i]->AddEntry(histos_nominal[i],  "Nominal","pl");
    	leg[i]->AddEntry(histos_no2blos[i],  "no 2bloose","pl");
 		leg[i]->Draw();


 		legA[i] = new TLegend(0.563,0.7366,0.714,0.915);
 		legA[i]->SetBorderSize(0);
 		legA[i]->SetTextSize(0.04);
 		legA[i]->SetFillStyle(0);
 		legA[i]->SetTextAlign(32);
 		legA[i]->SetTextFont(42);

 		legA[i]->AddEntry((TObject*)0,"#bf{#it{ATLAS}} Internal","");
 		legA[i]->AddEntry((TObject*)0,"#sqrt{s} = 13 TeV","");
 		legA[i]->AddEntry((TObject*)0,Form("%s",fnames[i]),"");
 		legA[i]->Draw();

		cv[i]->cd();

 		pad2[i] = new TPad(Form("pad2_%d",i), Form("pad2_%d",i), 0, 0, 1, 0.31); //Bottom
		pad2[i]->SetTopMargin(0.); // Upper and lower plot are joined
 		pad2[i]->SetTopMargin(0);
 		pad2[i]->SetLeftMargin(0.15);
 		pad2[i]->SetBottomMargin(0.4);
 		pad2[i]->Range(-3.458,0.7273355,3.128667,1.08939);
 		pad2[i]->Draw();
 		pad2[i]->cd();       // pad2 becomes the current pad

        ratio[i] = (TH1D*)histos_nominal[i]->Clone(Form("ratio_%d",i));
        ratio[i]->Divide(histos_nominal[i],histos_no2blos[i],1.,1.,"B");
 		ratio[i]->SetLineColor(col[1]);
 		ratio[i]->SetMarkerColor(col[1]);
 		ratio[i]->Draw("Pl");


    	ratio[i]->GetYaxis()->SetTitle("Nom. / Alt. ");
 		ratio[i]->GetYaxis()->SetNdivisions(505);
   		ratio[i]->GetYaxis()->SetTitleSize(20);
   		ratio[i]->GetYaxis()->SetTitleFont(43);
   		ratio[i]->GetYaxis()->SetTitleOffset(2);
   		ratio[i]->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
   		ratio[i]->GetYaxis()->SetLabelSize(20);
   		ratio[i]->GetYaxis()->SetLabelOffset(0.02);
   		ratio[i]->GetYaxis()->SetRangeUser(0.95,1.05);

   		// X axis ratio plot settings
   		//ratio[i]->GetXaxis()->SetTitle("#eta ");
   		ratio[i]->GetXaxis()->SetTitleSize(20);
   		ratio[i]->GetXaxis()->SetTitleFont(43);
   		ratio[i]->GetXaxis()->SetTitleOffset(5.);
   		ratio[i]->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
   		ratio[i]->GetXaxis()->SetLabelSize(20);
   		ratio[i]->GetXaxis()->SetLabelOffset(0.06);

   		cv[i]->SaveAs(Form("%s.pdf",fnames[i]));

	}//end of for loop


}
