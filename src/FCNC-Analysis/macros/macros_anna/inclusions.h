#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>

#include "TROOT.h"
#include "TGraph.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TRandom3.h"

#include "TClonesArray.h"
#include "TParticle.h"
#include "TGenerator.h"
//#include "TPythia8.h"
#include "TDatabasePDG.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"
#include "TEfficiency.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TRefArray.h"

#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"
#include "TColor.h"
#include "TLine.h"
#include "TPaletteAxis.h"
#include "TPolyLine.h"



#include "AtlasLabels.C"
#include "AtlasStyle.C"

