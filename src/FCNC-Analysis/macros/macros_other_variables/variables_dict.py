#!/bin/python


oneLep3b = {'name':"c1lep5jin3bin",'legend':"1l, #geq5j, #geq3b"}
oneLep2b = {'name':"c1lep5jin2bin",'legend':"1l, #geq5j, #geq2b"}
oneLep6j3b = {'name':"c1lep6jin3bin",'legend':"1l, #geq6j, #geq3b"}
oneLep6j2b = {'name':"c1lep6jin2bin",'legend':"1l, #geq6j, #geq2b"}

oneLep6j3b_e = {'name':"c1lep6jin3bin_el",'legend':"1e, 0#mu, #geq6j, #geq3b"}
oneLep6j3b_mu = {'name':"c1lep6jin3bin_mu",'legend':"1#mu, 0e, #geq6j, #geq3b"}
oneLep5j3b_e = {'name':"c1lep5jin3bin_el",'legend':"1e, 0#mu, #geq5j, #geq3b"}
oneLep5j3b_mu = {'name':"c1lep5jin3bin_mu",'legend':"1#mu, 0e, #geq5j, #geq3b"}

zeroLep3b = {'name':"c0lep6jin3bin",'legend':"0l, #geq6j, #geq3b"}
zeroLep2b = {'name':"c0lep6jin2bin",'legend':"0l, #geq6j, #geq2b"}
zeroLep3b7j = {'name':"c0lep7jin3bin",'legend':"0l, #geq7j, #geq3b"}
zeroLep2b7j = {'name':"c0lep7jin2bin",'legend':"0l, #geq7j, #geq2b"}

#regions = [oneLep2b, zeroLep2b, oneLep3b, zeroLep3b, oneLep6j3b, oneLep6j2b, zeroLep3b7j, zeroLep2b7j]


sum_oneLep3b = {'name':"sum1lep5jin3bin",'legend':"1l, #geq5j, #geq3b"}
sum_oneLep2b = {'name':"sum1lep5jin2bin",'legend':"1l, #geq5j, #geq2b"}
sum_oneLep6j3b = {'name':"sum1lep6jin3bin",'legend':"1l, #geq6j, #geq3b"}
sum_oneLep6j2b = {'name':"sum1lep6jin2bin",'legend':"1l, #geq6j, #geq2b"}

sum_zeroLep3b = {'name':"sum0lep6jin3bin",'legend':"0l, #geq6j, #geq3b"}
sum_zeroLep2b = {'name':"sum0lep6jin2bin",'legend':"0l, #geq6j, #geq2b"}
sum_zeroLep3b7j = {'name':"sum0lep7jin3bin",'legend':"0l, #geq7j, #geq3b"}
sum_zeroLep2b7j = {'name':"sum0lep7jin2bin",'legend':"0l, #geq7j, #geq2b"}

regions = [sum_oneLep2b, sum_zeroLep2b, sum_oneLep3b, sum_zeroLep3b, sum_oneLep6j3b, sum_oneLep6j2b, sum_zeroLep3b7j, sum_zeroLep2b7j]


jets_n_1l       = {'name':"jets_n",'legend':"Jet multiplicity",'binning':"4.5,5.5,6.5,7.5,8.5,9.5,10.5"}
bjets_n         = {'name':"bjets_n",'legend':"b-jet multiplicity",'binning':"1.5,2.5,3.5,4.5,5.5"}
meff            = {'name':"meff",'legend':"m_{eff} [GeV]",'binning':"1000,1200,1400,1600,2000,2500,3500",'width':"200"}
met             = {'name':"met_zoom",'legend':"E_{T}^{miss} [GeV]"}
jet0pt          = {'name':"jet0_pt",'legend':"Leading jet p_{T} [GeV]",'binning':"50,100,150,200,250,300,350,400,450,500,550,600,700,800,1000",'width':"50"}
higgs_n         = {'name':"RCMHiggs_jets_n",'legend':"Higgs-tagged jets multiplicity",'binning':"-0.5,0.5,1.5,2.5,3.5"}
top_n           = {'name':"RCMTop_jets_n",'legend':"Top-tagged jets multiplicity",'binning':"-0.5,0.5,1.5,2.5,3.5"}
mtbmin          = {'name':"mtbmin_zoom",'legend':"m_{T,min}^{b} [GeV]"}
RCjets_m        = {'name':"RCjet0_m",'legend':"Leading RC jet mass [GeV]",'binning':"50,70,90,110,130,150,170,190,210,240,260,300",'width':"20"}
RCjets_pt       = {'name':"RCjets_pt",'legend':"Leading RC jet p_{T} [GeV]"}
lep0_pt         = {'name':"lep0_pt_zoom",'legend':"Lepton p_{T} [GeV]"}
lep0_eta         = {'name':"lep0_eta",'legend':"Lepton #eta",'binning':"-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1.0,1.5,2.0,2.5,3.0"}
mtw_zoom         = {'name':"mtw_zoom",'legend':"m_{T}(W) [GeV]"}

list_variables = []
list_variables += [jets_n_1l]
list_variables += [bjets_n]
list_variables += [meff]
list_variables += [met]
list_variables += [jet0pt]
list_variables += [higgs_n]
list_variables += [top_n]
list_variables += [mtbmin]
list_variables += [RCjets_m]
list_variables += [RCjets_pt]
list_variables += [lep0_pt]
list_variables += [lep0_eta]
list_variables += [mtw_zoom]

# jet0_btagw       = {'name':"jet0_btagw",'legend':"Leading MV2c10 jet MV2c10 score", 'binning':"-1,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0"}
# jet1_btagw       = {'name':"jet1_btagw",'legend':"2nd MV2c10 jet MV2c10 score", 'binning':"-1,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0"}
# jet2_btagw       = {'name':"jet2_btagw",'legend':"2nd MV2c10 jet MV2c10 score", 'binning':"-1,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0"}
# jet3_btagw       = {'name':"jet3_btagw",'legend':"2nd MV2c10 jet MV2c10 score", 'binning':"-1,-0.8,-0.6,-0.4,-0.2,0.0,0.2,0.4,0.6,0.8,1.0"}
# list_variables = []
# list_variables += [jet0_btagw]
# list_variables += [jet1_btagw]
# list_variables += [jet2_btagw]
# list_variables += [jet3_btagw]
