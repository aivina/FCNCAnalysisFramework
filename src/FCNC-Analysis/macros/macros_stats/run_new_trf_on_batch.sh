#!/bin/bash

#Add here your directory
output_directory="/nfs/at3/scratch2/orlando/fit_results"
#Add here your directory 
config_file_dir="/nfs/pic.es/user/o/orlando/config_file_trex_fitter/fcnc_Hbb_full_run2/fit_conf_files/v10-mar-2020"
tar_path=/nfs/at3/scratch2/farooque/TRexFitterArea/TRExFitter/TtHFitter-00-04-06-master.tar.gz
fit_actions=preplotsfitpostplots

config="${config_file_dir}/configFile_"

python LaunchTRExFitterOnBatchNew.py tarball=${tar_path} action=${fit_actions} inputDir=${config} outputDir=${output_directory} 


