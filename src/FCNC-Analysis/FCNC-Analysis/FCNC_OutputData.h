#ifndef FCNC_OUTPUTDATA_H
#define FCNC_OUTPUTDATA_H

#include "IFAETopFramework/OutputData.h"
#include <vector>
#include <map>

class TriggerInfo;
class AnalysisObject;
class FCNC_Options;

class FCNC_OutputData : public OutputData {

public:

    //
    // Standard C++ functions
    //
    FCNC_OutputData( FCNC_Options* opt );
    FCNC_OutputData( const FCNC_OutputData & );
    ~FCNC_OutputData();

    //
    // Class specific functions
    //
    virtual void ClearOutputData();

private:
    FCNC_Options* m_opt;

public:

    //
    // Declare here the output variables of your code. The naming convention
    // is o_*
    //

    //List of triggers to use. Remains constant for all events
    std::vector<TriggerInfo*> o_trigger_list;

    //
    // Event variables
    //
    int o_channel_type;
    int o_period;
    int o_run_number;
    double o_pileup_mu;
    int o_npv;
    double o_meff;
    double o_met;
    double o_mtwl;
    double o_ptwl;
    double o_hthad;
    double o_met_sig;
    unsigned long long int o_event_number;
    std::vector < int > *o_region;
    
    //
    // Jet-related quantities
    //
    int o_jets_n;
    int o_bjets_n;
    int o_fjets_n;

    std::vector< AnalysisObject* >* o_jets;
    std::vector< AnalysisObject* >* o_bjets;
    std::vector< AnalysisObject* >* o_fjets;
    std::vector< AnalysisObject* >* o_ljets;

    std::vector< AnalysisObject* >* o_bjets_lowb_3b;
    std::vector< AnalysisObject* >* o_bjets_lowb_4b;

    std::vector< AnalysisObject* >* o_jets_btagworder_4j;
    //
    // Lepton variables
    //
    int o_el_n;
    int o_mu_n;
    int o_lep_n;
    int o_lepForVeto_n;
    int o_el_loose_n;
    int o_mu_loose_n;
    int o_lep_loose_n;
    std::vector< AnalysisObject* >* o_el;
    std::vector< AnalysisObject* >* o_mu;
    std::vector< AnalysisObject* >* o_lep;
    AnalysisObject *o_selLep;
    AnalysisObject *o_AO_met;

    //
    // Other kinematic variables
    //
    double o_dRmin_ejets;
    double o_dRmin_mujets;
    double o_dRmin_jetjet;
    double o_dRmin_bjetbjet;
    double o_dRmin_bjetbjet_lowb_3b;
    double o_dRmin_bjetbjet_lowb_4b;
    double o_dR_TTL_bjets;
    double o_dR_TTT_bjets;
    double o_dR_TTLooser_bjets;
    double o_mbb_mindR;
    double o_mbb_mindR_lowb_3b;
    double o_mbb_mindR_lowb_4b;

    double o_dPhi_lepmet;
    double o_dPhi_jetmet;
    double o_dPhi_jetmet5;
    double o_dPhi_jetmet6;
    double o_dPhi_jetmet7;
    double o_dPhi_lepjet;
    double o_dPhi_lepbjet;
    double o_dRmin_ebjets;
    double o_dRmin_mubjets;
    double o_mTbmin;
    double o_mTbmin_lowb_3b;
    double o_mTbmin_lowb_4b;

    int o_jets40_n;
    double o_centrality;
    double o_mbb_leading_bjets;
    double o_mbb_softest_bjets;
    double o_J_lepton_invariant_mass;
    double o_J_leadingb_invariant_mass;
    double o_J_J_invariant_mass;
    double o_dRaverage_bjetbjet;
    double o_dRaverage_jetjet;

    double o_mbb_maxdR;
    double o_dPhibb_leading_bjets;
    double o_dPhibb_mindR;
    double o_dPhibb_maxdR;
    double o_dEtabb_mindR;
    double o_dEtabb_maxdR;
    double o_dEtabb_leading_bjets;
    double o_mjj_leading_jets;
    double o_mjj_mindR;
    double o_mjj_maxdR;
    double o_dPhijj_leading_jets;
    double o_dPhijj_mindR;
    double o_dPhijj_maxdR;
    double o_dEtajj_leading_jets;
    double o_dEtajj_mindR;
    double o_dEtajj_maxdR;
    
    double o_mjb_mindR;
    double o_mjb_maxdR; 
    double o_mjb_leading_bjet;
    // discrete b tagging weight                                                                                                                                                                          
    //                                                                                                                                                                                           
    std::vector < double > o_btagw_discrete;
    std::vector < double > o_btagw_discrete_bord;
    
    //other
    int o_pseudo_mass;

    //
    //event variable 
    //
    int o_rejectEvent;

    //
    // Truth variables
    //
    double o_truth_dR_Wb;
    double o_truth_top_pt;
    double o_truth_ht_filter;
    double o_truth_met_filter;
    
    int o_TopHeavyFlavorFilterFlag;
    //
    //ISR and FSR systs
    //
    double o_nom_weight_ISR_UP;
    double o_nom_weight_ISR_DOWN;
    double o_nom_weight_FSR_UP;
    double o_nom_weight_FSR_DOWN;
    //Individual weights
    double o_nom_weight_index_1, o_nom_weight_index_2, o_nom_weight_index_3, o_nom_weight_index_4, o_nom_weight_index_5, o_nom_weight_index_6, o_nom_weight_index_7, o_nom_weight_index_8, o_nom_weight_index_193, o_nom_weight_index_194, o_nom_weight_index_198, o_nom_weight_index_199; 

    //
    // TRF-related variables
    //
    std::vector < double > *o_TRFweight_in;
    std::vector < double > *o_TRFweight_ex;
    std::vector < std::vector< bool > > *o_TRFPerm_in;
    std::vector < std::vector< bool > > *o_TRFPerm_ex;
    std::vector< std::vector<int> > *o_TRFbins_in;
    std::vector< std::vector<int> > *o_TRFbins_ex;

    std::vector < std::vector < double > > *o_LTag_BreakUp_TRF_in;
    std::vector < std::vector < double > > *o_LTag_BreakDown_TRF_in;
    std::vector < std::vector < double > > *o_LTag_BreakUp_TRF_ex;
    std::vector < std::vector < double > > *o_LTag_BreakDown_TRF_ex;

    std::vector < std::vector < double > > *o_CTag_BreakUp_TRF_in;
    std::vector < std::vector < double > > *o_CTag_BreakDown_TRF_in;
    std::vector < std::vector < double > > *o_CTag_BreakUp_TRF_ex;
    std::vector < std::vector < double > > *o_CTag_BreakDown_TRF_ex;

    std::vector < std::vector < double > > *o_BTag_BreakUp_TRF_in;
    std::vector < std::vector < double > > *o_BTag_BreakDown_TRF_in;
    std::vector < std::vector < double > > *o_BTag_BreakUp_TRF_ex;
    std::vector < std::vector < double > > *o_BTag_BreakDown_TRF_ex;

    std::vector < std::vector < double > > *o_BTagExtrapUp_TRF_in;
    std::vector < std::vector < double > > *o_BTagExtrapDown_TRF_in;
    std::vector < std::vector < double > > *o_BTagExtrapUp_TRF_ex;
    std::vector < std::vector < double > > *o_BTagExtrapDown_TRF_ex;
    std::vector < std::vector < double > > *o_BTagExtrapFromCharmUp_TRF_in;
    std::vector < std::vector < double > > *o_BTagExtrapFromCharmDown_TRF_in;
    std::vector < std::vector < double > > *o_BTagExtrapFromCharmUp_TRF_ex;
    std::vector < std::vector < double > > *o_BTagExtrapFromCharmDown_TRF_ex;

    //
    // Some booleans
    //
    bool o_is_ttbar;
};

#endif //FCNC_OUTPUTDATA_H
