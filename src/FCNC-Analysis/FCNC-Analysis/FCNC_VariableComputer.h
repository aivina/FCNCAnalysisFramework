#ifndef FCNC_VARIABLECOMPUTER_H
#define FCNC_VARIABLECOMPUTER_H

#include <vector>
#include <TRandom3.h>

class AnalysisObject;
class OptionsBase;

class FCNC_VariableComputer {

public:

    //
    // Standard C++ functions
    //
    FCNC_VariableComputer( OptionsBase* opt );
    FCNC_VariableComputer( const FCNC_VariableComputer & );
    ~FCNC_VariableComputer();

    //
    // Specific functions
    //
    double GetMeff( std::vector< AnalysisObject* > &v_jets,
                    std::vector< AnalysisObject* > &v_el,
                    std::vector< AnalysisObject* > &v_mu,
                    AnalysisObject *met) const;

    double GetHtHad( std::vector< AnalysisObject* > &v_jets ) const;
    double GetMetSignificance( const double met, const double hthad) const;
    double GetMTw( std::vector< AnalysisObject* > &v_el,
                   std::vector< AnalysisObject* > &v_mu,
                   AnalysisObject* met ) const;

    double GetPTw( std::vector< AnalysisObject* > &v_el,
                   std::vector< AnalysisObject* > &v_mu,
                   AnalysisObject* met ) const;

    double GetMindR( std::vector< AnalysisObject* > &v_obj1,
                    std::vector< AnalysisObject* > &v_obj2,
                    const std::string &mom1 = "", const std::string &mom2 = "" ) const;

    double GetAveragedR( std::vector< AnalysisObject* > &v_obj1,
			 std::vector< AnalysisObject* > &v_obj2,
			 const std::string &mom1 = "", const std::string &mom2 = "" ) const;

    double GetMindPhi( AnalysisObject *v_obj1, std::vector< AnalysisObject* > &v_obj2, const int maxVec = -1, const std::string &mom = "" ) const ;

    double GetMindPhi( std::vector< AnalysisObject* > &v_obj1,
                      std::vector< AnalysisObject* > &v_obj2 ) const ;

    double GetMbb( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetMbbMaxDr( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;

    double GetDphibbLeadingBjets( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetDphibbMinDr( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetDphibbMaxDr( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetDetabbLeadingBjets( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetDetabbMinDr( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetDetabbMaxDr( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;

    double GetMjjLeadingJets( std::vector< AnalysisObject* > &v_jets ) const;
    double GetMjjMinDr ( std::vector< AnalysisObject* > &v_jets ) const;
    double GetMjjMaxDr ( std::vector< AnalysisObject* > &v_jets ) const;
    double GetDphijjLeadingJets( std::vector < AnalysisObject* > &v_jets ) const;
    double GetDphijjMinDr( std::vector< AnalysisObject* > &v_jets ) const;
    double GetDphijjMaxDr( std::vector< AnalysisObject* > &v_jets ) const;
    double GetDetajjLeadingJets( std::vector < AnalysisObject* > &v_jets ) const;
    double GetDetajjMinDr( std::vector<AnalysisObject*> &v_jets ) const;
    double GetDetajjMaxDr( std::vector<AnalysisObject*> &v_jets ) const;

    int GetNbjets( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;

    int GetNjets40GeV( std::vector< AnalysisObject* > &v_jets ) const;
    double GetCentrality( std::vector< AnalysisObject* > &v_jets, std::vector< AnalysisObject* > &v_el, std::vector< AnalysisObject* > &v_mu ) const;
    double GetMbbLeadingBjets( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetMjbMinDr(std::vector< AnalysisObject* > &v_jets) const;
    double GetMjbMaxDr(std::vector< AnalysisObject* > &v_jets) const;
    double GetMjbLeadingBjet(std::vector< AnalysisObject* > &v_jets) const;
    double GetMbbSoftestBjets( std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = "" ) const;
    double GetJLeptonInvariantMass(std::vector< AnalysisObject* > &v_RCTTMjets, std::vector< AnalysisObject* > &v_el, std::vector< AnalysisObject* > &v_mu ) const;
    double GetJLeadingBInvariantMass(std::vector< AnalysisObject* > &v_RCTTMjets,  std::vector< AnalysisObject* > &v_jets, const std::string &bjet_moment = ""  ) const;
    double GetJJInvariantMass( std::vector< AnalysisObject* > &v_RCTTMjets ) const;
    std::vector<double> GetBtagwDiscrete( std::vector< AnalysisObject* > &v_jets, std::string btagalg = "MV2c10") const;
    double GetMTbmin( std::vector< AnalysisObject* > &v_jets, AnalysisObject *met, const std::string &mom = "" ) const;
    int GetJetClass(AnalysisObject* jet) const;
    int GetPseudoHplusMass( int dsid ) const;
    double GetISRSystUp( int dsid, std::vector< float > &v_mc_generator_weights) const;
    double GetISRSystDown( int dsid, std::vector< float > &v_mc_generator_weights) const;
    double GetFSRSystUp( int dsid, std::vector< float > &v_mc_generator_weights) const;
    double GetFSRSystDown( int dsid, std::vector< float > &v_mc_generator_weights) const;
    double GetIndividualWeight( int dsid, std::vector< float > &v_mc_generator_weights, int weight_index) const;
    double GetIndividualSumOfWeight( int dsid, std::vector< float > &v_mc_generator_weights, int weight_index, double &sum_of_weights) const;

private:
    OptionsBase* m_opt;
};

#endif// FCNC_VARIABLECOMPUTER_H
