
#ifndef FCNC_SELECTOR_H
#define FCNC_SELECTOR_H

#include "IFAETopFramework/SelectorBase.h"
#include <fstream>

class FCNC_Options;
class FCNC_AnalysisTools;

class OutputHistManager;
class OutputTreeManager;

class FCNC_Selector : public SelectorBase{

public:

  struct SelProp{
    int index;
    std::string name;
    std::string primanc_name;
    int type;
  };

  //=============================================================================================

  enum FCNCSelFlags{DOSYST=DOTREE+1,FIT,ONELEP};

  //=========== Top selections =========================
  enum FCNCTopSels{ c_1l_chan=1, c_1el_chan, c_1mu_chan, 
		    c_4jex, c_5jex, c_6jex, 
		    c_4jin, c_5jin, c_6jin, c_10jin, 
		    c_0bex, c_1bex, c_2bex, c_3bex, 
		    c_0bin, c_1bin, c_2bin, c_3bin, c_4bin,
		    TOPSEL_MAX
  };


    FCNC_Selector( FCNC_Options* opt, const FCNC_NtupleData *ntupData, FCNC_OutputData *outData, FCNC_AnalysisTools *anaTools
		   , const bool useDecisions=true, const bool add_primaries=false );
    FCNC_Selector( const FCNC_Selector & );
    ~FCNC_Selector();

    inline void SetOutputHistManager(OutputHistManager* outMngrHist){ m_outMngrHist = outMngrHist; }

    bool Init();

    Selection* AddFCNCSelection( const std::string &name, const bool do_runop=true, const bool do_syst=false, const int reg_type=0);

    int GetSelectionIndex( const std::string& sel_name);
    std::string GetSelectionName( const int sel_index);

    virtual bool PassSelection( const int index );
    virtual bool RunOperations(const Selection& sel) const;

 protected:
    virtual Selection* MakeSelection( const int index, const std::string& name="" );

    bool AddPrimary( Selection& sel, const std::string& anc_name);
    bool AddAncestor( Selection& sel, const std::string& anc_name, bool is_primary=false);
    bool AddAncestors( Selection& sel, const std::vector<std::string> &anc_list, const std::string& primary);
    bool AddBlindingCut(Selection& sel);

    int AddSelectionIndex( const std::string& sel_name, int index=-1, bool newentry=false);
    SelProp MakeSelProp(const std::string& _name, int _type, const std::string& _primanc_name="");

 private:
    FCNC_Options *m_opt;
    const FCNC_NtupleData *m_ntupData;
    FCNC_OutputData *m_outData;
    FCNC_AnalysisTools *m_anaTools;

    OutputHistManager* m_outMngrHist;

    std::map<std::string, int>* m_sel_indices;
    std::map<int, std::string>* m_sel_names;
    std::vector<SelProp>* m_sel_lep_prop;
    std::vector<SelProp>* m_sel_lepflav_prop;
    std::vector<SelProp>* m_sel_jet_prop;
    std::vector<SelProp>* m_sel_bjet_prop;
    std::ifstream m_blinding_config;

};

#endif
