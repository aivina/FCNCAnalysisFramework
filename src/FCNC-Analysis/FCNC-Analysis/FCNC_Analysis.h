#ifndef FCNC_ANALYSIS_H
#define FCNC_ANALYSIS_H

#include <map>
#include <string>
#include <vector>

#include "IFAETopFramework/CutFlowTools.h"

//IFAEFramework classes
class OutputHistManager;
class OutputTreeManager;
class CutFlowTools;

//FCNC specific classes
class FCNC_Options;
class FCNC_OutputData;
class FCNC_AnalysisTools;
class FCNC_VariableComputer;
class FCNC_TruthManager;
class FCNC_TRFManager;
class FCNC_WeightManager;
class FCNC_NtupleData;
class FCNC_NtupleReader;
class FCNC_Selector;

class FCNC_Analysis {

public:
    FCNC_Analysis(FCNC_Options *opt);
    ~FCNC_Analysis();

    bool Begin();
    bool Loop();
    bool Process(Long64_t entry);
    bool Terminate();
    
    bool CutFlow();
    CutFlowTools* m_CutFlowTools; //!

private:
    /// a text summary of the yields from the main histograms
    std::string summaryYields();
    bool SumAnalysisRegions(const bool newFile);

private:
    FCNC_Options *m_opt;
    const FCNC_NtupleData *m_ntupData;
    FCNC_NtupleReader *m_reader;
    FCNC_OutputData* m_outData;
    OutputHistManager *m_outMngrHist;
    OutputTreeManager *m_outMngrTree;
    OutputTreeManager *m_outMngrDLTree;
    OutputTreeManager *m_outMngrOvlerlapTree;
    FCNC_AnalysisTools *m_anaTools;
    FCNC_VariableComputer *m_varComputer;
    FCNC_TruthManager *m_truthMngr;
    FCNC_TRFManager *m_TRFMngr;
    FCNC_WeightManager *m_weightMngr;
    FCNC_Selector *m_selector;
    std::map < std::string, std::vector < std::string > > m_channels;
    std::vector < std::string > m_topTaggers;
    std::map < int, int > m_map_region_enum_index;
};//end of class FCNC_Analysis

#endif //FCNC_ANALYSIS_H
