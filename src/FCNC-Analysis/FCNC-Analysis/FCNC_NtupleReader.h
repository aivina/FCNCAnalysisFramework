#ifndef FCNC_NTUPLEREADER
#define FCNC_NTUPLEREADER

#include "IFAETopFramework/NtupleReader.h"

#include <string>

class FCNC_Options;
class FCNC_NtupleData;
class TriggerInfo;

class FCNC_NtupleReader : public NtupleReader {
    
public:
    //
    // Standard C++ functions
    //
    FCNC_NtupleReader(FCNC_Options* opt);
    ~FCNC_NtupleReader();
    
    //
    // Branch setters
    //
    int SetAllBranchAddresses();
    int SetEventBranchAddresses();
    int SetJetBranchAddresses(const std::string &sj = "");
    int SetLeptonBranchAddresses();
    int SetWeightBranchAddresses();
    int SetMETBranchAddresses();
    int SetTruthParticleBranchAddresses();
    int SetTRFBranchAddresses();
    
    //Set the list of triggers
    inline void SetTriggerList(std::vector<TriggerInfo*>& trigger_list){ m_trigger_list = &trigger_list; }

    //
    // Get the ntuple data
    //
    const inline FCNC_NtupleData* Data() const{ return m_FCNC_ntupData; }
    
private:
    //
    // Here we declare the local FCNC_NtupleData object to store the
    // analysis specific variables
    //
    FCNC_NtupleData* m_FCNC_ntupData;
    FCNC_Options* m_opt;
    const std::vector<TriggerInfo*>* m_trigger_list;
    
};

#endif //FCNC_NTUPLEREADER_H
