#ifndef FCNC_WEIGHTSETTER_H
#define FCNC_WEIGHTSETTER_H

//in IFAETopFramework
#include "IFAETopFramework/WeightManager.h"

#include <map>

class SampleInfo;

//in FCNCAnalysis
class FCNC_Options;
class FCNC_OutputData;
class FCNC_NtupleData;
class FCNC_VariableComputer;

//in IFAETopFramework
class Selection;

class FCNC_WeightManager : public WeightManager {

public:
    //
    // Standard C++ functions
    //
    FCNC_WeightManager( FCNC_Options *opt, const FCNC_NtupleData* ntupleData, FCNC_OutputData* outputData );
    FCNC_WeightManager( const FCNC_WeightManager & );
    virtual ~FCNC_WeightManager();

    //
    // Class specific functions
    //
    //void Init( std::map < int, Selection* >* selection_tree );
    void Init();
    bool AddFCNCNominalWeights();
    bool AddFCNCSystematicWeights();

    bool SetLeptonSFWeights( const bool apply_trigger_weights );
    bool SetCrossSectionWeight();
    bool SetTtbarHtSliceScale();

 private:
    FCNC_Options *m_fcnc_opt;
    const FCNC_NtupleData *m_fcnc_ntupData;
    FCNC_OutputData *m_fcnc_outData;
    SampleInfo *m_sampleInfo;
    FCNC_VariableComputer *m_varComputer;
};

#endif //FCNC_WEIGHTSETTER_H
