#ifndef FCNC_ANALYSIS_TOOLS_H
#define FCNC_ANALYSIS_TOOLS_H

#include <vector>
#include <string>

class AnalysisObject;
class OutputHistManager;

class FCNC_NtupleData;
class TTH_NtupleData;
class FCNC_NtupleData;
class FCNC_OutputData;
class FCNC_Options;
class FCNC_TRFManager;
class FCNC_VariableComputer;
class FCNC_WeightManager;

class FCNC_AnalysisTools {

public:

    //
    // Standard C++ functions
    //
    FCNC_AnalysisTools( FCNC_Options*, OutputHistManager*, const FCNC_NtupleData *, FCNC_OutputData *, FCNC_WeightManager *, FCNC_TRFManager *, FCNC_VariableComputer * );
    FCNC_AnalysisTools( const FCNC_AnalysisTools & );
    ~FCNC_AnalysisTools();

    //
    // Objects selection
    //
    bool GetObjectVectors();

    //
    // Histograms handling
    //
    void BookAllHistograms( const std::string &key/*, const bool split = false*/, const bool useSysts = true );
    void FillAllHistograms( const std::string &key );
    void BookAllTH1DHistogramsHistManager(/*const bool split,*/ const std::string &name, const std::string &title, double binsize, double xlow, double xup, const std::string &key="", const std::string &xtitle="", const std::string &ytitle="", int lw=2, int lc=1);
    void BookAllTH1DHistogramsHistManager(/*const bool split,*/ const std::string &name, const std::string &title, int nbins, double* xedges, const std::string &key="", const std::string &xtitle="", const std::string &ytitle="", int lw=2, int lc=1);
    void FillAllTH1DHistogramsHistManager( const std::string &key, double val, double wgt );

    //
    // Update region-dependant weights (e.g. ttbar systematics)
    //
    bool UpdateRegionDependentWeight( );

    //
    // TRF handling
    //
    bool PassBTagRequirement( const int btag_req, const bool isIncl );

    //
    // Variable computation
    //
    bool ComputeAllVariables();
    bool ComputeBTagVariables();
    void ConvertToGeVAll();

private:
    FCNC_Options *m_opt;
    OutputHistManager* m_outputMngr;

    const FCNC_NtupleData* m_ntupData;
    FCNC_OutputData* m_outData;
    FCNC_WeightManager* m_weightMngr;
    FCNC_TRFManager* m_trfMngr;
    FCNC_VariableComputer* m_varComputer;
    float mev_to_gev=0.001;
};

#endif //FCNC_ANALYSIS_TOOLS_H
