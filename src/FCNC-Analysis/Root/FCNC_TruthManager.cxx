#include "IFAETopFramework/AnalysisObject.h"

#include "IFAETopFramework/AnalysisObject.h"
#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_NtupleData.h"

#include "FCNC-Analysis/FCNC_TruthManager.h"


//________________________________________________________
//
FCNC_TruthManager::FCNC_TruthManager( FCNC_Options *opt, const FCNC_NtupleData *ntup ):
m_opt(opt),
m_ntupData(ntup),
m_truthParticle(0),
m_truthPartons(0),
m_truthMEParticles(0),
m_dottbar(0)
{}

//________________________________________________________
//
FCNC_TruthManager::FCNC_TruthManager( const FCNC_TruthManager &q ){
    m_opt              = q.m_opt;
    m_ntupData         = q.m_ntupData;
    m_truthParticle    = q.m_truthParticle;
    m_truthPartons     = q.m_truthPartons;
    m_truthMEParticles = q.m_truthMEParticles;
    m_dottbar          = q.m_dottbar;
}

//________________________________________________________
//
FCNC_TruthManager::~FCNC_TruthManager()
{
    if(m_truthParticle){
        for( const AnalysisObject *truthpart : *m_truthParticle ){
            delete truthpart;
        }
        m_truthParticle -> clear();
        delete m_truthParticle;
    }

    if(m_truthPartons){
      for (const AnalysisObject *parton : *m_truthPartons){
	delete parton;
      }
      m_truthPartons -> clear();
      delete m_truthPartons;

    }


    if(m_truthMEParticles){
        for( const AnalysisObject *MEpart : *m_truthMEParticles ){
            delete MEpart;
        }
        m_truthMEParticles -> clear();
        delete m_truthMEParticles;
    }
}

//________________________________________________________
//
bool FCNC_TruthManager::Initialize() {

  if(!m_ntupData){
    std::cout << "<!> ERROR in FCNC_TruthManager: the NtupleData object seems absent, please check !" << std::endl;
    return false;
  }
  if(m_truthParticle){
    //Reinitialize the object before filling it again
    for( const AnalysisObject *truthpart : *m_truthParticle ){
      delete truthpart;
    }
    m_truthParticle -> clear();
    delete m_truthParticle;
  }

  m_truthParticle = new AOVector();
  for ( unsigned int iTruth = 0; iTruth < m_ntupData -> d_mc_pt -> size(); ++iTruth ) {
    AnalysisObject *temp = new AnalysisObject();
    temp -> SetPtEtaPhiM( m_ntupData -> d_mc_pt -> at(iTruth), m_ntupData -> d_mc_eta -> at(iTruth), m_ntupData -> d_mc_phi -> at(iTruth), m_ntupData -> d_mc_m -> at(iTruth) );
    temp -> SetMoment( "pdgId", m_ntupData -> d_mc_pdgId -> at(iTruth) );
    temp -> SetMoment( "absPdgId", TMath::Abs(m_ntupData -> d_mc_pdgId -> at(iTruth)) );
    temp -> SetMoment( "children_n", m_ntupData -> d_mc_children_index -> at(iTruth).size() );

    //Children information
    int counter = 0;
    for ( const int index : m_ntupData -> d_mc_children_index -> at(iTruth) ) {
      std::string str_ind = "children_";
      str_ind += std::to_string(counter);
      temp -> SetMoment( str_ind, index );
      counter++;
    }

    //Decay type (always useful)
    int code = 0;
    if(m_ntupData -> d_mc_children_index -> at(iTruth).size() >= 2){
      for( unsigned int i = 0; i < m_ntupData -> d_mc_children_index -> at(iTruth).size(); ++i ){
        const int index = m_ntupData -> d_mc_children_index -> at(iTruth)[i];
        if( index < (int)m_ntupData -> d_mc_pt -> size() ){
          const int abspdgId = TMath::Abs( m_ntupData -> d_mc_pdgId -> at(index) );
          code |= 1<<abspdgId;
        }
      }
    }
    temp -> SetMoment( "decayType", code );
    m_truthParticle -> push_back(temp);
  }

  // PrintTruthContent();
  return true;
}

//________________________________________________________
//
double FCNC_TruthManager::GetTopPt() const {
  if(m_truthParticle){
    for( const AnalysisObject *truthpart : *m_truthParticle ){
      if ( (int)truthpart -> GetMoment( "pdgId" ) == 6 ){
        return truthpart->Pt();
      }
    }
  }
  return 0;
}

//________________________________________________________
//
int FCNC_TruthManager::PrintTruthContent() const {
    std::cout << "================================================================" << std::endl;
    std::cout << "=============== Printing truth information =====================" << std::endl;
    std::cout << "================================================================" << std::endl;
    std::cout << std::endl;
    std::cout << "Brute force truth info dumping ================================= " << std::endl;;
    for ( unsigned int iTruth = 0; iTruth < m_truthParticle -> size(); ++iTruth) {
        AnalysisObject *obj = m_truthParticle -> at(iTruth);
        std::cout << "=> Index = " << iTruth << " \t  pdgId: " << obj -> GetMoment("pdgId") << " \t  Children: ";
        for ( int iChild = 0; iChild < obj -> GetMoment("children_n"); ++iChild ) {
            std::string str_ind = "children_";
            str_ind += std::to_string(iChild);
            std::cout << obj -> GetMoment(str_ind) << ", ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Decay chain plotting =========================================== " << std::endl;
    for ( unsigned int iTruth = 0; iTruth < m_truthParticle -> size(); ++iTruth) {
        if(TMath::Abs(m_truthParticle -> at(iTruth) -> GetMoment("pdgId"))==6){
            std::string space = "";
            FollowDecayChain(iTruth,space);
        }
    }
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "============================= DONE =============================" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    return 1;
}

//________________________________________________________
//
int FCNC_TruthManager::FollowDecayChain( const int index, std::string &space ) const {



  if(!m_truthParticle){
        std::cerr << "<!> Error in FCNC_TruthManager::FollowDecayChain(" << index << "): no m_truthParticle vector found :-( " << std::endl;
        return -1;
    }
    if(index >= (int)m_truthParticle -> size()){
        std::cerr << "<!> Error in FCNC_TruthManager::FollowDecayChain(" << index << "): index is larger than vector size !!" << std::endl;
        return -1;
    }

    std::cout << space << "-> index: " << index << " (pdgId = " << m_truthParticle -> at(index) -> GetMoment("pdgId") << ", n_children = "<< m_truthParticle -> at(index) -> GetMoment("children_n") <<")" << std::endl;

    for ( int iChild = 0; iChild < m_truthParticle->at(index)->GetMoment("children_n"); ++iChild ) {
        std::string str_ind = "children_";
        str_ind += std::to_string(iChild);
        std::string temp_space = space + " ";
        FollowDecayChain( m_truthParticle -> at(index) -> GetMoment(str_ind), temp_space );
    }

    return 1;
}
