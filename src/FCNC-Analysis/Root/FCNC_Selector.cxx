
#include <iostream>
#include <algorithm>
#include <ctype.h>
#include "IFAETopFramework/AnalysisUtils.h"
#include "IFAETopFramework/OptionsBase.h"
#include "IFAETopFramework/OutputHistManager.h"
#include "IFAETopFramework/Selection.h"

#include "FCNC-Analysis/FCNC_NtupleData.h"
#include "FCNC-Analysis/FCNC_OutputData.h"
#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_AnalysisTools.h"

#include "FCNC-Analysis/FCNC_Selector.h"
#include "FCNC-Analysis/FCNC_Enums.h"

//______________________________________________________________________________
//
FCNC_Selector::FCNC_Selector( FCNC_Options *opt, const FCNC_NtupleData *ntupData,
                            FCNC_OutputData *outData, FCNC_AnalysisTools *anaTools,
                            const bool useDecisions, const bool add_primaries ):
  SelectorBase((OptionsBase*)opt, (OutputData*)outData, useDecisions, add_primaries),
  m_opt(opt),
  m_ntupData(ntupData),
  m_outData(outData),
  m_anaTools(anaTools),
  m_outMngrHist(0),
  m_sel_indices(NULL),
  m_sel_names(NULL),
  m_sel_lep_prop(NULL),
  m_sel_lepflav_prop(NULL),
  m_sel_jet_prop(NULL),
  m_sel_bjet_prop(NULL)
{

}

//______________________________________________________________________________
//
FCNC_Selector::FCNC_Selector( const FCNC_Selector &q ):
SelectorBase(q)
{
    m_opt                  = q.m_opt;
    m_ntupData             = q.m_ntupData;
    m_outData              = q.m_outData;
    m_anaTools             = q.m_anaTools;
    m_outMngrHist          = q.m_outMngrHist;
    m_sel_indices          = new std::map<std::string, int>(*(q.m_sel_indices));
    m_sel_names            = new std::map<int, std::string>(*(q.m_sel_names));
    m_sel_lep_prop         = new std::vector<SelProp>(*(q.m_sel_lep_prop));
    m_sel_lepflav_prop     = new std::vector<SelProp>(*(q.m_sel_lepflav_prop));
    m_sel_jet_prop         = new std::vector<SelProp>(*(q.m_sel_jet_prop));
    m_sel_bjet_prop        = new std::vector<SelProp>(*(q.m_sel_bjet_prop));
}

//______________________________________________________________________________
//
FCNC_Selector::~FCNC_Selector(){
  delete m_sel_indices;
}

//______________________________________________________________________________
//
bool FCNC_Selector::Init(){
  if(m_opt->MsgLevel() == Debug::DEBUG){
    std::cout<<" Inside FCNC_Selector::Init()"<<std::endl;
  }

  m_blinding_config.open( Form("%s/python/FCNCAnalysis/regions_dictionary.py", std::getenv("ROOTCOREBIN")) );
  //============================  Initialise top selections and add rules for primary ancestors ==================================
  m_sel_indices = new std::map<std::string, int>();
  m_sel_names = new std::map<int, std::string>();

  m_sel_lep_prop = new std::vector<SelProp>({
      MakeSelProp("1lep", c_1l_chan)});

  m_sel_lepflav_prop = new std::vector<SelProp>({
      MakeSelProp("1el", c_1el_chan) , MakeSelProp("1mu", c_1mu_chan) });

  m_sel_jet_prop = new std::vector<SelProp>({
      MakeSelProp("4jin", c_4jin, ""),
      MakeSelProp("4jex", c_4jex, "4jin"), MakeSelProp("5jin", c_5jin, "4jin"),
      MakeSelProp("5jex", c_5jex, "5jin"), MakeSelProp("6jin", c_6jin, "5jin"),
      MakeSelProp("10jin", c_10jin, "6jin") });

  m_sel_bjet_prop = new std::vector<SelProp>({
    MakeSelProp("0bex", c_0bex, ""), MakeSelProp("1bin", c_1bin, "")
    ,MakeSelProp("1bex", c_1bex, "1bin"), MakeSelProp("2bin", c_2bin, "1bin")
    ,MakeSelProp("2bex", c_2bex, "2bin"), MakeSelProp("3bin", c_3bin, "2bin")
    ,MakeSelProp("3bex", c_3bex, "3bin"), MakeSelProp("4bin", c_4bin, "3bin") });

  for(auto selprop : *m_sel_lep_prop){ AddSelectionIndex(selprop.name, selprop.index); }
  for(auto selprop : *m_sel_lepflav_prop){ AddSelectionIndex(selprop.name, selprop.index); }
  for(auto selprop : *m_sel_jet_prop){ AddSelectionIndex(selprop.name, selprop.index); }
  for(auto selprop : *m_sel_bjet_prop){ AddSelectionIndex(selprop.name, selprop.index); }
  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<"Added all top selections "<<std::endl; }
  //========================== Add desired regions ================================================

  bool do_runop = true;

  std::vector<std::string> ch_lep; ch_lep.clear();
  if(m_opt->DoSplitEMu()) ch_lep = {"", "_el", "_mu"};
  else ch_lep = {""};

 std::vector<std::string> v_bjet_presel = {"2bin", "2bex", "3bin"};
  if(m_opt->DoLowBRegions()){
    v_bjet_presel.push_back("0bex");
    v_bjet_presel.push_back("1bex");
  }

  const bool do_syst=true;
  //all regions are treated as fit regions
  for(const std::string& jet : {"4jin"}){
    for(const std::string& bjet : v_bjet_presel){
      AddFCNCSelection("c-1lep-"+jet+"-"+bjet, do_runop, do_syst, FIT);
      if(m_opt->DoSplitEMu()){
        AddFCNCSelection("c-1lep-"+jet+"-"+bjet+"_el", do_runop, do_syst, FIT);
        AddFCNCSelection("c-1lep-"+jet+"-"+bjet+"_mu", do_runop, do_syst, FIT);
      }
    }//bjet
  }

  for(const std::string& channel : ch_lep)
    {
      //AddFCNCSelection("c-1lep-4jin-2bin"+channel, do_runop, do_syst, FIT);
      for(const std::string& jet : {"4jex", "5jex", "6jin", "10jin"}){
        for( const std::string& bjet : {"2bex", "3bex", "4bin"}){
          AddFCNCSelection("c-1lep-"+jet+"-"+bjet+channel, do_runop, do_syst, FIT);
        }//bjet
      }//jet
    }

  m_blinding_config.close();

  return true;

}

//______________________________________________________________________________
//
Selection* FCNC_Selector::AddFCNCSelection(const std::string& name, bool do_runop, bool do_syst, const int reg_type){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" AddFCNCSelection "<<name<<" do_runop = "<<do_runop<<" do_syst = "<<do_syst<<" reg_type = "<<reg_type<<std::endl; }
  Selection* sel = AddSelection( AddSelectionIndex(name), name, do_runop );
  if(!sel){ std::cerr << "Failed to add selection with name "<<name<<std::endl; return NULL; }

  sel->AddFlagAtBit(DOSYST, do_syst);
  sel->AddFlagAtBit(FIT, true);
  sel->AddFlagAtBit(ONELEP, true);

  std::string hist_name = AnalysisUtils::ReplaceString(name, "-", "");
  //=================================== Book histogram in region ===============================
  if(m_opt->DumpHistos() && do_runop){
    m_anaTools -> BookAllHistograms(hist_name/*, true*/, do_syst);
  }

  return sel;

}

//______________________________________________________________________________
//
Selection* FCNC_Selector::MakeSelection(const int index, const std::string& name){
  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" FCNC_Selector::MakeSelection with name "<<name<<" and index "<<index<<std::endl; }

  if( (index < 0) && name.empty() ){
    std::cerr << "FCNC_Selector::MakeSelection --> No rule available to make selection with empty name and negative index. Exiting."<<std::endl;
    return NULL;
  }

  const std::string& sel_name = (name.empty()) ? GetSelectionName(index) : name;
  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Selection index = "<<index<<" name = "<<name<<" sel_name = "<<sel_name<<std::endl; }

  //================ Build selection hierarchies -=========

  //Parse the name into its components
  std::string _name_ = sel_name;
  std::string _parts_= "";
  std::string::size_type pos = 0;

  int n_nodes = 0;

  SelProp* sprop_lep = NULL;
  SelProp* sprop_jet = NULL;
  SelProp* sprop_bjet = NULL;

  do{

    pos = AnalysisUtils::ParseString(_name_, _parts_, "-");
    AnalysisUtils::TrimString(_parts_);
    bool found = false;

    //=============== Lepton part =====================
    for(SelProp& lepprop : *m_sel_lep_prop){
      if(lepprop.name == _parts_){
    	sprop_lep = &lepprop;
    	found = true; n_nodes++; break;
      }
    }
    if(found) continue;

    //=============== Jet part ========================
    for(SelProp& jetprop : *m_sel_jet_prop){
      if(jetprop.name == _parts_){
    	sprop_jet = &jetprop;
    	found = true; n_nodes++; break;
      }
    }
    if(found) continue;

    //=============== B-jet part ========================
    for(SelProp& bjetprop : *m_sel_bjet_prop){
      if(bjetprop.name == _parts_){
    	sprop_bjet = &bjetprop;
    	found = true; n_nodes++; break;
      }
    }
    if(found) continue;

  }while(pos != std::string::npos);

  //============================= Only certain selection patterns are currently allowed =========================
  //==== All regions are required to have, at minimum, defined lepton and jet multiplicity requirements.
  //==== Other requirements are optional
  if( (n_nodes > 1) && !sprop_lep ){ //If no lepton channel found for a compound selection
    std::cerr << " No lepton channel could be identified for selection " << name << ". Exiting."  << std::endl;
    return NULL;
  }
  if( (n_nodes > 1) && !sprop_lep ){ //If no jet channel found for a compound selection
    std::cerr << " No allowed jet multiplicity selection found for selection " << name << ". Exiting."  << std::endl;
    return NULL;
  }

  //==================== MAKE SELECTION ==================================
  Selection* sel = SelectorBase::MakeSelection(index, sel_name);

  //============================= ADD ANCESTORS ======================================================================
  if(n_nodes <= 1){
    ;//no ancestors added for a top selection
  }
  else if( (sel_name.find("_el") == sel_name.size()-3) || (sel_name.find("_mu") == sel_name.size()-3) ){
    if(sel_name.find("_el") != std::string::npos){
      AddAncestor(*sel, sel_name.substr(0, sel_name.find("_el")), true);
      SelectorBase::AddAncestor(*sel, c_1el_chan);
    }
    else if(sel_name.find("_mu") != std::string::npos){
      AddAncestor(*sel, sel_name.substr(0, sel_name.find("_mu")), true);
      SelectorBase::AddAncestor(*sel, c_1mu_chan);
    }
  }//el/mu channel splitting is done last
  else{

    if(!sprop_bjet){
      if(sprop_jet->primanc_name.empty()){
	SelectorBase::AddAncestors(*sel,{sprop_lep->index, sprop_jet->index}, sprop_lep->index);
      }
      else{
	AddPrimary(*sel, "c-"+sprop_lep->name+"-"+sprop_jet->primanc_name);
	SelectorBase::AddAncestors(*sel, {sprop_lep->index, sprop_jet->index});
      }
    }//lep+jet
    else{
      if(sprop_bjet->primanc_name.empty()){
	AddAncestor(*sel, "c-"+sprop_lep->name+"-"+sprop_jet->name, true);
	SelectorBase::AddAncestor(*sel, sprop_bjet->index);
      }
      else{
	AddPrimary(*sel, "c-"+sprop_lep->name+"-"+sprop_jet->name+"-"+sprop_bjet->primanc_name);
	SelectorBase::AddAncestors(*sel, {sprop_lep->index, sprop_jet->index, sprop_bjet->index});
      }
    }//lep+jet+bjet
  }

  return sel;
}

//______________________________________________________________________________
//
bool FCNC_Selector::PassSelection(const int index){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Calling PassSelection for index "<<index<<std::endl; }
  bool pass = true;

  //== Lepton channels ==
  if(index == c_1l_chan){
    pass = (m_outData->o_channel_type == FCNC_Enums::ELECTRON) || (m_outData->o_channel_type == FCNC_Enums::MUON);
  }
  else if(index == c_1el_chan){
    pass = (m_outData->o_channel_type == FCNC_Enums::ELECTRON);
  }
  else if(index == c_1mu_chan){
    pass = (m_outData->o_channel_type == FCNC_Enums::MUON);
  }


  //=========== Jet multiplicities ====================
  else if(index == c_4jex){ pass = (m_outData->o_jets_n == 4); }
  else if(index == c_5jex){ pass = (m_outData->o_jets_n == 5); }
  else if(index == c_6jex){ pass = (m_outData->o_jets_n == 6); }

  else if(index == c_4jin){ pass = (m_outData->o_jets_n >= 4); }
  else if(index == c_5jin){ pass = (m_outData->o_jets_n >= 5); }
  else if(index == c_6jin){ pass = (m_outData->o_jets_n >= 6); }
  else if(index == c_10jin){ pass = (m_outData->o_jets_n >= 10); }


  //=========== B-tag multiplicities ====================
  else if(index == c_0bex){ pass = m_anaTools->PassBTagRequirement(0, false); }
  else if(index == c_1bex){ pass = m_anaTools->PassBTagRequirement(1, false); }
  else if(index == c_2bex){ pass = m_anaTools->PassBTagRequirement(2, false); }
  else if(index == c_3bex){ pass = m_anaTools->PassBTagRequirement(3, false); }

  else if(index == c_0bin){ pass = m_anaTools->PassBTagRequirement(0, true); }
  else if(index == c_1bin){ pass = m_anaTools->PassBTagRequirement(1, true); }
  else if(index == c_2bin){ pass = m_anaTools->PassBTagRequirement(2, true); }
  else if(index == c_3bin){ pass = m_anaTools->PassBTagRequirement(3, true); }
  else if(index == c_4bin){ pass = m_anaTools->PassBTagRequirement(4, true); }

  return pass;

}

//______________________________________________________________________________
//
int FCNC_Selector::GetSelectionIndex( const std::string& sel_name ){
  std::map<std::string, int>::iterator selit = m_sel_indices -> find( sel_name );

  if(selit == m_sel_indices->end()){
    std::cout<<"FCNC_Selector::GetSelectionIndex --> Cannot find index for selection with name "<<sel_name<<std::endl;
    return -1;
  }
  return selit->second;

}

//______________________________________________________________________________
//
std::string FCNC_Selector::GetSelectionName( const int sel_ind){
  std::map<int, std::string>::iterator selit = m_sel_names -> find( sel_ind );
  if(selit == m_sel_names->end()){
    std::cout<<"FCNC_Selector::GetSelectionName --> Cannot find name for selection with index "<<sel_ind<<std::endl;
    return "";
  }

  return selit->second;

}

//______________________________________________________________________________
//Adds a new selection index to the map. If !newentry
int FCNC_Selector::AddSelectionIndex( const std::string& sel_name, int index, bool newentry){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Calling AddSelectionIndex("<<sel_name<<", "<<index<<","<<newentry<<")"<<std::endl; }
  int sel_index = -1;
  std::pair< std::map<string, int>::iterator, bool > selname_pair = m_sel_indices -> insert( std::pair< string, int >(sel_name, sel_index) );
  if(!selname_pair.second){
    if(newentry){
      std::cerr << " Selection at index "<<selname_pair.first->second<<" already has name "<<sel_name<<". Please check"<<std::endl;
      return -1;
    }
    else{ sel_index = selname_pair.first->second; }
  }
  else{
    sel_index = (index >= 0) ? index : std::max(m_sel_names->rbegin()->first,(int)TOPSEL_MAX)+1;
  }
  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Selection name "<<sel_name<<" assigned to index "<<sel_index<<std::endl; }
  selname_pair.first->second = sel_index;
  m_sel_names->insert(std::pair<int, std::string>(sel_index, sel_name));
  return sel_index;

}

//______________________________________________________________________________
//
bool FCNC_Selector::AddAncestor( Selection& sel, const std::string& anc_name, bool is_primary){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Adding "<<anc_name<<" as ancestor of "<<sel.Name()<<" is_primary = "<<is_primary<<std::endl; }
  bool stat = SelectorBase::AddAncestor(sel, AddSelectionIndex( anc_name ), is_primary);

  return stat;

}

//______________________________________________________________________________
//
bool FCNC_Selector::AddAncestors(Selection& sel, const std::vector<std::string> &anclist, const std::string& primary){

  bool found_primary = false;
  for(const std::string& anc : anclist){
    bool is_primary = (!found_primary) && (anc==primary);
    if(is_primary){ found_primary = true; }
    AddAncestor(sel, anc, is_primary);
  }
  if(!found_primary && !primary.empty()){ AddPrimary(sel, primary); }

  return true;
}

//______________________________________________________________________________
//
bool FCNC_Selector::AddPrimary( Selection& sel, const std::string& anc_name){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<" Adding "<<anc_name<<" as primary of "<<sel.Name()<<std::endl; }
  bool stat = SelectorBase::AddPrimary(sel, AddSelectionIndex( anc_name));
  return stat;

}

//______________________________________________________________________________
//
bool FCNC_Selector::RunOperations(const Selection& sel) const{

  //m_anaTools -> UpdateRegionDependentWeight( AnalysisUtils::ReplaceString(sel.Name(),"-","" ) );

  if( m_opt->DoBlind() && !(sel.PassBlindingCuts()) ){ return true; } //blind if needed

  std::string histName = AnalysisUtils::ReplaceString(sel.Name(),"-", "");
  //Check https://gitlab.cern.ch/htx/FCNC-Analysis/issues/15 , needed only if we use TRF
  //m_anaTools -> FillAllHistograms(histName,m_outData->o_FCNCtype/*,sel.PassFlagAtBit(DOSYST)*/);
  m_anaTools -> FillAllHistograms(histName);
  return true;

}

//______________________________________________________________________________
//
bool FCNC_Selector::AddBlindingCut(Selection& sel){

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<"FCNC_Selector::AddBlindingCut FOR "<<sel.Name()<<std::endl; }
  std::string line="";
  std::string region="";
  std::string blind_bin="";
  bool found_reg=false;
  std::string delim = ":\"";
  double blind_cut = 0.;

  std::string selName = AnalysisUtils::ReplaceString(sel.Name(),"-","");

  m_blinding_config.clear();
  m_blinding_config.seekg(0, ios::beg);

  while(std::getline(m_blinding_config, line)){
    AnalysisUtils::TrimString(line); //remove all whitespaces
    if(line.find("#") != std::string::npos){ line = line.substr(0,line.find("#")); } //remove comments

    if(found_reg){
      if(line.find("binning_blind") != std::string::npos){
        if(line.find("\"\"") != std::string::npos){blind_cut = 0.;} //empty bins
        else{
          line=AnalysisUtils::ReplaceString(line,"\",",""); //remove the comma and quotation mark at the end of the line
          blind_bin=line.substr(line.find_last_of(",")+1);
          blind_cut=atof(blind_bin.c_str());
        }
        break;
      }
      else{ continue; }
    }
    else{
      if( line.find("name") != std::string::npos ){
        region = line.substr( line.find(delim) + delim.size() );
        region=AnalysisUtils::ReplaceString(region,"\",","");  //remove the comma and quotation mark at the end of the line
        if(region == "HTX_"+selName){ found_reg = true; }
      }
      else{ continue; }
    }

  }

  if(m_opt->MsgLevel() == Debug::DEBUG){ std::cout<<"FCNC_Selector::AddBlindingCut Selection "<<sel.Name()<<" blinding cut on meff = "<<blind_cut<<std::endl; }

  return true;
}

//______________________________________________________________________________
//
FCNC_Selector::SelProp FCNC_Selector::MakeSelProp(const std::string& _name, int _index, const std::string& _primanc_name){

  SelProp selProp;
  selProp.index = (_index<0) ? AddSelectionIndex(_name) : _index;
  selProp.name = _name;
  selProp.primanc_name = _primanc_name;

  return selProp;
}
