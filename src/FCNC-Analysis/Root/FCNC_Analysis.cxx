#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "stdlib.h"
#include "TBits.h"
#include "TMath.h"
#include "TSystem.h"

#include "IFAETopFramework/OutputHistManager.h"
#include "IFAETopFramework/OutputTreeManager.h"
#include "IFAETopFramework/CommonConstants.h"
#include "IFAETopFramework/AnalysisObject.h"
#include "IFAETopFramework/AnalysisUtils.h"
#include "IFAETopFramework/TriggerInfo.h"
#include "IFAETopFramework/Selection.h"

#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_NtupleData.h"
#include "FCNC-Analysis/FCNC_NtupleReader.h"
#include "FCNC-Analysis/FCNC_OutputData.h"
#include "FCNC-Analysis/FCNC_AnalysisTools.h"
#include "FCNC-Analysis/FCNC_VariableComputer.h"
#include "FCNC-Analysis/FCNC_Enums.h"
#include "FCNC-Analysis/FCNC_TruthManager.h"
#include "FCNC-Analysis/FCNC_WeightManager.h"
#include "FCNC-Analysis/FCNC_TRFManager.h"
#include "FCNC-Analysis/FCNC_Selector.h"
#include "FCNC-Analysis/string_utils.h"

#include "FCNC-Analysis/FCNC_Analysis.h"

#include <algorithm> // copy_if
#include <iterator> // back_inserter
#include <sstream> // ostringstream
#include <stdexcept> // invalid_argument
#include <utility> // pair

using std::cout;
using std::endl;

//____________________________________________________________________________
//
FCNC_Analysis::FCNC_Analysis( FCNC_Options* opt ):
m_opt(opt),
m_ntupData(0),
m_reader(0),
m_outData(0),
m_outMngrHist(0),
m_outMngrTree(0),
m_outMngrOvlerlapTree(0),
m_outMngrDLTree(0),
m_anaTools(0),
m_varComputer(0),
m_truthMngr(0),
m_TRFMngr(0),
m_weightMngr(0)
{
  m_channels.clear();
  m_topTaggers.clear();
}

//____________________________________________________________________________
//
FCNC_Analysis::~FCNC_Analysis(){
}

//____________________________________________________________________________
//
bool FCNC_Analysis::Begin(){

  //
  // The begin function allows the proper initialisation of all the data members of the
  // analysis class.
  //
  if(!m_opt){
    std::cerr << "<!> The FCNC_Options data member is not defined ..." << std::endl;
    return false;
  }
  m_opt->checkConcistency();

  // Declaration of the NtupleReader object
  m_reader = new FCNC_NtupleReader(m_opt);
  m_ntupData = m_reader -> Data();

  // Declaration of the OutputData-derived object
  m_outData = new FCNC_OutputData(m_opt);

  //Cut flow object
  m_CutFlowTools = new CutFlowTools;

  m_CutFlowTools->addCutFlow("Cut-Flow","All|OneMu|OneEl|OneLep|MET-MTW|FourJets");

  // Defining the is ttbar boolean here
  m_outData -> o_is_ttbar =  (m_opt -> SampleName() == SampleName::TTBAR);
  m_outData -> o_is_ttbar =  m_outData -> o_is_ttbar || (m_opt -> SampleName() == SampleName::TTBARBB);
  m_outData -> o_is_ttbar =  m_outData -> o_is_ttbar || (m_opt -> SampleName() == SampleName::TTBARCC);
  m_outData -> o_is_ttbar =  m_outData -> o_is_ttbar || (m_opt -> SampleName() == SampleName::TTBARLIGHT);

  // Declaration of the WeightManager, and reading in of nominal and systematic
  // weight components
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Declaring WeightManager" << std::endl;
  m_weightMngr = new FCNC_WeightManager( m_opt, m_ntupData, m_outData );
  m_weightMngr -> AddFCNCNominalWeights();
  if(m_opt -> ComputeWeightSys()){
    m_weightMngr -> AddFCNCSystematicWeights();
  }
  m_weightMngr -> Print( true );

  // Declaration of the OutputManager
  m_outMngrHist = new OutputHistManager( m_opt, m_outData, m_weightMngr->SystMap() );

  // Declaration of the TRFManager
  m_TRFMngr = new FCNC_TRFManager( m_opt, m_weightMngr, m_ntupData, m_outData );
  m_TRFMngr -> Init();

  // Declaration of the VariableComputer
  m_varComputer = new FCNC_VariableComputer(m_opt);

  // Declaration of the FCNC_AnalysisTools object
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Declaring FCNC_AnalysisTools" << std::endl;
  m_anaTools = new FCNC_AnalysisTools( m_opt, m_outMngrHist, m_ntupData, m_outData, m_weightMngr, m_TRFMngr, m_varComputer );

  // Define selector
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Declaring FCNC_Selector" << std::endl;
  m_selector = new FCNC_Selector( m_opt, m_ntupData, m_outData, m_anaTools, false/*useDecisions*/, false/*addPrimaries*/ );

  // Declares and Initialise the FCNC_NtupleReader
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << " Initialise FCNC_NtupleReader with weight maps" << std::endl;
  int stat = m_reader -> Init( m_weightMngr -> NomMap(), m_weightMngr -> SystMap() );
  if(!stat){
    std::cerr<< "Failed NtupleReader::Init(). Exiting" << std::endl;
    abort();
  }

  // Set all branches
  if(m_opt -> MsgLevel() == Debug::DEBUG){
    std::cout << "Setting all branch addresses in Example_NtupleReader" << std::endl;
  }
  int i_stat = m_reader -> SetAllBranchAddresses();
  if(i_stat != 0){
    std::cerr << "Failed NtupleReader::SetAllBranchAddresses(). Exiting" << std::endl;
    abort();
  }
  if( m_opt -> DoTRF() && !(m_opt -> IsData() || (m_opt -> StrSampleName().find("QCD") != std::string::npos)) && !m_opt -> RecomputeTRF() ){
    i_stat = m_reader -> SetTRFBranchAddresses();
    if(i_stat != 0){
      std::cerr << "Failed NtupleReader::SetTRFBranchAddresses(). Exiting" << std::endl;
      abort();
    }
  }

  // Declaration of the OutputTreeManager for the small tree
  if(m_opt->DumpTree()){

    m_outMngrTree = new OutputTreeManager( m_opt, m_outData );
    m_outMngrTree->AddAllWeightBranches("nomWeight", m_weightMngr, true);
    //m_outMngrTree->AddAllWeightBranches("", m_weightMngr, true);
    //checking the HF filter cut is done properly
    m_outMngrTree->AddStandardBranch("TopHeavyFlavorFilterFlag", "TopHeavyFlavorFilterFlag", &(m_outData->o_TopHeavyFlavorFilterFlag)  );
    //-----------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("run_number"   , "Run Number"  ,  &(m_outData->o_run_number)   );
    m_outMngrTree->AddStandardBranch("event_number" , "event_number",  &(m_outData->o_event_number) );
    m_outMngrTree->AddStandardBranch("data_period"  , "Data Period" ,  &(m_outData->o_period)       );
    m_outMngrTree->AddStandardBranch("pileup_mu"    , "<#mu>"       ,  &(m_outData->o_pileup_mu)    );
    m_outMngrTree->AddStandardBranch("npv"          , "N_{PV}"      ,  &(m_outData->o_npv)          );
    m_outMngrTree->AddStandardBranch("channel"      , "Channel Type",  &(m_outData->o_channel_type) );
    //-----------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("jets_n" , "Number of jets"  ,  &(m_outData->o_jets_n)  );
    m_outMngrTree->AddStandardBranch("bjets_n", "Number of b-jets",  &(m_outData->o_bjets_n) );
    //-----------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("jets_pt"    , "Jets  p_{T} [GeV]"  ,  &(m_outData->o_jets), -1, "Pt"       );
    m_outMngrTree->AddStandardBranch("jets_m"     , "Jets Mass [GeV]"    ,  &(m_outData->o_jets), -1, "M"        );
    m_outMngrTree->AddStandardBranch("jets_eta"   , "Jets #eta"          ,  &(m_outData->o_jets), -1, "Eta"      );
    m_outMngrTree->AddStandardBranch("jets_y"     , "Jets rapidity"      ,  &(m_outData->o_jets), -1, "Rapidity" );
    m_outMngrTree->AddStandardBranch("jets_phi"   , "Jets #phi"          ,  &(m_outData->o_jets), -1, "Phi"      );
    m_outMngrTree->AddStandardBranch("jets_btagw" , "Jets MV2c20 weight" ,  &(m_outData->o_jets), -1, "btagw"    );
    m_outMngrTree->AddStandardBranch("jets_jvt"   , "Jets JVT weight"    ,  &(m_outData->o_jets), -1, "jvt"      );
    //-----------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("el_n" , "Number of electrons",  &(m_outData->o_el_n)  );
    m_outMngrTree->AddStandardBranch("mu_n" , "Number of muons"    ,  &(m_outData->o_mu_n)  );
    m_outMngrTree->AddStandardBranch("lep_n", "Number of leptons"  ,  &(m_outData->o_lep_n) );
    //------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("el1_pt"      , "Electron 1 p_{T} [GeV]" ,  &(m_outData->o_el), 0, "Pt"      );
    m_outMngrTree->AddStandardBranch("el1_eta"     , "Electron 1 #eta"        ,  &(m_outData->o_el), 0, "Eta"     );
    m_outMngrTree->AddStandardBranch("el1_phi"     , "Electron 1 #phi"        ,  &(m_outData->o_el), 0, "Phi"     );
    //------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("mu1_pt"      , "Muon 1 p_{T} [GeV]" ,  &(m_outData->o_mu), 0, "Pt"      );
    m_outMngrTree->AddStandardBranch("mu1_eta"     , "Muon 1 #eta"        ,  &(m_outData->o_mu), 0, "Eta"     );
    m_outMngrTree->AddStandardBranch("mu1_phi"     , "Muon 1 #phi"        ,  &(m_outData->o_mu), 0, "Phi"     );
    //------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("lep1_pt"     , "Lepton 1 p_{T} [GeV]" , &(m_outData->o_lep), 0, "Pt"    );
    m_outMngrTree->AddStandardBranch("lep1_eta"    , "Lepton 1 #eta"        , &(m_outData->o_lep), 0, "Eta"   );
    m_outMngrTree->AddStandardBranch("lep1_phi"    , "Lepton 1 #phi [GeV]"  , &(m_outData->o_lep), 0, "Phi"   );
    //-------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("meff"   , "Effective mass"    , &(m_outData->o_meff)              );
    m_outMngrTree->AddStandardBranch("met"    , "Missing E_{T}"     , &(m_outData->o_AO_met), -1, "Pt"  );
    m_outMngrTree->AddStandardBranch("met_phi", "#phi_{MET}"        , &(m_outData->o_AO_met), -1, "Phi" );
    m_outMngrTree->AddStandardBranch("mtw"    , "Transverse W mass" , &(m_outData->o_mtwl)              );
    m_outMngrTree->AddStandardBranch("ptw"    , "Transverse W p_{T}", &(m_outData->o_ptwl)              );
    m_outMngrTree->AddStandardBranch("hthad"  , "H_{T}^{had}"       , &(m_outData->o_hthad)             );
    //-------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("dRmin_ejets" , "#DeltaR_{min}(e, jets)"     , &(m_outData->o_dRmin_ejets)   );
    m_outMngrTree->AddStandardBranch("dRmin_mujets", "#DeltaR_{min}(#mu, jets)"   , &(m_outData->o_dRmin_mujets)  );
    m_outMngrTree->AddStandardBranch("dRmin_jetjet", "#DeltaR_{min}(jet, jet)"    , &(m_outData->o_dRmin_jetjet)  );
    m_outMngrTree->AddStandardBranch("dRmin_bb"    , "#DeltaR_{min}(b-jet, b-jet)", &(m_outData->o_dRmin_bjetbjet));
    //-------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("mbb_mindr"    , "m_{inv}^{min#DeltaR}"        , &(m_outData->o_mbb_mindR)    );
    m_outMngrTree->AddStandardBranch("dPhi_lepmet"  , "#Delta#phi(lep, MET)"        , &(m_outData->o_dPhi_lepmet)  );
    m_outMngrTree->AddStandardBranch("dPhi_jetmet"  , "#Delta#phi_{min}(jet, MET)"  , &(m_outData->o_dPhi_jetmet)  );
    m_outMngrTree->AddStandardBranch("dPhi_lepjet"  , "#Delta#phi_{min}(lep, jet)"  , &(m_outData->o_dPhi_lepjet)  );
    m_outMngrTree->AddStandardBranch("dPhi_lepbjet" , "#Delta#phi_{min}(lep, b-jet)", &(m_outData->o_dPhi_lepbjet) );
    //-------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("dRmin_ebjets" , "#DeltaR_{min}(e, b-jets)"  , &(m_outData->o_dRmin_ebjets)  );
    m_outMngrTree->AddStandardBranch("dRmin_mubjets", "#DeltaR_{min}(#mu, b-jets)", &(m_outData->o_dRmin_mubjets) );
    m_outMngrTree->AddStandardBranch("mT_bmin"      , "m_{T}^{min}(b-jets, MET)"  , &(m_outData->o_mTbmin)        );
    //-------------------------------------------------------------------------------------------------------------------
    m_outMngrTree->AddStandardBranch("mbb_leading_bjets"    , "m_{inv} leading b-jets"     , &(m_outData->o_mbb_leading_bjets )   );
    m_outMngrTree->AddStandardBranch("mbb_maxdr"            , "m_{inv}^{max#DeltaR}"       , &(m_outData->o_mbb_maxdR)            );
    m_outMngrTree->AddStandardBranch("mbb_mindr"            , "m_{inv}^{min#DeltaR}"       , &(m_outData->o_mbb_mindR)            );
    m_outMngrTree->AddStandardBranch("mjb_mindr"            , "m_{jb}^{min#DeltaR}"        , &(m_outData->o_mjb_mindR)            );
    m_outMngrTree->AddStandardBranch("mjb_maxdr"            , "m_{jb}^{max#DeltaR}"        , &(m_outData->o_mjb_maxdR)            );
    m_outMngrTree->AddStandardBranch("mjb_leading_bjet"     , "m_{jb}^{Leading}"           , &(m_outData->o_mjb_leading_bjet)     );
    m_outMngrTree->AddStandardBranch("dPhi_bb_leading_bjets", "#Delta#phi(leading b-jets)" , &(m_outData->o_dPhibb_leading_bjets) );
    m_outMngrTree->AddStandardBranch("dPhi_bb_mindr"        , "#Delta#phi(b-jets mindr)"   , &(m_outData->o_dPhibb_mindR)         );
    m_outMngrTree->AddStandardBranch("dPhi_bb_maxdr"        , "#Delta#phi(b-jets maxdr)"   , &(m_outData->o_dPhibb_maxdR)         );
    m_outMngrTree->AddStandardBranch("dEta_bb_leading_bjets", "#Delta#eta(leading b-jets)" , &(m_outData->o_dEtabb_leading_bjets) );
    m_outMngrTree->AddStandardBranch("dEta_bb_mindr"        , "#Delta#eta(b-jets mindr)"   , &(m_outData->o_dEtabb_mindR)         );
    m_outMngrTree->AddStandardBranch("dEta_bb_maxdr"        , "#Delta#eta(b-jets maxdr)"   , &(m_outData->o_dEtabb_maxdR)         );
    m_outMngrTree->AddStandardBranch("m_jj_leading_jets"    , "m_{jj inv}^{leading jets}"  , &(m_outData->o_mjj_leading_jets)     );
    m_outMngrTree->AddStandardBranch("mjj_mindr"            , "m_{jj inv}^{min#DeltaR}"    , &(m_outData->o_mjj_mindR)            );
    m_outMngrTree->AddStandardBranch("mjj_maxdr"            , "m_{jjinv}^{max#DeltaR}"     , &(m_outData->o_mjj_maxdR)            );
    m_outMngrTree->AddStandardBranch("dPhi_jj_leading_bjets", "#Delta#phi(leading jets)"   , &(m_outData->o_dPhijj_leading_jets)  );
    m_outMngrTree->AddStandardBranch("dPhi_jj_mindr"        , "#Delta#phi(jets mindr)"     , &(m_outData->o_dPhijj_mindR)         );
    m_outMngrTree->AddStandardBranch("dPhi_jj_maxdr"        , "#Delta#phi(jets maxdr)"     , &(m_outData->o_dPhijj_maxdR)         );
    m_outMngrTree->AddStandardBranch("dEta_jj_leading_bjets", "#Delta#eta(leading jets)"   , &(m_outData->o_dEtajj_leading_jets)  );
    m_outMngrTree->AddStandardBranch("dEta_jj_mindr"        , "#Delta#eta(jets mindr)"     , &(m_outData->o_dEtajj_mindR)         );
    m_outMngrTree->AddStandardBranch("dEta_jj_maxdr"        , "#Delta#eta(jets maxdr)"     , &(m_outData->o_dEtajj_maxdR)         );
    m_outMngrTree->AddStandardBranch("mH"                   , "pseudo_mass"                , &(m_outData->o_pseudo_mass)          );
    //--------------------------------------------------------------------------------------------------------------------
    if(m_outData -> o_is_ttbar && m_opt->InputTree()=="nominal_Loose" )
      {
	m_outMngrTree->AddStandardBranch("nomWeight_ISR_UP"    , "nomWeight_ISR_UP"          , &(m_outData->o_nom_weight_ISR_UP)    );
	m_outMngrTree->AddStandardBranch("nomWeight_ISR_DOWN"  , "nomWeight_ISR_DOWN"        , &(m_outData->o_nom_weight_ISR_DOWN)  );
	m_outMngrTree->AddStandardBranch("nomWeight_FSR_UP"    , "nomWeight_FSR_UP"          , &(m_outData->o_nom_weight_FSR_UP)    );
	m_outMngrTree->AddStandardBranch("nomWeight_FSR_DOWN"  , "nomWeight_FSR_DOWN"        , &(m_outData->o_nom_weight_FSR_DOWN)  );
	//--------------------------------------------------------------------------------------------------------------------
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_1"    , "o_nom_weight_index_1"          , &(m_outData->o_nom_weight_index_1  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_2"    , "o_nom_weight_index_2"          , &(m_outData->o_nom_weight_index_2  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_3"    , "o_nom_weight_index_3"          , &(m_outData->o_nom_weight_index_3  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_4"    , "o_nom_weight_index_4"          , &(m_outData->o_nom_weight_index_4  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_5"    , "o_nom_weight_index_5"          , &(m_outData->o_nom_weight_index_5  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_6"    , "o_nom_weight_index_6"          , &(m_outData->o_nom_weight_index_6  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_7"    , "o_nom_weight_index_7"          , &(m_outData->o_nom_weight_index_7  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_8"    , "o_nom_weight_index_8"          , &(m_outData->o_nom_weight_index_8  )    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_193"  , "o_nom_weight_index_193"        , &(m_outData->o_nom_weight_index_193)    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_194"  , "o_nom_weight_index_194"        , &(m_outData->o_nom_weight_index_194)    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_198"  , "o_nom_weight_index_198"        , &(m_outData->o_nom_weight_index_198)    );
	m_outMngrTree->AddStandardBranch("o_nom_weight_index_199"  , "o_nom_weight_index_199"        , &(m_outData->o_nom_weight_index_199)    );
      }

    for ( int iJet=0; iJet<=5; ++iJet ) {
      std::string str_id = "";
      str_id += std::to_string(iJet);
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_btagw", "Jet"+str_id+" MV2c10"     ,  &(m_outData -> o_jets), iJet, "btagw"   );
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_pt"   , "Jet"+str_id+" p_{T} [GeV]",  &(m_outData -> o_jets), iJet, "Pt"      );
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_eta"  , "Jet"+str_id+" #Eta"       ,  &(m_outData -> o_jets), iJet, "Eta"     );
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_y"    , "Jet"+str_id+" rapidity"   ,  &(m_outData -> o_jets), iJet, "Rapidity");
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_phi"  , "Jet"+str_id+" #Phi"       ,  &(m_outData -> o_jets), iJet, "Phi"     );
      m_outMngrTree -> AddStandardBranch( "jet"+str_id+"_m"    , "Jet"+str_id+" Mass [GeV]" ,  &(m_outData -> o_jets), iJet, "M"       );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_btagw_discrete", "Jet"+str_id+" MV2c10 discrete"
					 , &(m_outData->o_btagw_discrete)      , iJet, "btagwdiscrete");
   }

    for (int iJet=0; iJet<=5; ++ iJet ) {
      std::string str_id = "";
      str_id += std::to_string(iJet);
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_btagw_bord"    , "Jet"+str_id+" MV2c10"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "btagw"        );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_pt_bord"       , "Jet"+str_id+" p_{T} [GeV]"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "Pt"           );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_eta_bord"      , "Jet"+str_id+" eta"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "Eta"          );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_y_bord"        , "Jet"+str_id+" rapidity"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "Rapidity"     );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_phi_bord"      , "Jet"+str_id+" #phi"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "Phi"          );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_m_bord"        , "Jet"+str_id+" Mass"
					 , &(m_outData -> o_jets_btagworder_4j), iJet, "M"            );
      m_outMngrTree -> AddStandardBranch("jet"+str_id+"_btagw_discrete_bord", "Jet"+str_id+" MV2c10 discrete"
					 , &(m_outData->o_btagw_discrete_bord)      , iJet, "btagwdiscrete");
    }
    if(m_opt->DumpIsoTree())
      {
	m_outMngrTree->AddStandardBranch("el_isoFixedCutLoose"         , ""     , &(m_outData->o_el), 0, "isoFixedCutLoose" 	         );
	m_outMngrTree->AddStandardBranch("el_isoFixedCutTight"         , ""     , &(m_outData->o_el), 0, "isoFixedCutTight" 	         );
	m_outMngrTree->AddStandardBranch("el_isoFixedCutTightTrackOnly", ""     , &(m_outData->o_el), 0, "isoFixedCutTightTrackOnly"     );
	m_outMngrTree->AddStandardBranch("el_isoGradient"              , ""     , &(m_outData->o_el), 0, "isoGradient" 	                 );
	m_outMngrTree->AddStandardBranch("el_isoGradientLoose"         , ""     , &(m_outData->o_el), 0, "isoGradientLoose" 	         );
	m_outMngrTree->AddStandardBranch("el_isoLoose"                 , ""     , &(m_outData->o_el), 0, "isoLoose" 		             );
	m_outMngrTree->AddStandardBranch("el_isoLooseTrackOnly"        , ""     , &(m_outData->o_el), 0, "isoLooseTrackOnly"             );

	m_outMngrTree->AddStandardBranch("mu_isoFixedCutLoose"         , ""     , &(m_outData->o_mu), 0, "isoFixedCutLoose" 	         );
	m_outMngrTree->AddStandardBranch("mu_isoFixedCutTightTrackOnly", ""     , &(m_outData->o_mu), 0, "isoFixedCutTightTrackOnly"     );
	m_outMngrTree->AddStandardBranch("mu_isoGradient"              , ""     , &(m_outData->o_mu), 0, "isoGradient" 	                 );
	m_outMngrTree->AddStandardBranch("mu_isoGradientLoose"         , ""     , &(m_outData->o_mu), 0, "isoGradientLoose" 	         );
	m_outMngrTree->AddStandardBranch("mu_isoLoose"                 , ""     , &(m_outData->o_mu), 0, "isoLoose" 		             );
	m_outMngrTree->AddStandardBranch("mu_isoLooseTrackOnly"        , ""     , &(m_outData->o_mu), 0, "isoLooseTrackOnly"             );
      }

    m_outMngrTree->BookStandardTree("tree", "small tree");

  }//DumpTree

      if(m_opt->DumpDLTree())
      {
	m_outMngrDLTree = new OutputTreeManager( m_opt, m_outData );
	m_outMngrDLTree->AddAllWeightBranches("nomWeight", m_weightMngr, true);

	m_outMngrDLTree->AddStandardBranch("jets_n" , "Number of jets"  ,  &(m_outData->o_jets_n)  );
	m_outMngrDLTree->AddStandardBranch("bjets_n", "Number of b-jets",  &(m_outData->o_bjets_n) );

	m_outMngrDLTree->AddStandardBranch("lep1_pt"     , "Lepton 1 p_{T} [GeV]" , &(m_outData->o_lep), 0, "Pt"    );
	m_outMngrDLTree->AddStandardBranch("lep1_eta"    , "Lepton 1 #eta"        , &(m_outData->o_lep), 0, "Eta"   );
	m_outMngrDLTree->AddStandardBranch("lep1_phi"    , "Lepton 1 #phi "  , &(m_outData->o_lep), 0, "Phi"   );
	m_outMngrDLTree->AddStandardBranch("lep1_e"    , "Lepton 1 #E [GeV]"  , &(m_outData->o_lep), 0, "E"   );

	m_outMngrDLTree->AddStandardBranch("met"    , "Missing E_{T}"     , &(m_outData->o_AO_met), -1, "Pt"  );
	m_outMngrDLTree->AddStandardBranch("met_phi", "#phi_{MET}"        , &(m_outData->o_AO_met), -1, "Phi" );
	m_outMngrDLTree->AddStandardBranch("mjb_mindr"            , "m_{jb}^{min#DeltaR}"        , &(m_outData->o_mjb_mindR)            );
	m_outMngrDLTree->AddStandardBranch("mjb_maxdr"            , "m_{jb}^{max#DeltaR}"        , &(m_outData->o_mjb_maxdR)            );
        m_outMngrDLTree->AddStandardBranch("mjb_leading_bjet"     , "m_{jb}^{Leading}"           , &(m_outData->o_mjb_leading_bjet)     );
	for ( int iJet=0; iJet<=5; ++iJet ) {
	  std::string str_id = "";
	  str_id += std::to_string(iJet);
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_btagw", "Jet"+str_id+" MV2c10"     ,  &(m_outData -> o_jets), iJet, "btagw"   );
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_pt"   , "Jet"+str_id+" p_{T} [GeV]",  &(m_outData -> o_jets), iJet, "Pt"      );
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_eta"  , "Jet"+str_id+" #Eta"       ,  &(m_outData -> o_jets), iJet, "Eta"     );
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_y"    , "Jet"+str_id+" rapidity"   ,  &(m_outData -> o_jets), iJet, "Rapidity");
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_phi"  , "Jet"+str_id+" #Phi"       ,  &(m_outData -> o_jets), iJet, "Phi"     );
	  m_outMngrDLTree -> AddStandardBranch( "jet"+str_id+"_m"    , "Jet"+str_id+" Mass [GeV]" ,  &(m_outData -> o_jets), iJet, "M"       );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_btagw_discrete", "Jet"+str_id+" MV2c10 discrete"
					     , &(m_outData->o_btagw_discrete)      , iJet, "btagwdiscrete");
	}
	for (int iJet=0; iJet<=5; ++ iJet ) {
	  std::string str_id = "";
	  str_id += std::to_string(iJet);
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_btagw_bord"    , "Jet"+str_id+" MV2c10"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "btagw"        );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_pt_bord"       , "Jet"+str_id+" p_{T} [GeV]"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "Pt"           );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_eta_bord"      , "Jet"+str_id+" eta"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "Eta"          );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_y_bord"        , "Jet"+str_id+" rapidity"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "Rapidity"     );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_phi_bord"      , "Jet"+str_id+" #phi"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "Phi"          );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_m_bord"        , "Jet"+str_id+" Mass"
					     , &(m_outData -> o_jets_btagworder_4j), iJet, "M"            );
	  m_outMngrDLTree -> AddStandardBranch("jet"+str_id+"_btagw_discrete_bord", "Jet"+str_id+" MV2c10 discrete"
					     , &(m_outData->o_btagw_discrete_bord)      , iJet, "btagwdiscrete");
	}
	m_outMngrDLTree->AddStandardBranch("mH"                   , "pseudo_mass"                , &(m_outData->o_pseudo_mass)          );

	m_outMngrDLTree->BookStandardTree("DLtree", "small tree for DL");
      } // DumpDLTree

  const bool otherVariables = m_opt -> MultipleVariables();

  if(m_opt->DumpHistos()){

    //Event variables
    m_outMngrHist -> AddStandardTH1( "npv"        ,  1, 0, 40        , ";N_{PV}"            , false         , &(m_outData -> o_npv)            );
    m_outMngrHist -> AddStandardTH1( "mu"         ,  1, 0, 40        , ";<#mu>"             , false         , &(m_outData -> o_pileup_mu)      );
    m_outMngrHist -> AddStandardTH1( "meff"       ,  20 , 0, 2500    , ";m_{eff} [GeV]"     , true          , &(m_outData -> o_meff)           );
    m_outMngrHist -> AddStandardTH1( "meff_zoom"  ,  20 , 0, 1500    , ";m_{eff} [GeV]"     , false         , &(m_outData -> o_meff)           );
    m_outMngrHist -> AddStandardTH1( "met"        ,  20 , 0, 1000    , ";E_{T}^{miss} [GeV]", false         , &(m_outData -> o_met)            );
    m_outMngrHist -> AddStandardTH1( "met_zoom"   ,  10 , 0, 300     , ";E_{T}^{miss} [GeV]", otherVariables, &(m_outData -> o_met)            );
    m_outMngrHist -> AddStandardTH1( "met_phi"    ,  0.2 , -3.5, 3.5 , ";#phi_{MET}"        , false         , &(m_outData->o_AO_met), -1, "Phi");
    m_outMngrHist -> AddStandardTH1( "mtw"        ,  20 , 0, 1000    , ";m_{T}(W) [GeV]"    , false         , &(m_outData -> o_mtwl)           );
    m_outMngrHist -> AddStandardTH1( "mtw_zoom"   ,  10 , 0, 300     , ";m_{T}(W) [GeV]"    , otherVariables, &(m_outData -> o_mtwl)           );
    m_outMngrHist -> AddStandardTH1( "ptw"        ,  20 , 0, 1000    , ";p_{T}(W) [GeV]"    , false         , &(m_outData -> o_ptwl)           );
    m_outMngrHist -> AddStandardTH1( "ptw_zoom"   ,  10 , 0, 300     , ";p_{T}(W) [GeV]"    , otherVariables, &(m_outData -> o_ptwl)           );
    m_outMngrHist -> AddStandardTH1( "hthad"      ,  20 , 0, 2000    , ";H_{T}^{had} [GeV]" , otherVariables, &(m_outData -> o_hthad)          );
    m_outMngrHist -> AddStandardTH1( "hthad_zoom" ,  20 , 0, 1000    , ";H_{T}^{had} [GeV]" , false         , &(m_outData -> o_hthad)          );

    m_outMngrHist -> AddStandardTH1( "truth_ht"   ,   25,  0, 3000 , ";Truth H_{T} [GeV]"                           , false         , &(m_outData -> o_truth_ht_filter )  );
    m_outMngrHist -> AddStandardTH1( "truth_met"  ,   25,  0, 1000 , ";Truth MET [GeV]"                             , false         , &(m_outData -> o_truth_met_filter ) );
    m_outMngrHist -> AddStandardTH1( "mtbmin"     ,   50,  0, 500  , ";m_{T}^{min}(b,MET)"                          , false         , &(m_outData->o_mTbmin)              );
    m_outMngrHist -> AddStandardTH1( "mtbmin_zoom",   25,  0, 250  , ";m_{T}^{min}(b,MET)"                          , otherVariables, &(m_outData->o_mTbmin)              );
    m_outMngrHist -> AddStandardTH1( "met_sig"    ,   0.5, 0, 50   , ";E_{T}^{miss}/#sqrt{H_{T}^{had}} [#sqrt{GeV}]", false         , &(m_outData -> o_met_sig)           );

    //Jet variables
    m_outMngrHist -> AddStandardTH1( "jets_n" ,  1, -2.5, 15.5 , ";Number of jets"   , otherVariables, &(m_outData -> o_jets_n)  );
    m_outMngrHist -> AddStandardTH1( "bjets_n",  1, -0.5, 8.5  , ";Number of b-jets" , otherVariables, &(m_outData -> o_bjets_n) );
    //std::vector<double> *edges= new std::vector<double> {-1.0, 0.11, 0.64, 0.83, 0.94, 1.0};
    std::vector<double> *edges= new std::vector<double>;
    double min_tag, max_tag;
    if(m_opt->BtagAlg() == "MV2c10"){
      edges->push_back(-1.0);
      edges->push_back(0.11);
      edges->push_back(0.64);
      edges->push_back(0.83);
      edges->push_back(0.94);
      edges->push_back(1.0);
      min_tag =-1.;
      max_tag = 1.;
    }
    // Setting the second bin edge a bit larger than the threshold, avoiding jet with btagging weight smaller than 1.27 filled into the second bin. (discrete btag weight=1.27 if btagw<1.27 in FCNC_VariableComputer.cxx)
    else if(m_opt->BtagAlg() == "DL1r"){
      edges->push_back(-10.0);
      edges->push_back(0.6651);
      edges->push_back(2.195);
      edges->push_back(3.245);
      edges->push_back(4.5649);
      edges->push_back(10.0);
      min_tag =-10.;
      max_tag = 10.;
    }
    else if(m_opt->BtagAlg() == "GN120220509"){
      edges->push_back(-10.0);
      edges->push_back(1.253);
      edges->push_back(2.602);
      edges->push_back(3.642);
      edges->push_back(5.135);
      edges->push_back(10.0);
      min_tag =-10.;
      max_tag = 10.;
    }
    else{ std::cout<<"Btagging algorithm can not be recognised!"; }
    for ( int iJet=-1; iJet<=5; ++iJet ) {
      std::string str_id = "";
      str_id += std::to_string(iJet);
      if(iJet==-1) str_id = "s";
      const bool DrawSyst = (iJet <= 0) && otherVariables;
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_pt"    ,  25 , 25, 1000  , ";Jet"+str_id+" p_{T} [GeV]", DrawSyst, &(m_outData -> o_jets), iJet, "Pt"      );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_eta"   ,  0.2, -3, 3     , ";Jet"+str_id+" #eta"       , DrawSyst, &(m_outData -> o_jets), iJet, "Eta"      );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_y"     ,  0.2, -3, 3     , ";Jet"+str_id+" rapidity"   , DrawSyst, &(m_outData -> o_jets), iJet, "Rapidity" );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_phi"   ,  0.2, -3.5, 3.5 , ";Jet"+str_id+" #varphi"    , false,    &(m_outData -> o_jets), iJet, "Phi"      );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_m"     ,  10, 0, 200     , ";Jet"+str_id+" mass [GeV]" , false,    &(m_outData -> o_jets), iJet, "M"        );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_btagw" ,  0.1,-1.1,1.1   , ";Jet"+str_id+" DL1r"     , false,    &(m_outData -> o_jets), iJet, "btagw"    );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_jvt"   ,  0.1,-1.1,1.1   , ";Jet"+str_id+" JVT"        , false,    &(m_outData -> o_jets), iJet, "jvt"      );
      m_outMngrHist -> AddStandardTH1( "jet"+str_id+"_btagw_discrete", edges   , ";Jet"+str_id+" DL1r discrete", false   , &(m_outData->o_btagw_discrete), iJet, "btagwdiscrete" );
    }
    for ( int iBJet=-1; iBJet<=3; ++iBJet ) {
      std::string str_id = "";
      str_id += std::to_string(iBJet);
      if(iBJet==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1( "bjet"+str_id+"_pt"   ,   25 , 25, 1000,  ";b-jet"+str_id+" p_{T} [GeV]",  false, &(m_outData -> o_bjets),  iBJet, "Pt"       );
      m_outMngrHist -> AddStandardTH1( "bjet"+str_id+"_eta"  ,   0.2, -3, 3   ,  ";b-jet"+str_id+" #eta"       ,  false, &(m_outData -> o_bjets),  iBJet, "Eta"      );
      m_outMngrHist -> AddStandardTH1( "bjet"+str_id+"_y"    ,   0.2, -3, 3   ,  ";b-jet"+str_id+" rapidity"   ,  false, &(m_outData -> o_bjets),  iBJet, "Rapidity" );
      m_outMngrHist -> AddStandardTH1( "bjet"+str_id+"_btagw",   0.1,min_tag,max_tag ,  ";b-jet"+str_id+" "+m_opt->BtagAlg()     ,  false, &(m_outData -> o_bjets),  iBJet, "btagw"    );
      m_outMngrHist -> AddStandardTH1( "bjet"+str_id+"_jvt"  ,   0.1,-1.1,1.1 ,  ";b-jet"+str_id+" JVT"        ,  false, &(m_outData -> o_bjets),  iBJet, "jvt"      );
    }
    for ( int iLJet=-1; iLJet<=4; ++iLJet  ) {
      std::string str_id = "";
      str_id += std::to_string(iLJet);
      if(iLJet==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1( "ljet"+str_id+"_pt"   ,   10 , 0, 500 , ";light-jet"+str_id+" p_{T} [GeV]",  false, &(m_outData -> o_ljets), iLJet, "Pt"       );
      m_outMngrHist -> AddStandardTH1( "ljet"+str_id+"_eta"  ,   0.2, -3, 3  , ";light-jet"+str_id+" #eta"       ,  false, &(m_outData -> o_ljets), iLJet, "Eta"      );
      m_outMngrHist -> AddStandardTH1( "ljet"+str_id+"_y"    ,   0.2, -3, 3  , ";light-jet"+str_id+" rapidity"   ,  false, &(m_outData -> o_ljets), iLJet, "Rapidity" );
      m_outMngrHist -> AddStandardTH1( "ljet"+str_id+"_btagw",   0.1,min_tag,max_tag, ";light-jet"+str_id+" "+m_opt->BtagAlg()     ,  false, &(m_outData -> o_ljets), iLJet, "btagw"    );
      m_outMngrHist -> AddStandardTH1( "ljet"+str_id+"_jvt"  ,   0.1,-1.1,1.1, ";light-jet"+str_id+" JVT"        ,  false, &(m_outData -> o_ljets), iLJet, "jvt"      );
    }
    for (int iBOJet=-1; iBOJet<=5; ++ iBOJet ) {
      std::string str_id = "";
      str_id += std::to_string(iBOJet);
      if(iBOJet==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_btagw_bord"    , 0.1, min_tag, max_tag, ";Jet"+str_id+" "+m_opt->BtagAlg()         , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "btagw"   );
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_pt_bord"       , 25, 25, 1000  , ";Jet"+str_id+" p_{T} [GeV]"    , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "Pt"      );
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_eta_bord"      , 0.2, -3, 3    , ";Jet"+str_id+" #eta"           , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "Eta"     );
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_y_bord"        , 0.2, -3, 3    , ";Jet"+str_id+" rapidity"       , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "Rapidity");
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_phi_bord"      , 0.2, -3.5, 3.5, ";Jet"+str_id+" #varphi"        , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "Phi"     );
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_m_bord"        , 10, 0, 200    , ";Jet"+str_id+" Mass [GeV]"     , false   , &(m_outData -> o_jets_btagworder_4j), iBOJet, "M"       );
      m_outMngrHist -> AddStandardTH1("jet"+str_id+"_btagw_discrete_bord", edges         , ";Jet"+str_id+" "+m_opt->BtagAlg()+" discrete", false   , &(m_outData->o_btagw_discrete_bord), iBOJet, "btagwdiscrete" );
    }


    //Electron variables
    m_outMngrHist -> AddStandardTH1( "el_n", 1, -0.5, 5.5, ";Number of signal electrons", false, &(m_outData -> o_el_n) );
    for ( int iEl=-1; iEl<=0; ++iEl ) {
      std::string str_id = "";
      str_id += std::to_string(iEl);
      if(iEl==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_pt"          ,  20 , 0, 800       , ";Electron p_{T} [GeV]"        ,  false, &(m_outData -> o_el), iEl, "Pt"           );
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_pt_zoom"     ,  10 , 0, 500       , ";Electron p_{T} [GeV]"        ,  false, &(m_outData -> o_el), iEl, "Pt"           );
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_eta"         ,  0.2, -3, 3        , ";Electron #eta"               ,  false, &(m_outData -> o_el), iEl, "Eta"          );
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_phi"         ,  0.2, -4, 4        , ";Electron #phi"               ,  false, &(m_outData -> o_el), iEl, "Phi"          );
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_ptvarcone20" ,  0.01, 0, 0.1      , ";Electron ptvarcone20/p_{T}"  ,  false, &(m_outData -> o_el), iEl, "ptvarcone20"  );
      m_outMngrHist -> AddStandardTH1( "el"+str_id+"_topoetcone20",  0.01, 0, 0.1      , ";Electron topoetcone20/p_{T}" ,  false, &(m_outData -> o_el), iEl, "topoetcone20" );
    }

    //Muon variables
    m_outMngrHist -> AddStandardTH1( "mu_n", 1, -0.5, 5.5, ";Number of signal muons", false, &(m_outData -> o_mu_n) );
    for ( int iMu=-1; iMu<=0; ++iMu ) {
      std::string str_id = "";
      str_id += std::to_string(iMu);
      if(iMu==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_pt"          ,  20 , 0, 800     , ";Muon p_{T} [GeV]"         , false,  &(m_outData -> o_mu), iMu, "Pt"           );
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_pt_zoom"     ,  10 , 0, 500     , ";Muon p_{T} [GeV]"         , false,  &(m_outData -> o_mu), iMu, "Pt"           );
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_eta"         ,  0.2, -3, 3      , ";Muon #eta"                , false,  &(m_outData -> o_mu), iMu, "Eta"          );
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_phi"         ,  0.2, -4, 4      , ";Muon #phi"                , false,  &(m_outData -> o_mu), iMu, "Phi"          );
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_ptvarcone30" ,  0.01, 0, 0.2    , ";Muon ptvarcone30/p_{T}"   , false,  &(m_outData -> o_mu), iMu, "ptvarcone30"  );
      m_outMngrHist -> AddStandardTH1( "mu"+str_id+"_topoetcone20",  0.01, 0, 0.2    , ";Muon topoetcone20/p_{T}"  , false,  &(m_outData -> o_mu), iMu, "topoetcone20" );
    }

    //Lepton variables
    m_outMngrHist -> AddStandardTH1( "lep_n", 1, -0.5, 5.5, ";Number of signal leptons", false, &(m_outData -> o_lep_n) );
    for ( int iLep=-1; iLep<=0; ++iLep ) {
      std::string str_id = "";
      str_id += std::to_string(iLep);
      if(iLep==-1) str_id = "s";
      m_outMngrHist -> AddStandardTH1( "lep"+str_id+"_pt"     ,  20 , 0, 800     , ";Lepton p_{T} [GeV]"     ,  false,           &(m_outData -> o_lep), iLep, "Pt"    );
      m_outMngrHist -> AddStandardTH1( "lep"+str_id+"_pt_zoom",  10 , 10, 500    , ";Lepton p_{T} [GeV]"     ,  otherVariables,  &(m_outData -> o_lep), iLep, "Pt"    );
      m_outMngrHist -> AddStandardTH1( "lep"+str_id+"_eta"    ,  0.2, -3, 3      , ";Lepton #eta"            ,  otherVariables,  &(m_outData -> o_lep), iLep, "Eta"   );
      m_outMngrHist -> AddStandardTH1( "lep"+str_id+"_phi"    ,  0.2, -4, 4      , ";Lepton #phi"            ,  false,           &(m_outData -> o_lep), iLep, "Phi"   );
    }

    //Kinematic variables
    m_outMngrHist -> AddStandardTH1( "dR_ejet"    ,  0.25,0,5,  ";#DeltaR_{min}(e,jet)"         , false, &(m_outData -> o_dRmin_ejets)     );
    m_outMngrHist -> AddStandardTH1( "dR_mujet"   ,  0.25,0,5,  ";#DeltaR_{min}(#mu,jet)"       , false, &(m_outData -> o_dRmin_mujets)    );
    m_outMngrHist -> AddStandardTH1( "dR_jetjet"  ,  0.25,0,5,  ";#DeltaR_{min}(jet,jet)"       , false, &(m_outData -> o_dRmin_jetjet)    );
    m_outMngrHist -> AddStandardTH1( "dR_bjetbjet",  0.25,0,5,  ";#DeltaR_{min}(b-jet,b-jet)"   , false, &(m_outData -> o_dRmin_bjetbjet)  );
    m_outMngrHist -> AddStandardTH1( "dR_ebjet"   ,  0.25,0,5,  ";#DeltaR_{min}(e,b-jet)"       , false, &(m_outData -> o_dRmin_ebjets)    );
    m_outMngrHist -> AddStandardTH1( "dR_mubjet"  ,  0.25,0,5,  ";#DeltaR_{min}(#mu,b-jet)"     , false, &(m_outData -> o_dRmin_mubjets)   );

    m_outMngrHist -> AddStandardTH1( "dPhi_lepmet" ,     0.1,-4,4 ,  ";#Delta#Phi(MET,lep)"         ,  false, &(m_outData -> o_dPhi_lepmet)  );
    m_outMngrHist -> AddStandardTH1( "dPhi_jetmet" ,     0.1,-4,4 ,  ";#Delta#Phi^{min}(MET,jet)"   ,  false, &(m_outData -> o_dPhi_jetmet)  );
    m_outMngrHist -> AddStandardTH1( "dPhi_jetmet5",     0.1,0,4  ,  ";#Delta#Phi^{min}(MET,jet)"   ,  false, &(m_outData -> o_dPhi_jetmet5) );
    m_outMngrHist -> AddStandardTH1( "dPhi_jetmet6",     0.1,0,4  ,  ";#Delta#Phi^{min}(MET,jet)"   ,  false, &(m_outData -> o_dPhi_jetmet6) );
    m_outMngrHist -> AddStandardTH1( "dPhi_jetmet7",     0.1,0,4  ,  ";#Delta#Phi^{min}(MET,jet)"   ,  false, &(m_outData -> o_dPhi_jetmet7) );
    m_outMngrHist -> AddStandardTH1( "dPhi_lepjet" ,     0.1,-4,4 ,  ";#Delta#Phi^{min}(lep,jet)"   ,  false, &(m_outData -> o_dPhi_lepjet)  );
    m_outMngrHist -> AddStandardTH1( "dPhi_lepbjet",     0.1,-4,4 ,  ";#Delta#Phi^{min}(lep,b-jets)",  false, &(m_outData -> o_dPhi_lepbjet) );

    m_outMngrHist -> AddStandardTH1( "mbb_mindR",  10,0,1000,  ";m_{bb}^{min #DeltaR} [GeV]" , otherVariables, &(m_outData -> o_mbb_mindR)  );

    m_outMngrHist->AddStandardTH1("jets40_n"                 ,   1, -0.5, 15.5,";Number of jets with p_{T}>40 GeV",false          , &(m_outData->o_jets40_n )                 );
    m_outMngrHist->AddStandardTH1("centrality"               ,   0.01, 0, 1   ,";Centrality"                      ,false          , &(m_outData->o_centrality )               );
    m_outMngrHist->AddStandardTH1("mbb_leading_bjets"        ,   20, 0, 1000  ,";m_{b,b}^{leading b-jets} [GeV]"  ,false          , &(m_outData->o_mbb_leading_bjets )        );
    m_outMngrHist->AddStandardTH1("mbb_softest_bjets"        ,   20, 0, 2000  ,";m(b,b) softests b-jets [GeV]"    ,false          , &(m_outData->o_mbb_softest_bjets )        );
    m_outMngrHist->AddStandardTH1("J_lepton_invariant_mass"  ,   2, 0, 2000   ,";m(leading-J,lepton) [GeV]"       ,false          , &(m_outData->o_J_lepton_invariant_mass)   );
    m_outMngrHist->AddStandardTH1("J_leadingb_invariant_mass",   2, 0, 2000   ,";m(leading-J,leading-b) [GeV]"    ,false          , &(m_outData->o_J_leadingb_invariant_mass) );
    m_outMngrHist->AddStandardTH1("J_J_invariant_mass"       ,   2, 0, 2000   ,";m(leading-J,subleading-J) [GeV]" ,false          , &(m_outData->o_J_J_invariant_mass)        );
    m_outMngrHist->AddStandardTH1("dRaverage_bjetbjet"       ,   0.25,0,5     ,";#DeltaR_{ave.}(b-jet,b-jet)"     ,false          , &(m_outData -> o_dRaverage_bjetbjet)      );
    m_outMngrHist->AddStandardTH1("dRaverage_jetjet"         ,   0.25,0,5     ,";#DeltaR_{ave.}(jet,jet)"         ,false          , &(m_outData -> o_dRaverage_jetjet)        );
    m_outMngrHist->AddStandardTH1("mbb_maxdR"                ,   20,0,1000    ,";m_{bb}^{max #DeltaR} [GeV]"      ,otherVariables , &(m_outData -> o_mbb_maxdR)               );
    m_outMngrHist->AddStandardTH1("mjb_mindR"                ,   10,0,500     ,";m_{jb}^{min #DeltaR} [GeV]"      ,otherVariables , &(m_outData -> o_mjb_mindR)               );
    m_outMngrHist->AddStandardTH1("mjb_maxdR"                ,   10,0,500     ,";m_{jb}^{max #DeltaR} [GeV]"      ,otherVariables , &(m_outData -> o_mjb_maxdR)               );
    m_outMngrHist->AddStandardTH1("mjb_leading_bjet"         ,   10,0,500     ,";m_{jb}^{Leading} [GeV]"          ,otherVariables , &(m_outData -> o_mjb_leading_bjet)        );
    m_outMngrHist->AddStandardTH1("dPhi_bb_leading_bjets"    ,   0.1,0,4      ,";#Delta#Phi_{bb}^{leading b-jets}",false          , &(m_outData -> o_dPhibb_leading_bjets)    );
    m_outMngrHist->AddStandardTH1("dPhi_bb_mindR"            ,   0.1,0,4      ,";#Delta#Phi_{bb}^{min #DeltaR}"   ,false          , &(m_outData -> o_dPhibb_mindR)            );
    m_outMngrHist->AddStandardTH1( "dPhi_bb_maxdR"           ,   0.1,0,4      ,";#Delta#Phi_{bb}^{ max #DeltaR}"  ,false          , &(m_outData -> o_dPhibb_maxdR)            );
    m_outMngrHist->AddStandardTH1("dEta_bb_leading_bjets"    ,   0.5,-5,5     ,";#Delta#eta_{bb}^{leading b-jets}",false          , &(m_outData -> o_dEtabb_leading_bjets)    );
    m_outMngrHist->AddStandardTH1("dEta_bb_mindR"            ,   0.5,-5,5     ,";#Delta#eta_{bb}^{min #DeltaR}"   ,false          , &(m_outData -> o_dEtabb_mindR)            );
    m_outMngrHist->AddStandardTH1("dEta_bb_maxdR"            ,   0.5,-5,5     ,";#Delta#eta_{bb}^{max #DeltaR}"   ,false          , &(m_outData -> o_dEtabb_maxdR)            );
    m_outMngrHist->AddStandardTH1("mjj_maxdR"                ,   10,0,1000    ,";m_{jj}^{max #DeltaR} [GeV]"      ,otherVariables , &(m_outData -> o_mjj_maxdR)               );
    m_outMngrHist->AddStandardTH1("dPhi_jj_leading_bjets"    ,   0.1,0,4      ,";#Delta#Phi_{jj}^{leading jets}"  ,false          , &(m_outData -> o_dPhijj_leading_jets)     );
    m_outMngrHist->AddStandardTH1("dPhi_jj_mindR"            ,   0.1,0,4      ,";#Delta#Phi_{jj}^{min #DeltaR}"   ,false          , &(m_outData -> o_dPhijj_mindR)            );
    m_outMngrHist->AddStandardTH1("dPhi_jj_maxdR"            ,   0.1,0,4      ,";#Delta#Phi_{jj}^{max #DeltaR}"   ,false          , &(m_outData -> o_dPhijj_maxdR)            );
    m_outMngrHist->AddStandardTH1("dEta_jj_leading_bjets"    ,   0.5,-5,5     ,";#Delta#eta_{jj}^{leading jets}"  ,false          , &(m_outData -> o_dEtajj_leading_jets)     );
    m_outMngrHist->AddStandardTH1("dEta_jj_mindR"            ,   0.5,-5,5     ,";#Delta#eta_{jj}^{min #DeltaR}"   ,false          , &(m_outData -> o_dEtajj_mindR)            );
    m_outMngrHist->AddStandardTH1("dEta_jj_maxdR"            ,   0.5,-5,5     ,";#Delta#eta_{jj}^{max #DeltaR}"   ,false          , &(m_outData -> o_dEtajj_maxdR)            );
    m_outMngrHist->AddStandardTH1("mjj_mindR"                ,   20,0,1000    ,";m_{jj}^{min #DeltaR} [GeV]"      ,false          , &(m_outData -> o_mjj_mindR)               );
    m_outMngrHist->AddStandardTH1("mjj_leading_jets"         ,   20,0,1000    ,";m_{jj}^{leading jets} [GeV]"     ,false          , &(m_outData -> o_mjj_leading_jets)        );

  }//if dumpHistos

  // Initialisation of the Selector and WeightManager
  m_selector->Init(); //This adds all selection regions and books histograms in each of them
  if(m_opt->MsgLevel()==Debug::DEBUG){
    m_selector->PrintSelectionTree();
  }

  //m_weightMngr -> Init( m_selector -> GetSelectionTree() );
  m_weightMngr -> Init();

  // Declaration of the TruthManager
  if( ( m_outData -> o_is_ttbar && m_opt->InputTree()=="nominal_Loose" ) ){
    m_truthMngr = new FCNC_TruthManager( m_opt, m_ntupData );
  } else {
    cout<<"FCNC_Analysis::Begin: skipping truth part (FCNC-specific)"<<endl;
  }

  return true;
}

//____________________________________________________________________________
//
bool FCNC_Analysis::Loop(){

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Entering in FCNC_Analysis::Loop()" << std::endl;

  int nEntries = m_reader -> ChainNEntries();
  for( int iEntry = 0; iEntry < nEntries; ++iEntry ){

    if(iEntry%10000==1) std::cout << "-> " << iEntry-1 << " / " << nEntries << " entries processed. " << std::endl;

    if(m_opt->SkipEvents()>-1 && iEntry < m_opt->SkipEvents()) continue;
    if(m_opt->NEvents()>-1 && iEntry > m_opt->NEvents()) continue;
    if(m_opt->PickEvent()>-1 && iEntry!=m_opt->PickEvent()) continue;
    this -> Process(iEntry);
  }

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Leaving FCNC_Analysis::Loop()" << std::endl;
  return true;
}

//____________________________________________________________________________
//
bool FCNC_Analysis::Process(Long64_t entry)
{
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "== Processing entry : " << entry << std::endl;

  // Put back all output variables to their initial value
  m_outData -> ClearOutputData();
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After clearing OutputData " << entry << std::endl;

  // Get entry in the input tree
  m_reader -> GetChainEntry(entry);
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After getting entry " << entry << std::endl;

  // C0: If this is ttbar, let's just select what we want
  if( m_outData -> o_is_ttbar ){

    const int &ttbar_HF     = m_ntupData -> d_HF_SimpleClassification;
    //const float &ht_truth   = m_ntupData -> d_HT_truth;
    //const float &met_truth  = m_ntupData -> d_MET_truth;

    // ttbar+HF splitting
    if( m_opt -> SampleName() == SampleName::TTBARBB ){
      if( ttbar_HF != 1 ) return false;
    }
    if( m_opt -> SampleName() == SampleName::TTBARCC ){
      if( ttbar_HF != -1 ) return false;
    }
    if( m_opt -> SampleName() == SampleName::TTBARLIGHT ){
      if( ttbar_HF != 0 ) return false;
    }

    /*
    if( m_opt -> SampleName() == SampleName::TTBARBB ){
      if( ttbar_HF <= 0 ) return false;
      const int classification = ( m_ntupData -> d_HF_Classification - ( m_ntupData -> d_HF_Classification % 100 ) ) / 100;
      //ttb
      if( (m_opt -> StrSampleName() == "TTBARBB_B") && classification != 10 ) return false;
      //ttbb
      if( (m_opt -> StrSampleName() == "TTBARBB_BB") && classification != 20 ) return false;
      //ttB
      if( (m_opt -> StrSampleName() == "TTBARBB_BIGB") && classification != 1 ) return false;
      //others
      if( (m_opt -> StrSampleName() == "TTBARBB_OTHERS") && (classification == 10 || classification == 20 || classification == 1) ) return false;
      //tt no B
      if( (m_opt -> StrSampleName() == "TTBARBB_NOBIGB") && classification == 1 ) return false;
    }
    if( m_opt -> SampleName() == SampleName::TTBARCC ){
      if( ttbar_HF >= 0 ) return false;
    }
    if( m_opt -> SampleName() == SampleName::TTBARLIGHT ){
      if( ttbar_HF != 0 ) return false;
    }
    */

    // ttbar HT/MET slicing
    /*
    m_outData -> o_truth_ht_filter = ht_truth;
    m_outData -> o_truth_met_filter = met_truth;
    if( m_opt -> FilterType() == FCNC_Options::APPLYFILTER ){
      if( m_ntupData -> d_runNumber == 410000 || m_ntupData -> d_runNumber == 410001 || m_ntupData -> d_runNumber == 410002 || m_ntupData -> d_runNumber == 410004 ){
        if( ht_truth > 600 || met_truth > 200 ) return false;
      } else if( m_ntupData -> d_runNumber == 407009 || m_ntupData -> d_runNumber == 407029 || m_ntupData -> d_runNumber == 407033 || m_ntupData -> d_runNumber == 407037 ){
        if( ht_truth < 600 || ht_truth > 1000 ) return false;
      } else if( m_ntupData -> d_runNumber == 407010 || m_ntupData -> d_runNumber == 407030 || m_ntupData -> d_runNumber == 407034 || m_ntupData -> d_runNumber == 407038  ){
        if( ht_truth < 1000 || ht_truth > 1500 ) return false;
      } else if( m_ntupData -> d_runNumber == 407011 || m_ntupData -> d_runNumber == 407031 || m_ntupData -> d_runNumber == 407035 || m_ntupData -> d_runNumber == 407039  ){
        if( ht_truth < 1500 ) return false;
      } else if( m_ntupData -> d_runNumber == 407012 || m_ntupData -> d_runNumber == 407032 || m_ntupData -> d_runNumber == 407036 || m_ntupData -> d_runNumber == 407040  ){
        if( met_truth < 200 || ht_truth > 600 ) return false;
      }
    }
    */

    // merging nominal inclusive ttbar sample with b-filter c-filter samples
    const int &HF_FilterFlag    = m_ntupData->d_TopHeavyFlavorFilterFlag;
    if(m_opt -> HFFilter() == FCNC_Options::APPLYFILTER ){

      if( m_ntupData -> d_mcChannelNumber == 410470 || m_ntupData -> d_mcChannelNumber == 410557 || m_ntupData -> d_mcChannelNumber == 410558 || m_ntupData -> d_mcChannelNumber == 410464 || m_ntupData -> d_mcChannelNumber == 410465 ){
	if( HF_FilterFlag != 0 ) return false;
      } else if( m_ntupData -> d_mcChannelNumber == 411073 || m_ntupData -> d_mcChannelNumber == 411076 || m_ntupData -> d_mcChannelNumber == 411082 || m_ntupData -> d_mcChannelNumber == 411085 || m_ntupData -> d_mcChannelNumber == 412066 || m_ntupData -> d_mcChannelNumber == 412069 ){
	if( HF_FilterFlag != 1 ) return false;
      } else if( m_ntupData -> d_mcChannelNumber == 411074 || m_ntupData -> d_mcChannelNumber == 411077 || m_ntupData -> d_mcChannelNumber == 411083 || m_ntupData -> d_mcChannelNumber == 411086 || m_ntupData -> d_mcChannelNumber == 412067 || m_ntupData -> d_mcChannelNumber == 412070 ){
	if( HF_FilterFlag != 2 ) return false;
      } else if( m_ntupData -> d_mcChannelNumber == 411075 || m_ntupData -> d_mcChannelNumber == 411078 || m_ntupData -> d_mcChannelNumber == 411084 || m_ntupData -> d_mcChannelNumber == 411087 || m_ntupData -> d_mcChannelNumber == 412068 || m_ntupData -> d_mcChannelNumber == 412071 ){
	if( HF_FilterFlag != 3 ) return false;
      }
    }

  }

  m_outData -> o_event_number = m_ntupData -> d_eventNumber;

  // Build object vectors

  //Cut flow, all events
  m_CutFlowTools->cutFlow("Cut-Flow","All" ,1.);

  const bool OKObjects = m_anaTools -> GetObjectVectors();
  if(!OKObjects){
    std::cerr << "<!> Problem => an error occured during the filling of object vectors" << std::endl;
    abort();
  }
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After building the objects" << std::endl;

  // C2: Lepton requirement

  AnalysisObject *lepton = 0;
  m_outData-> o_channel_type = FCNC_Enums::UNDEFINED;

  if ( (m_outData -> o_mu_n + m_outData -> o_el_n) == 1 )
    {

      if(m_opt -> StrSampleName().find("QCD") != std::string::npos)
	{
	  //This is QCD
	  if(m_opt -> SampleName() == SampleName::QCDE){//QCD in electron channel
	    if ( m_outData -> o_el_n == 1 ){
	      lepton = m_outData -> o_el -> at(0);
	      m_outData-> o_channel_type = FCNC_Enums::ELECTRON;
	    }
	  } else if(m_opt -> SampleName() == SampleName::QCDMU){//QCD in the muon channel
	    if( m_outData -> o_mu_n == 1 ){
	      lepton = m_outData -> o_mu -> at(0);
	      m_outData-> o_channel_type = FCNC_Enums::MUON;
	    }
	  }
	}//end QCD
      else
	{
	  //This is not QCD
	  if( m_outData -> o_mu_n == 1 ){
	    lepton = m_outData -> o_mu -> at(0);
	    m_outData-> o_channel_type = FCNC_Enums::MUON;
	    //Cut flow, muons
	    m_CutFlowTools->cutFlow("Cut-Flow","OneMu" ,1.);
	  } else if ( m_outData -> o_el_n == 1 ){
	    lepton = m_outData -> o_el -> at(0);
	    m_outData-> o_channel_type = FCNC_Enums::ELECTRON;
	    //Cut flow, electrons
	    m_CutFlowTools->cutFlow("Cut-Flow","OneEl" ,1.);
	  }
	}
      m_outData -> o_selLep = lepton;
    }
  else return false;

  if ( (m_outData -> o_mu_n + m_outData -> o_el_n) == 1 )
    {
    //Cut flow, one lepton
      m_CutFlowTools->cutFlow("Cut-Flow","OneLep" ,1.);
    }
  else return false;

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After lepton selection" << std::endl;

  // Refining channel definition
  bool isElectronChannel  = false;
  bool isMuonChannel      = false;

  isElectronChannel = ( m_outData-> o_channel_type == FCNC_Enums::ELECTRON );
  isMuonChannel     = ( m_outData-> o_channel_type == FCNC_Enums::MUON )    ;

  if( isElectronChannel  ){
    m_outData-> o_channel_type = FCNC_Enums::ELECTRON;
  } else if( isMuonChannel ){
    m_outData-> o_channel_type = FCNC_Enums::MUON;
  }

  // C4: MET/MTW cut to reject QCD in the 1L channel
  if( m_opt -> ApplyMetMtwCuts() && ( m_outData-> o_channel_type == FCNC_Enums::ELECTRON || m_outData-> o_channel_type == FCNC_Enums::MUON ) )
    {
      bool lowMETMTW = ( m_outData -> o_AO_met -> Pt() / 1000 < 20 ) || (m_outData -> o_AO_met -> Pt() / 1000 + (m_varComputer -> GetMTw( *(m_outData->o_el), *(m_outData->o_mu), m_outData -> o_AO_met )/1000) < 60 );
      if( (!m_opt -> InvertMetMtwCuts() && lowMETMTW) || (m_opt -> InvertMetMtwCuts() && !lowMETMTW) ){
	m_outData -> o_rejectEvent |= 1 << FCNC_Enums::METMTW_REJECTED;
	return false;
      }
      else
	{
	  //Cut flow, MET and MTW cuts
	  m_CutFlowTools->cutFlow("Cut-Flow","MET-MTW" ,1.);
	}
    }


  // C5: Jet requirement
  int min_nj = 4;
  if( m_outData -> o_jets_n < min_nj ) return false;
  else
    {
      //Cut flow, jets
      m_CutFlowTools->cutFlow("Cut-Flow","FourJets" ,1.);
    }

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After jets selection" << std::endl;


  // Reject the events not passing the pre-selection
  /*
  if( !m_opt -> DoCutFlow() && m_outData -> o_rejectEvent ){
    return false;
  }
  */

  // Compute all variables
  m_anaTools -> ComputeAllVariables();

  // Setting weights in WeightManager

  //Weight to applied to all events if not Data or QCD
  if ( !(m_opt -> IsData() || (m_opt -> StrSampleName().find("QCD") != std::string::npos)) ) {
    m_weightMngr -> SetCrossSectionWeight();
    //passing argument to get SF without MET trigger
    m_weightMngr -> SetLeptonSFWeights( false );

    if( m_outData -> o_is_ttbar ){
      if(m_opt->ScaleTtbarHtSlices()){
        m_weightMngr -> SetTtbarHtSliceScale();
      }
    }
  }

  // TRF
  if( !(m_opt -> IsData() || (m_opt -> StrSampleName().find("QCD") != std::string::npos)) && ( m_opt->RecomputeBtagSF() || ( m_opt -> DoTRF() && m_opt->RecomputeTRF() ) ) ){
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> Evaluating TRF with TRF Manager" << std::endl;
    m_TRFMngr->ComputeTRFWeights();
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After TRF manager" << std::endl;
  }

  // Compute all weights
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> Before setting the weight " << entry << std::endl;
  m_weightMngr -> ComputeAllWeights();
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After setting the weight " << entry << std::endl;


  //============================================ RUN SELECTION CHAIN HERE ======================================
  m_selector->RunSelectionChain();

  if(m_opt->DumpTree()){
    m_outData -> o_region -> clear();
    for( const std::pair<int, Selection*> &sel : *(m_selector->GetSelectionTree()) ){
      if((sel.second)->Decision()){
	m_outData -> o_region -> push_back((sel.second)->SelecInd());
      }
    }
    //c_4jin c_2bin (changed 3bin) (we start to train the MVA from this region)
    if(m_outData -> o_region -> size() && m_selector->PassSelection(m_selector->FCNCTopSels::c_2bin) && m_selector->PassSelection(m_selector->FCNCTopSels::c_4jin) ){
      m_outMngrTree->FillStandardTree("tree");
    }
  }

  if(m_opt->DumpDLTree()){
    m_outData -> o_region -> clear();
    for( const std::pair<int, Selection*> &sel : *(m_selector->GetSelectionTree()) ){
      if((sel.second)->Decision()){
	m_outData -> o_region -> push_back((sel.second)->SelecInd());
      }
    }
    //c_4jin c_2bin (changed 3bin) (we start to train the MVA from this region)
    if(m_outData -> o_region -> size() && m_selector->PassSelection(m_selector->FCNCTopSels::c_2bin) && m_selector->PassSelection(m_selector->FCNCTopSels::c_4jin) ){
      m_outMngrDLTree->FillStandardTree("DLtree");
    }
  }

  // Fills the histogram to remember about the rejection causes for events
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "== End of processing of entry : " << entry << std::endl;

  return true;
}

//____________________________________________________________________________
//
bool FCNC_Analysis::Terminate()
{

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "-> Entering in Terminate function." << std::endl;
  cout<<"Yield summary: "<<endl
  <<summaryYields()
  <<endl;
  //
  // Writting outputs
  //
  std::string nameHist = m_opt->OutputFile();
  if(m_outMngrHist){

    if(m_opt->DoSumRegions()){

      SumAnalysisRegions(true);
      for( const std::pair<int, Selection*> &sel : *(m_selector->GetSelectionTree()) ){
	if(sel.second->PassFlagAtBit(FCNC_Selector::FIT)){
	  m_outMngrHist -> SaveStandardTH1(nameHist, false, AnalysisUtils::ReplaceString(sel.second->Name(),"-",""));
	}
      }

    }
    else{
      m_outMngrHist -> SaveStandardTH1(nameHist);
      m_outMngrHist -> SaveStandardTH2(nameHist,false);
    }

  }
  if(m_outMngrOvlerlapTree){
    m_outMngrOvlerlapTree -> SaveStandardTree(AnalysisUtils::ReplaceString(nameHist, ".root", "_OVTREE.root"));
  }

  if(m_outMngrDLTree){
    m_outMngrDLTree -> SaveStandardTree(AnalysisUtils::ReplaceString(nameHist, ".root", "_DLTREE.root"));
  }

  if(m_outMngrTree){
    m_outMngrTree -> SaveStandardTree(AnalysisUtils::ReplaceString(nameHist, ".root", "_TREE.root"));
  }

  m_outData -> EmptyOutputData();
  m_CutFlowTools->printAllCutFlow();

  delete m_outData;

  delete m_outMngrHist;
  delete m_outMngrTree;

  delete m_weightMngr;
  delete m_TRFMngr;
  delete m_selector;

  delete m_CutFlowTools;

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "-> End of Terminate function." << std::endl;
  return true;
}

bool FCNC_Analysis::SumAnalysisRegions(const bool newFile){

  struct{
    void operator()(TH1D* hist, TH1D** target, const std::string& name){
      if(*target != NULL){ (*target)->Add(hist); }
      else{
	*target = (TH1D*)(hist->Clone(name.c_str()));
	(*target)->SetDirectory(0);
      }

      return;
    };
  } AddHistogramToTarget;

  std::vector<std::string> variableList = { };
  for( std::pair< std::string, OutputHistManager::h1Def* > hpair : *(m_outMngrHist->StdTH1Def()) ){

    if( hpair.second->hasSyst ){ variableList.push_back(hpair.first); }

  }

  if( m_opt->MsgLevel() == Debug::DEBUG ){
    for(const std::string& var : variableList){ std::cout << var << std::endl; }
  }


  std::vector<std::string> regList{};

  //Define FCNC regions


  std::map<std::string, std::vector<std::string> > targetPrintList = {
    {"sum0lep6jin2bin", {}},
    {"sum0lep7jin2bin", {}},
    {"sum0lep6jin3bin", {}},
    {"sum0lep7jin3bin", {}},

    {"sum0lep6jin3binLowMtbmin", {}},
    {"sum0lep7jin3binLowMtbmin", {}},

    {"sum1lep5jin3bin", {}},
    {"sum1lep6jin3bin", {}}

  };

  for(const std::string& region : regList){
    if(region.find("1lep") != std::string::npos)
      {
	targetPrintList.at("sum1lep5jin3bin").push_back(region);
	if(region.find("6jin") != std::string::npos)
	  {
	    targetPrintList.at("sum1lep6jin3bin").push_back(region);
	  }//6jin
      }//1L regions
  }

  if( m_opt->MsgLevel() == Debug::DEBUG ){
    //============== PRINT regions lists ===================
    for(std::pair<std::string, std::vector<std::string> > targetPair : targetPrintList){
      std::cout << " TARGET REGION : " <<  targetPair.first << " built from :: " <<std::endl;
      for(std::string source_reg : targetPair.second){
	std::cout<<source_reg<<std::endl;
      }
      std::cout<<std::endl<<std::endl;
    }
  }

  std::vector<std::string> lepchannels{""};
  if(m_opt->DoSplitEMu()){
    lepchannels.push_back("_el");
    lepchannels.push_back("_mu");
  }

  std::vector<std::string> wgtList{""};
  for(std::pair<std::string, WeightObject*> sysPair : *(m_weightMngr->SystMap()) ){
    wgtList.push_back("_"+sysPair.first);
  }

  std::string fileMode = newFile ? "RECREATE" : "UPDATE";
  TFile* outfile = TFile::Open( m_opt->OutputFile().c_str(), fileMode.c_str());

  for ( const std::string& variable : variableList ){

    for(std::string sys : wgtList){

      for(const std::string& channel : lepchannels){

	for(std::pair<std::string, std::vector<std::string> > targetPair : targetPrintList){

	  if(!channel.empty() && targetPair.first.find("1lep") == std::string::npos){ continue; }

	  TH1D* targethist = NULL;
	  for(std::string source_reg : targetPair.second){
	    TH1D* sourcehist = m_outMngrHist->HistMngr()->GetTH1D( (source_reg + channel + "_" + variable + sys).c_str() );
	    AddHistogramToTarget(sourcehist, &targethist, targetPair.first + channel + "_" + variable + sys);
	  }//source regions

	  outfile->cd();
	  if(targethist){
	    targethist->Write();
	    delete targethist;
	  }

	}//target regions

      }//lepton channels

    }//systematics + nominal

  }//variables

  wgtList.clear();
  regList.clear();
  targetPrintList.clear();

  outfile->Close();
  return true;
}

//____________________________________________________________________________
//
std::string FCNC_Analysis::summaryYields()
{
  struct {
    bool operator()(const string &histoname) {
      return (fcnc::utils::endswith(histoname, "meff") and not
      fcnc::utils::endswith(histoname, "_vs_meff"));
    }
  } isInterestingHistogram; // what I think is useful; this definition is to be discussed

  std::ostringstream oss;
  if(HistManager *hm = m_outMngrHist->HistMngr()) {
    const string cutflow_histoname = "cutFlow_unWeight";
    if(TH1D *h = hm->GetTH1D(cutflow_histoname)){
      const vector<double> bin_centers = {1, 2, 3, 4, 5, 6, 7, 8}; // avoid unused bins, see FCNC_Analysis::Loop
      oss<<cutflow_histoname
      <<" (docutflow = "<<m_opt -> DoCutFlow()<<"): ";
      for(auto v : bin_centers)
      oss<<" -> "<<h->GetBinContent(h->GetXaxis()->FindFixBin(v));
      oss<<"\n";
    } else {
      cout<<"FCNC_Analysis::summaryYields: Cannot retrieve '"<<cutflow_histoname<<"'"<<endl;
    }
    vector<string> all_th1_names = hm->GetTH1KeyList();
    vector<string> relevant_th1_names;
    relevant_th1_names.reserve(all_th1_names.size());
    copy_if(all_th1_names.begin(), all_th1_names.end(), std::back_inserter(relevant_th1_names), isInterestingHistogram);
    for(const string &hn : relevant_th1_names)
    if(TH1D *h = hm->GetTH1D(hn))
    oss<<hn<<": "<<h->Integral()<<"\n";
  } else {
    cout<<"FCNC_Analysis::summaryYields: cannot access hist manager."<<endl;
  }
  return oss.str();
}
