#include "FCNC-Analysis/FCNC_Options.h"

#include "IFAETopFramework/AnalysisUtils.h"
#include "IFAETopFramework/OptionsBase.h"

#include <iostream>
#include <string>
#include <stdexcept> // invalid_argument

using std::string;
using std::logic_error;

//______________________________________________________________________________
//
FCNC_Options::FCNC_Options():
OptionsBase(),
m_useLeptonsSF(true),
m_useBtagSF(false),
m_recomputeBtagSF(false),
m_usePileUpWeight(true),
m_dumpHistos(true),
m_dumpTree(true),
m_dumpDLTree(false),
m_dumpIsoTree(false),
m_dumpOverlapTree(false),
m_doTruthAnalysis(false),
m_doTRF(false),
m_doPerBinTRF(false),
m_recomputeTRF(false),
m_applyMetMtwCuts(false),
m_invertMetMtwCuts(false),
m_multipleVariablesWithUncertainties(false),
m_doCutFlow(true),
m_doLowBRegions(false),
m_doSplitEMu(true),
m_doSumRegions(false),
m_scaleTtbarHtSlices(false),
m_pflowjetAlg(true),
m_maxb(4),
m_jetsPtCut(25.),
m_jetsEtaCut(2.5),
m_lepPtCut(28.),
m_btagOP("60"),
m_btagAlg("GN120220509"),
m_LepIsoCut("Nominal"),
m_btagCollection(PFLOW),
m_TRFCDIPath("xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC20-CDI_Test_2023-08-1_v1.root"),
m_sampleDat("samples_test_mc20.dat"),
m_filterType(NOFILTER),
m_hffilter(NOFILTER)
{}

//_____________________________________________________________
//
FCNC_Options::FCNC_Options( const FCNC_Options &q ):
OptionsBase(q)
{
    m_useLeptonsSF       = q.m_useLeptonsSF;
    m_useBtagSF          = q.m_useBtagSF;
    m_recomputeBtagSF    = q.m_recomputeBtagSF;
    m_usePileUpWeight    = q.m_usePileUpWeight;
    m_dumpHistos         = q.m_dumpHistos;
    m_dumpTree           = q.m_dumpTree;
    m_dumpDLTree         = q.m_dumpDLTree;
    m_dumpIsoTree        = q.m_dumpIsoTree;
    m_dumpOverlapTree    = q.m_dumpOverlapTree;
    m_doTruthAnalysis    = q.m_doTruthAnalysis;
    m_doTRF              = q.m_doTRF;
    m_doPerBinTRF        = q.m_doPerBinTRF;
    m_recomputeTRF       = q.m_recomputeTRF;
    m_applyMetMtwCuts    = q.m_applyMetMtwCuts;
    m_invertMetMtwCuts   = q.m_invertMetMtwCuts;
    m_multipleVariablesWithUncertainties = q.m_multipleVariablesWithUncertainties;
    m_doCutFlow           = q.m_doCutFlow;
    m_doLowBRegions       = q.m_doLowBRegions;
    m_doSplitEMu          = q.m_doSplitEMu;
    m_doSumRegions        = q.m_doSumRegions;
    m_scaleTtbarHtSlices  = q.m_scaleTtbarHtSlices;
    m_jetsPtCut         = q.m_jetsPtCut;
    m_jetsEtaCut        = q.m_jetsEtaCut;
    m_lepPtCut          = q.m_lepPtCut;
    m_btagOP            = q.m_btagOP;
    m_btagAlg           = q.m_btagAlg;
    m_LepIsoCut         = q.m_LepIsoCut;
    m_TRFCDIPath        = q.m_TRFCDIPath;
    m_maxb              = q.m_maxb;
    m_sampleDat         = q.m_sampleDat;
    m_filterType        = q.m_filterType;
    m_hffilter          = q.m_hffilter;
    m_pflowjetAlg        = q.m_pflowjetAlg;
    m_btagCollection    = q.m_btagCollection;
}

//_____________________________________________________________
//
FCNC_Options::~FCNC_Options()
{}

//_____________________________________________________________
//
bool FCNC_Options::IdentifyOption ( const std::string &argument, const std::string &value ) {

    std::string temp_arg = argument;
    std::string temp_val = value;

    if(!OptionsBase::IdentifyOption(argument, value)){

        //
        // Boolean arguments
        //
        if( temp_arg.find("--USELEPTONSSF") != std::string::npos ){
            m_useLeptonsSF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--USEBTAGGINGSF") != std::string::npos ){
	         m_useBtagSF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--RECOMPUTEBTAGSF") != std::string::npos ){
            m_recomputeBtagSF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--USEPUWEIGHT") != std::string::npos ){
            m_usePileUpWeight = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DUMPHISTOS") != std::string::npos ){
            m_dumpHistos = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DUMPTREE") != std::string::npos ){
            m_dumpTree = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DUMPDLTREE") != std::string::npos ){
            m_dumpDLTree = AnalysisUtils::BoolValue(temp_val, temp_arg);
	}
	else if( temp_arg.find("--DUMPISOTREE") != std::string::npos ){
            m_dumpIsoTree = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DUMPOVERLAPTREE") != std::string::npos ){
            m_dumpOverlapTree = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOTRUTHANALYSIS") != std::string::npos ){
            m_doTruthAnalysis = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOTRF") != std::string::npos ){
            m_doTRF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOPERBINTRF") != std::string::npos ){
            m_doPerBinTRF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--RECOMPUTETRF") != std::string::npos ){
            m_recomputeTRF = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--APPLYMETMTWCUTS") != std::string::npos ){
            m_applyMetMtwCuts = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--INVERTMETMTWCUTS") != std::string::npos ){
            m_invertMetMtwCuts = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--OTHERVARIABLES") != std::string::npos ){
            m_multipleVariablesWithUncertainties = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOCUTFLOW") != std::string::npos ){
            m_doCutFlow     = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOLOWBREGIONS") != std::string::npos ){
            m_doLowBRegions = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOSPLITEMU") != std::string::npos ){
            m_doSplitEMu = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--DOSUMREGIONS") != std::string::npos ){
            m_doSumRegions = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
	else if( temp_arg.find("--SCALETTBARHTSLICES") != std::string::npos ){
            m_scaleTtbarHtSlices = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
    else if( temp_arg.find("--PFLOWJETALGO") != std::string::npos ){
           m_pflowjetAlg = AnalysisUtils::BoolValue(temp_val, temp_arg);
        }
        //
        // Float arguments
        //
        else if( temp_arg.find("--JETPTCUT") != std::string::npos ){
            m_jetsPtCut = atof(temp_val.c_str());
        }
        else if( temp_arg.find("--JETETACUT") != std::string::npos ){
            m_jetsEtaCut = atof(temp_val.c_str());
        }
        else if( temp_arg.find("--LEPPTCUT") != std::string::npos ){
            m_lepPtCut = atof(temp_val.c_str());
        }

        //
        // String arguments
        //
        else if( temp_arg.find("--BTAGOP") != std::string::npos ){
            m_btagOP = temp_val;
        }
        else if( temp_arg.find("--BTAGALG") != std::string::npos ){
            m_btagAlg = temp_val;
        }
        else if( temp_arg.find("--LEPISOCUT") != std::string::npos ){
            m_LepIsoCut = temp_val;
        } else if( temp_arg.find("--TRFCDI") != std::string::npos ){
            m_TRFCDIPath = temp_val;
        } else if( temp_arg.find("--SAMPLEDAT") != std::string::npos ){
            m_sampleDat = temp_val;
        }
        //
        // Int arguments
        //
        else if( temp_arg.find("--MAXTRFB") != std::string::npos ){
            m_maxb = atoi(temp_val.c_str());
        }
        //
        // Enum arguments
        //
        else if( temp_arg.find("--FILTERTYPE") != std::string::npos ){
            std::transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
            if ( temp_val.find("NOFILTER") != std::string::npos) 	m_filterType = NOFILTER;
            else if ( temp_val.find("APPLYFILTER") != std::string::npos) 	m_filterType = APPLYFILTER;
            else {
                std::cout<<"Unknown FILTERTYPE option : " << temp_val << std::endl;
            }
        }
      	else if( temp_arg.find("--HFFILTER") != std::string::npos ){
            std::transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
            if ( temp_val.find("NOFILTER") != std::string::npos) 	m_hffilter = NOFILTER;
            else if ( temp_val.find("APPLYFILTER") != std::string::npos) 	m_hffilter = APPLYFILTER;
            else {
                std::cout<<"Unknown HFFILTER option : " << temp_val << std::endl;
            }
        }

        else if( temp_arg.find("--BTAGCOLLECTION") != std::string::npos ){
            std::transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
            if ( temp_val.find("PFLOW") != std::string::npos) 	m_btagCollection = PFLOW;
            else if ( temp_val.find("EMTOPO") != std::string::npos) 	m_btagCollection = EMTOPO;
            else {
                std::cout<<"Unknown BTAGCOLLECTION option : " << temp_val << std::endl;
            }
        }
	else {
            return false;
        }
    }
    return true;
}

//_____________________________________________________________
//
void FCNC_Options::PrintOptions(){
    OptionsBase::PrintOptions();
    std::cout << "============== FCNC_Options ================="        << std::endl;
    std::cout << " m_jetsPtCut               = " << m_jetsPtCut         << std::endl;
    std::cout << " m_jetsEtaCut              = " << m_jetsEtaCut        << std::endl;
    std::cout << " m_lepPtCut                = " << m_lepPtCut          << std::endl;
    std::cout << " m_btagOP                  = " << m_btagOP            << std::endl;
    std::cout << " m_btagAlg                 = " << m_btagAlg           << std::endl;
    std::cout << " m_LepIsoCut               = " << m_LepIsoCut         << std::endl;
    std::cout << " m_useLeptonsSF            = " << m_useLeptonsSF      << std::endl;
    std::cout << " m_useBtagSF               = " << m_useBtagSF         << std::endl;
    std::cout << " m_recomputeBtagSF         = " << m_recomputeBtagSF   << std::endl;
    std::cout << " m_usePileUpWeight         = " << m_usePileUpWeight   << std::endl;
    std::cout << " m_doTruthAnalysis         = " << m_doTruthAnalysis   << std::endl;
    std::cout << " m_doTRF                   = " << m_doTRF             << std::endl;
    std::cout << " m_doPerBinTRF             = " << m_doPerBinTRF       << std::endl;
    std::cout << " m_recomputeTRF            = " << m_recomputeTRF      << std::endl;
    std::cout << " m_TRFCDIPath              = " << m_TRFCDIPath        << std::endl;
    std::cout << " m_maxb                    = " << m_maxb              << std::endl;
    std::cout << " m_applyMetMtwCuts         = " << m_applyMetMtwCuts   << std::endl;
    std::cout << " m_invertMetMtwCuts        = " << m_invertMetMtwCuts  << std::endl;
    std::cout << " m_dumpHistos              = " << m_dumpHistos        << std::endl;
    std::cout << " m_dumpTree                = " << m_dumpTree          << std::endl;
    std::cout << " m_dumpDLTree              = " << m_dumpDLTree        << std::endl;
    std::cout << " m_dumpIsoTree             = " << m_dumpIsoTree       << std::endl;
    std::cout << " m_dumpOverlapTree         = " << m_dumpOverlapTree   << std::endl;
    std::cout << " m_multipleVariablesWithUncertainties = " << m_multipleVariablesWithUncertainties << std::endl;
    std::cout << " m_doCutFlow               = " << m_doCutFlow         << std::endl;
    std::cout << " m_doLowBRegions           = " << m_doLowBRegions     << std::endl;
    std::cout << " m_doSplitEMu              = " << m_doSplitEMu        << std::endl;
    std::cout << " m_doSumRegions            = " << m_doSumRegions      << std::endl;
    std::cout << " m_scaleTtbarHtSlices      = " << m_scaleTtbarHtSlices<< std::endl;
    std::cout << " m_filterType              = " << m_filterType        << std::endl;
    std::cout << " m_hffilter                = " << m_hffilter           << std::endl;
    std::cout << " m_sampleDat               = " << m_sampleDat         << std::endl;
    std::cout << " m_pflowjetAlg              = " << m_pflowjetAlg      << std::endl;
    std::cout << " m_btagCollection          = " << m_btagCollection    << std::endl;
    std::cout << "============================================="        << std::endl;
    std::cout << "" << std::endl;
}

//_____________________________________________________________
//
string const bool2string(bool b)
{
    return b ? "true" : "false";
}

//_____________________________________________________________
//
void FCNC_Options::checkConcistency() const
{
  //Currently not used, to be implemented
  /*
  if(!m_useMETTrigger && !m_useLeptonTrigger && !m_useMETTriggerOneLep)
    throw logic_error(string(__FILE__)+"\n"
                      +" choose either one of the two triggers:"
                      +" useMETTrigger "+bool2string(m_useMETTrigger)+","
                      +" useLeptonTrigger "+bool2string(m_useLeptonTrigger)+","
                      +" useMETTriggerOneLep "+bool2string(m_useMETTriggerOneLep));
  if(m_doOneLeptonAna==false and m_doZeroLeptonAna==false)
    throw logic_error(string(__FILE__)+"\n"
                      +" choose at least one of the two analyses:"
                      +" doOneLeptonAna "+bool2string(m_doOneLeptonAna)+","
                      +" doZeroLeptonAna "+bool2string(m_doZeroLeptonAna));
  if(m_doFcncRegions==true and m_doOneLeptonAna==false) {
    throw logic_error(string(__FILE__)+"\n"
                      +" Fcnc requires one lep:"
                      +" m_doFcncRegions "+bool2string(m_doFcncRegions)+","
                      +" m_doOneLeptonAna "+bool2string(m_doOneLeptonAna));
  }
  // TODO implement other checks
  */

  if( m_btagOP!="85" && m_btagOP!="77" && m_btagOP!="70" && m_btagOP!="60")
    {
      std::cerr << "Working point "<< m_btagOP <<" is not supported"<< std::endl;
      exit(1);
    }

  if( m_btagAlg!="MV2c10" && m_btagAlg!="DL1r" && m_btagAlg!="GN120220509")
    {
      std::cerr << "Working point "<< m_btagAlg <<" is not supported"<< std::endl;
      exit(1);
    }

  if( m_LepIsoCut!="Nominal" && m_LepIsoCut!="isoFixedCutLoose" && m_LepIsoCut!="isoFixedCutTight" && m_LepIsoCut!="isoFixedCutTightTrackOnly" &&
      m_LepIsoCut!="isoGradient" && m_LepIsoCut!="isoGradientLoose" && m_LepIsoCut!="isoLoose" && m_LepIsoCut!="isoLooseTrackOnly" )
    {
      std::cerr << "Isolation working point "<< m_LepIsoCut <<" is not supported"<< std::endl;
      exit(1);
    }










}
