#include "IFAETopFramework/TriggerInfo.h"

#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_Enums.h"
#include "FCNC-Analysis/FCNC_NtupleReader.h"
#include "FCNC-Analysis/FCNC_NtupleData.h"

#include <iostream>

using namespace std;

//_________________________________________________________________________________
//
FCNC_NtupleReader::FCNC_NtupleReader(FCNC_Options* opt):
NtupleReader(opt),
m_opt(opt),
m_trigger_list(NULL)
{
  m_FCNC_ntupData = new FCNC_NtupleData();
  m_ntupData = dynamic_cast<NtupleData*>(m_FCNC_ntupData);
}

//_________________________________________________________________________________
//
FCNC_NtupleReader::~FCNC_NtupleReader()
{
  delete m_FCNC_ntupData;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetAllBranchAddresses()
{
  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetAllBranchAddresses()" << std::endl;

  stat += NtupleReader::SetAllBranchAddresses(); if(stat != 0){ std::cout << "Failed in " << std::endl; return stat; }
  stat += SetEventBranchAddresses(); if(stat != 0){ std::cout << "Failed in SetEventBranchAddresses" << std::endl; return stat; }
  stat += SetJetBranchAddresses(); if(stat != 0){ std::cout << "Failed in SetJetBranchAddresses" << std::endl; return stat; }
  stat += SetLeptonBranchAddresses(); if(stat != 0){ std::cout << "Failed in SetLeptonBranchAddresses" << std::endl; return stat; }
  stat += SetMETBranchAddresses(); if(stat != 0){ std::cout << "Failed in SetMETBranchAddresses" << std::endl; return stat; }
  if( m_opt -> InputTree() == "nominal_Loose" && m_opt -> IsData() == false ){
    stat += SetTruthParticleBranchAddresses(); if(stat != 0){ std::cout << "Failed in SetTruthParticleBranchAddresses" << std::endl; return stat; }
  }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving in FCNC_NtupleReader::SetAllBranchAddresses()" << std::endl;

  return stat;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetEventBranchAddresses(){

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetEventBranchAddresses()" << std::endl;

  //
  // Event variables (PU, ...)
  //
  stat =  SetVariableToChain("eventNumber",     &(m_FCNC_ntupData->d_eventNumber) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("runNumber",       &(m_FCNC_ntupData->d_runNumber) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mcChannelNumber",       &(m_FCNC_ntupData->d_mcChannelNumber) ); if(stat != 0){ return stat; }
  if(!(m_opt->IsData() || m_opt -> StrSampleName().find("QCD") != std::string::npos)){
    stat =  SetVariableToChain("randomRunNumber",         &(m_FCNC_ntupData->d_randomRunNumber) ); if(stat != 0){ return stat; }
    //Load the mc_generator_weights branch only if you run on the nominal tree, not available on systematics, use nominal_Loose, inputTree option
    if(m_opt->InputTree()=="nominal_Loose")
      stat =  SetVariableToChain("mc_generator_weights", &(m_FCNC_ntupData->d_mc_generator_weights) ); if(stat !=0) { return stat; }
  }
  stat =  SetVariableToChain("mu",     &(m_FCNC_ntupData->d_mu) ); if(stat != 0){ return stat; }

  if( m_opt -> SampleName() == SampleName::TTBAR || m_opt -> SampleName() == SampleName::TTBARBB ||
  m_opt -> SampleName() == SampleName::TTBARCC || m_opt -> SampleName() == SampleName::TTBARLIGHT ){

    // Ttbar+HF variables
    //followging SM 4tops implementation, implemented in online ntuple, split only between tt+b, tt+c, tt+l
    stat =  SetVariableToChain("HF_SimpleClassification", &(m_FCNC_ntupData->d_HF_SimpleClassification) ); if(stat != 0){ return stat; }
    //more complex categorisation to distinguish between the various tt+b components
    //stat =  SetVariableToChain("", &(m_FCNC_ntupData->d_HF_Classification) ); if(stat != 0){ return stat; }
    //b filter, c filter flag
    stat =  SetVariableToChain("TopHeavyFlavorFilterFlag", &(m_FCNC_ntupData->d_TopHeavyFlavorFilterFlag) ); if(stat != 0){ return stat;}
    // Truth information
    //stat =  SetVariableToChain("", &(m_FCNC_ntupData->d_HT_truth) ); if(stat != 0){ return stat; }
    //stat =  SetVariableToChain("", &(m_FCNC_ntupData->d_MET_truth) ); if(stat != 0){ return stat; }
  }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::setEventBranchAddresses()" << std::endl;

  return 0;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetJetBranchAddresses(const string &/*sj*/){

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetJetBranchAddresses()" << std::endl;

  //
  // Small-radius jets
  //
  stat =  SetVariableToChain("jet_pt", &(m_FCNC_ntupData->d_jet_pt) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("jet_phi", &(m_FCNC_ntupData->d_jet_phi) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("jet_eta", &(m_FCNC_ntupData->d_jet_eta) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("jet_e", &(m_FCNC_ntupData->d_jet_E) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("jet_jvt", &(m_FCNC_ntupData->d_jet_jvt) ); if(stat != 0){ return stat; }
  if(m_opt->BtagAlg()=="MV2c10") stat =  SetVariableToChain("jet_mv2c10", &(m_FCNC_ntupData->d_jet_btag_weight) ); if(stat != 0){ return stat; }
  if(m_opt->BtagAlg()=="DL1r")    stat =  SetVariableToChain("jet_DL1r", &(m_FCNC_ntupData->d_jet_btag_weight) ); if(stat != 0){ return stat; }
  if(m_opt->BtagAlg()=="GN120220509")    stat =  SetVariableToChain("jet_GN120220509", &(m_FCNC_ntupData->d_jet_btag_weight) ); if(stat != 0){ return stat; }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::setJetBranchAddresses()" << std::endl;

  return 0;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetLeptonBranchAddresses(){

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetLeptonBranchAddresses()" << std::endl;

  // Electron-related branches
  stat =  SetVariableToChain("el_pt"                       , &(m_FCNC_ntupData->d_el_pt                          ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("el_phi"                      , &(m_FCNC_ntupData->d_el_phi                         ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("el_eta"                      , &(m_FCNC_ntupData->d_el_eta                         ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("el_e"                        , &(m_FCNC_ntupData->d_el_e                           ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("el_ptvarcone20"              , &(m_FCNC_ntupData->d_el_ptvarcone20                 ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("el_topoetcone20"             , &(m_FCNC_ntupData->d_el_topoetcone20                ) ); if(stat != 0){ return stat; }
  if(m_opt->DumpIsoTree())
    {
      stat =  SetVariableToChain("el_isoFixedCutLoose"         , &(m_FCNC_ntupData->d_el_isoFixedCutLoose	         ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoFixedCutTight"         , &(m_FCNC_ntupData->d_el_isoFixedCutTight	         ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoFixedCutTightTrackOnly", &(m_FCNC_ntupData->d_el_isoFixedCutTightTrackOnly       ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoGradient"              , &(m_FCNC_ntupData->d_el_isoGradient		         ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoGradientLoose"         , &(m_FCNC_ntupData->d_el_isoGradientLoose	         ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoLoose"                 , &(m_FCNC_ntupData->d_el_isoLoose		         ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("el_isoLooseTrackOnly"        , &(m_FCNC_ntupData->d_el_isoLooseTrackOnly	         ) ); if(stat != 0){ return stat; }
    }

  // Muon-related branches
  stat =  SetVariableToChain("mu_pt"                       ,   &(m_FCNC_ntupData->d_mu_pt                          ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mu_phi"                      ,   &(m_FCNC_ntupData->d_mu_phi                         ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mu_eta"                      ,   &(m_FCNC_ntupData->d_mu_eta                         ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mu_e"                        ,   &(m_FCNC_ntupData->d_mu_e                           ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mu_ptvarcone30"              ,   &(m_FCNC_ntupData->d_mu_ptvarcone30                 ) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("mu_topoetcone20"             ,   &(m_FCNC_ntupData->d_mu_topoetcone20                ) ); if(stat != 0){ return stat; }
  if(m_opt->DumpIsoTree())
    {
      stat =  SetVariableToChain("mu_isoFixedCutLoose"         ,   &(m_FCNC_ntupData->d_mu_isoFixedCutLoose	       ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("mu_isoFixedCutTightTrackOnly",   &(m_FCNC_ntupData->d_mu_isoFixedCutTightTrackOnly   ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("mu_isoGradient"              ,   &(m_FCNC_ntupData->d_mu_isoGradient		       ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("mu_isoGradientLoose"         ,   &(m_FCNC_ntupData->d_mu_isoGradientLoose	       ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("mu_isoLoose"                 ,   &(m_FCNC_ntupData->d_mu_isoLoose		       ) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain("mu_isoLooseTrackOnly"        ,   &(m_FCNC_ntupData->d_mu_isoLooseTrackOnly           ) ); if(stat != 0){ return stat; }
    }
  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::setLeptonBranchAddresses()" << std::endl;

  return 0;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetMETBranchAddresses(){

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetMETBranchAddresses()" << std::endl;

  stat =  SetVariableToChain("met_met", &(m_FCNC_ntupData->d_met_met) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("met_phi", &(m_FCNC_ntupData->d_met_phi) ); if(stat != 0){ return stat; }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::SetMETBranchAddresses()" << std::endl;

  return 0;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetTruthParticleBranchAddresses(){

  if( m_opt -> SampleName() != SampleName::TTBAR && m_opt -> SampleName() != SampleName::TTBARBB &&
      m_opt -> SampleName() != SampleName::TTBARCC && m_opt -> SampleName() != SampleName::TTBARLIGHT ){
    return 0;
  }

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetTruthParticleBranchAddresses()" << std::endl;

  stat =  SetVariableToChain("truth_pt", &(m_FCNC_ntupData->d_mc_pt) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truth_eta", &(m_FCNC_ntupData->d_mc_eta) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truth_phi", &(m_FCNC_ntupData->d_mc_phi) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truth_m", &(m_FCNC_ntupData->d_mc_m) ); if(stat != 0){ return stat; }
  //stat =  SetVariableToChain("truth_pdgId", &(m_FCNC_ntupData->d_mc_pdgId) ); if(stat != 0){ return stat; }
  //std::cout << "FCNC_NtupleReader::SetTruthParticleBranchAddresses() dumpp setting to have the code working" << std::endl;
  //std::cout << "stat =  SetVariableToChain(\"\", &(m_FCNC_ntupData->d_mc_children_index) ); if(stat != 0){ return stat; }" << std::endl;
  //stat =  SetVariableToChain("", &(m_FCNC_ntupData->d_mc_children_index) ); if(stat != 0){ return stat; }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::SetTruthParticleBranchAddresses()" << std::endl;

  return 0;
}

//_________________________________________________________________________________
//
int FCNC_NtupleReader::SetTRFBranchAddresses(){

  int stat = 0;

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Entering in FCNC_NtupleReader::SetTRFBranchAddresses()" << std::endl;

  //Nominal weights
  stat =  SetVariableToChain("truthTagisB_2bex", &(m_FCNC_ntupData->d_trf_tagged_77_2ex) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truthTagisB_3bex", &(m_FCNC_ntupData->d_trf_tagged_77_3ex) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truthTagisB_4bin", &(m_FCNC_ntupData->d_trf_tagged_77_4in) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truthTagWei_Nominal_2bex", &(m_FCNC_ntupData->d_trf_weight_77_2ex) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truthTagWei_Nominal_3bex", &(m_FCNC_ntupData->d_trf_weight_77_3ex) ); if(stat != 0){ return stat; }
  stat =  SetVariableToChain("truthTagWei_Nominal_4bin", &(m_FCNC_ntupData->d_trf_weight_77_4in) ); if(stat != 0){ return stat; }

  //Systematic weights
  if(m_opt -> ComputeWeightSys()){
    //B-flavour
    for ( unsigned int iB = 0; iB <=5; ++iB ){
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1up_2bex", iB), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_B_up->at(iB)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1up_3bex", iB ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_B_up->at(iB)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1up_4bin", iB ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_B_up->at(iB)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1down_2bex", iB ), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_B_down->at(iB)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1down_3bex", iB ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_B_down->at(iB)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_B_%i__1down_4bin", iB ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_B_down->at(iB)) ); if(stat != 0){ return stat; }
    }//B-eigenvectors
    //C-flavour
    for ( unsigned int iC = 0; iC <=3; ++iC ){
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1up_2bex", iC ), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_C_up->at(iC)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1up_3bex", iC ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_C_up->at(iC)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1up_4bin", iC ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_C_up->at(iC)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1down_2bex", iC ), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_C_down->at(iC)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1down_3bex", iC ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_C_down->at(iC)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_C_%i__1down_4bin", iC ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_C_down->at(iC)) ); if(stat != 0){ return stat; }
    }//C-eigenvectors
    //Light-flavour
    for ( unsigned int iL = 0; iL <=16; ++iL ){
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1up_2bex", iL ), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_Light_up->at(iL)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1up_3bex", iL ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_Light_up->at(iL)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1up_4bin", iL ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_Light_up->at(iL)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1down_2bex", iL ), &(m_FCNC_ntupData->d_trf_weight_77_2ex_eigenvars_Light_down->at(iL)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1down_3bex", iL ), &(m_FCNC_ntupData->d_trf_weight_77_3ex_eigenvars_Light_down->at(iL)) ); if(stat != 0){ return stat; }
      stat =  SetVariableToChain(Form("truthTagWei_FT_EFF_Eigen_Light_%i__1down_4bin", iL ), &(m_FCNC_ntupData->d_trf_weight_77_4in_eigenvars_Light_down->at(iL)) ); if(stat != 0){ return stat; }
    }//Light-eigenvectors

    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1up_2bex", &(m_FCNC_ntupData->d_trf_weight_77_2ex_extrapolation_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1down_2bex", &(m_FCNC_ntupData->d_trf_weight_77_2ex_extrapolation_down) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1up_2bex", &(m_FCNC_ntupData->d_trf_weight_77_2ex_extrapolation_from_charm_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1down_2bex", &(m_FCNC_ntupData->d_trf_weight_77_2ex_extrapolation_from_charm_down) ); if(stat != 0){ return stat; }

    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1up_3bex", &(m_FCNC_ntupData->d_trf_weight_77_3ex_extrapolation_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1down_3bex", &(m_FCNC_ntupData->d_trf_weight_77_3ex_extrapolation_down) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1up_3bex", &(m_FCNC_ntupData->d_trf_weight_77_3ex_extrapolation_from_charm_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1down_3bex", &(m_FCNC_ntupData->d_trf_weight_77_3ex_extrapolation_from_charm_down) ); if(stat != 0){ return stat; }

    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1up_4bin", &(m_FCNC_ntupData->d_trf_weight_77_4in_extrapolation_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation__1down_4bin", &(m_FCNC_ntupData->d_trf_weight_77_4in_extrapolation_down) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1up_4bin", &(m_FCNC_ntupData->d_trf_weight_77_4in_extrapolation_from_charm_up) ); if(stat != 0){ return stat; }
    stat =  SetVariableToChain("truthTagWei_FT_EFF_extrapolation_from_charm__1down_4bin", &(m_FCNC_ntupData->d_trf_weight_77_4in_extrapolation_from_charm_down) ); if(stat != 0){ return stat; }

  }

  if(m_opt->MsgLevel()==Debug::DEBUG) std::cout << "Leaving FCNC_NtupleReader::SetTRFBranchAddresses()" << std::endl;

  return 0;
}
