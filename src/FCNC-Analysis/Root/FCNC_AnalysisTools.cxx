#include "FCNC-Analysis/FCNC_AnalysisTools.h"

#include "IFAETopFramework/OutputHistManager.h"
#include "IFAETopFramework/AnalysisObject.h"
#include "IFAETopFramework/AnalysisUtils.h"
#include "IFAETopFramework/TriggerInfo.h"

#include "FCNC-Analysis/FCNC_NtupleData.h"
#include "FCNC-Analysis/FCNC_Options.h"
#include "FCNC-Analysis/FCNC_OutputData.h"
#include "FCNC-Analysis/FCNC_TRFManager.h"
#include "FCNC-Analysis/FCNC_VariableComputer.h"
#include "FCNC-Analysis/FCNC_WeightManager.h"
#include "FCNC-Analysis/FCNC_Enums.h"

#include <iostream>
#include <utility> // pair

using std::cout;
using std::endl;
using std::pair;

//_________________________________________________________________________
//
FCNC_AnalysisTools::FCNC_AnalysisTools( FCNC_Options* opt, OutputHistManager* outMngr,
  const FCNC_NtupleData *m_ntupData, FCNC_OutputData* outData, FCNC_WeightManager *weightManager,
  FCNC_TRFManager* trfMngr, FCNC_VariableComputer *varCptr ):
m_opt(opt),
m_outputMngr(outMngr),
m_ntupData(m_ntupData),
m_outData(outData),
m_weightMngr(weightManager),
m_trfMngr(trfMngr),
m_varComputer(varCptr){
}

//_________________________________________________________________________
//
FCNC_AnalysisTools::FCNC_AnalysisTools( const FCNC_AnalysisTools &q )
{
  m_opt               = q.m_opt;
  m_outputMngr        = q.m_outputMngr;
  m_ntupData          = q.m_ntupData;
  m_outData           = q.m_outData;
  m_trfMngr           = q.m_trfMngr;
}

//_________________________________________________________________________
//
FCNC_AnalysisTools::~FCNC_AnalysisTools(){
}

//_________________________________________________________________________
//
bool FCNC_AnalysisTools::GetObjectVectors(){

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> Event number = " << m_ntupData -> d_eventNumber   << std::endl;

  //Leptons pTcut
  const double pTCut = m_opt -> LepPtCut();
  std::string iso_cut = m_opt -> LepIsoCut();

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // Electrons
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for ( unsigned int iEle = 0; iEle < m_ntupData -> d_el_pt -> size(); ++iEle ) {
    if( m_ntupData -> d_el_pt -> at(iEle) < pTCut ) continue;
    if( TMath::Abs(m_ntupData -> d_el_eta -> at(iEle)) > 2.47  ) continue;

    if(m_opt->DumpIsoTree())
      {
	if( iso_cut == "Nominal"	               && m_ntupData  -> d_el_isoFixedCutTight		->at(iEle) < 1 ) continue;
	if( iso_cut == "isoFixedCutLoose"	       && m_ntupData  -> d_el_isoFixedCutLoose		->at(iEle) < 1 ) continue;
	if( iso_cut == "isoFixedCutTight"	       && m_ntupData  -> d_el_isoFixedCutTight		->at(iEle) < 1 ) continue;
	if( iso_cut == "isoFixedCutTightTrackOnly"     && m_ntupData  -> d_el_isoFixedCutTightTrackOnly ->at(iEle) < 1 ) continue;
	if( iso_cut == "isoGradient"		       && m_ntupData  -> d_el_isoGradient		->at(iEle) < 1 ) continue;
	if( iso_cut == "isoGradientLoose"	       && m_ntupData  -> d_el_isoGradientLoose		->at(iEle) < 1 ) continue;
	if( iso_cut == "isoLoose"		       && m_ntupData  -> d_el_isoLoose			->at(iEle) < 1 ) continue;
	if( iso_cut == "isoLooseTrackOnly"             && m_ntupData  -> d_el_isoLooseTrackOnly         ->at(iEle) < 1 ) continue;
      }

    m_outData->o_el_loose_n++;
    m_outData->o_lep_loose_n++;

    if(m_opt -> MsgLevel() == Debug::DEBUG){
      std::cout << "==> In Electron builder:  pt " << m_ntupData -> d_el_pt->at(iEle);
      std::cout << "   eta " << m_ntupData -> d_el_eta -> at(iEle) << std::endl;
    }

    AnalysisObject *obj = new AnalysisObject();
    obj -> SetPtEtaPhiE( m_ntupData -> d_el_pt->at(iEle), m_ntupData -> d_el_eta -> at(iEle), m_ntupData -> d_el_phi -> at(iEle), m_ntupData -> d_el_e -> at(iEle) );
    obj -> SetMoment("ptvarcone20"  ,   m_ntupData -> d_el_ptvarcone20  -> at(iEle)/m_ntupData -> d_el_pt->at(iEle));
    obj -> SetMoment("topoetcone20" ,   m_ntupData -> d_el_topoetcone20 -> at(iEle));

    if(m_opt->DumpIsoTree())
      {
	obj -> SetMoment( "isoFixedCutLoose"          ,   m_ntupData -> d_el_isoFixedCutLoose	        -> at(iEle));
	obj -> SetMoment( "isoFixedCutTight"          ,   m_ntupData -> d_el_isoFixedCutTight	        -> at(iEle));
	obj -> SetMoment( "isoFixedCutTightTrackOnly" ,   m_ntupData -> d_el_isoFixedCutTightTrackOnly  -> at(iEle));
	obj -> SetMoment( "isoGradient"               ,   m_ntupData -> d_el_isoGradient		-> at(iEle));
	obj -> SetMoment( "isoGradientLoose"          ,   m_ntupData -> d_el_isoGradientLoose	        -> at(iEle));
	obj -> SetMoment( "isoLoose"                  ,   m_ntupData -> d_el_isoLoose		        -> at(iEle));
	obj -> SetMoment( "isoLooseTrackOnly"         ,   m_ntupData -> d_el_isoLooseTrackOnly          -> at(iEle));
      }

    m_outData -> o_el -> push_back(obj);
    m_outData -> o_lep -> push_back(obj);
  }
  m_outData -> o_el_n = m_outData -> o_el -> size();

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After filling electrons" << std::endl;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // Muons
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for ( unsigned int iMu = 0; iMu < m_ntupData -> d_mu_pt -> size(); ++iMu ) {

    if(m_opt -> MsgLevel() == Debug::DEBUG){
      std::cout << "==> In Muon builder:  pt " << m_ntupData  -> d_mu_pt-> at(iMu);
      std::cout << "   eta " << m_ntupData -> d_mu_eta -> at(iMu) << std::endl;
    }

    if( m_ntupData  -> d_mu_pt -> at(iMu) < pTCut  ) continue;
    if( TMath::Abs(m_ntupData  -> d_mu_eta -> at(iMu)) > 2.5 ) continue;

    if(m_opt->DumpIsoTree())
      {
	if( iso_cut == "Nominal"	               && m_ntupData  -> d_mu_isoFixedCutTightTrackOnly	->at(iMu) < 1 ) continue;
	if( iso_cut == "isoFixedCutLoose"	       && m_ntupData  -> d_mu_isoFixedCutLoose		->at(iMu) < 1 ) continue;
	//when running with isoFixedCutTight (only available in the el channel) apply nominal isolation cut in the muon channel
	if( iso_cut == "isoFixedCutTight"	       && m_ntupData  -> d_mu_isoFixedCutTightTrackOnly	->at(iMu) < 1 ) continue;
	if( iso_cut == "isoFixedCutTightTrackOnly"     && m_ntupData  -> d_mu_isoFixedCutTightTrackOnly ->at(iMu) < 1 ) continue;
	if( iso_cut == "isoGradient"		       && m_ntupData  -> d_mu_isoGradient		->at(iMu) < 1 ) continue;
	if( iso_cut == "isoGradientLoose"	       && m_ntupData  -> d_mu_isoGradientLoose		->at(iMu) < 1 ) continue;
	if( iso_cut == "isoLoose"		       && m_ntupData  -> d_mu_isoLoose			->at(iMu) < 1 ) continue;
	if( iso_cut == "isoLooseTrackOnly"             && m_ntupData  -> d_mu_isoLooseTrackOnly         ->at(iMu) < 1 ) continue;
      }

    m_outData->o_mu_loose_n++;
    m_outData->o_lep_loose_n++;

    //if(!(m_opt -> SampleName() == SampleName::QCDMU || m_ntupData  -> d_mu_isSignal -> at(iMu)) ) continue;

    AnalysisObject *obj = new AnalysisObject();
    obj -> SetPtEtaPhiE( m_ntupData -> d_mu_pt -> at(iMu), m_ntupData -> d_mu_eta -> at(iMu), m_ntupData -> d_mu_phi -> at(iMu), m_ntupData -> d_mu_e -> at(iMu) );
    //some more information to be stored in the outputs
    obj -> SetMoment( "ptvarcone30"  ,   m_ntupData -> d_mu_ptvarcone30     -> at(iMu) / m_ntupData -> d_mu_pt -> at(iMu));
    obj -> SetMoment( "topoetcone20" ,   m_ntupData -> d_mu_topoetcone20    -> at(iMu));

    if(m_opt->DumpIsoTree())
      {
	obj -> SetMoment( "isoFixedCutLoose"          ,   m_ntupData -> d_mu_isoFixedCutLoose	        -> at(iMu));
	obj -> SetMoment( "isoFixedCutTightTrackOnly" ,   m_ntupData -> d_mu_isoFixedCutTightTrackOnly  -> at(iMu));
	obj -> SetMoment( "isoGradient"               ,   m_ntupData -> d_mu_isoGradient		-> at(iMu));
	obj -> SetMoment( "isoGradientLoose"          ,   m_ntupData -> d_mu_isoGradientLoose	        -> at(iMu));
	obj -> SetMoment( "isoLoose"                  ,   m_ntupData -> d_mu_isoLoose		        -> at(iMu));
	obj -> SetMoment( "isoLooseTrackOnly"         ,   m_ntupData -> d_mu_isoLooseTrackOnly          -> at(iMu));
      }

    //trigger matching information
    /*
    for(TriggerInfo* trig : m_outData -> o_trigger_list){
      if( ! ( (trig->Type() == FCNC_Enums::TRIGMUON) && (m_outData->o_period & trig->Period()) ) ) continue;
      obj -> SetMoment( trig->Name(), m_ntupData->d_mu_trigMatch.at(trig->Name())->at(iMu) );
    }
    */

    m_outData -> o_mu -> push_back(obj);
    m_outData -> o_lep -> push_back(obj);
  }

  m_outData -> o_mu_n = m_outData -> o_mu -> size();
  m_outData -> o_lep_n = m_outData -> o_lep -> size();
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After filling muons" << std::endl;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // Lepton veto
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  m_outData -> o_lepForVeto_n = m_outData -> o_lep -> size();

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // Small-radius jets
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%

  for ( unsigned int iJet = 0; iJet < m_ntupData -> d_jet_pt -> size(); ++iJet ) {

    if(m_opt -> MsgLevel() == Debug::DEBUG){
      std::cout << "==> In the Jet builder:  pt " << m_ntupData -> d_jet_pt -> at(iJet);
      std::cout << "    eta " <<  TMath::Abs(m_ntupData -> d_jet_eta -> at(iJet))  << std::endl;
    }

    bool isSignalJet = m_ntupData -> d_jet_pt -> at(iJet)  >= m_opt->JetsPtCut();
    isSignalJet = isSignalJet && TMath::Abs( m_ntupData -> d_jet_eta -> at(iJet) ) < m_opt->JetsEtaCut();

    if( isSignalJet ){
      AnalysisObject *obj = new AnalysisObject();
      obj -> SetPtEtaPhiE( m_ntupData -> d_jet_pt -> at(iJet), m_ntupData -> d_jet_eta -> at(iJet), m_ntupData -> d_jet_phi -> at(iJet), m_ntupData -> d_jet_E -> at(iJet) );
      //based on release 21 wp https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21



     if(m_opt->BtagAlg()=="MV2c10"){
        if(m_opt->PFlowJetAlg()){//This is PFlow jets
         obj -> SetMoment("isb_60", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.94 ) );
	 obj -> SetMoment("isb_70", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.83 ) );
	 obj -> SetMoment("isb_77", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.63 ) );
	 obj -> SetMoment("isb_85", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.07 ) );
	}
	else{//this is EMtopo jets
	  obj -> SetMoment("isb_60", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.94 ) );
	  obj -> SetMoment("isb_70", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.83 ) );
	  obj -> SetMoment("isb_77", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.64 ) );
	  obj -> SetMoment("isb_85", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.11 ) );
	}
     }
     else if(m_opt->BtagAlg()=="DL1r"){
       if(m_opt->PFlowJetAlg()){//This is PFlow jets
	 obj -> SetMoment("isb_60", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 4.565 ) );
	 obj -> SetMoment("isb_70", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 3.245 ) );
	 obj -> SetMoment("isb_77", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 2.195 ) );
	 obj -> SetMoment("isb_85", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 0.665 ) );
       }
       else{//this is EMtopo jets
	 std::cerr << "Trying to access DL1r with EMTopo jets, not supported"<< std::endl;
	 exit(1);
       }
     }
     else if(m_opt->BtagAlg()=="GN120220509"){
       if(m_opt->PFlowJetAlg()){//This is PFlow jets
	 obj -> SetMoment("isb_60", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 5.135 ) );
	 obj -> SetMoment("isb_70", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 3.642 ) );
	 obj -> SetMoment("isb_77", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 2.602 ) );
	 obj -> SetMoment("isb_85", ( m_ntupData -> d_jet_btag_weight -> at(iJet) > 1.253 ) );
       }
       else{//this is EMtopo jets
	 std::cerr << "Trying to access GN1 with EMTopo jets, not supported"<< std::endl;
	 exit(1);
    }
	}
     obj -> SetMoment("btagw",    m_ntupData -> d_jet_btag_weight -> at(iJet) );

     obj -> SetMoment("jvt", m_ntupData -> d_jet_jvt -> at(iJet) );

     int isB = 0;
     if(TMath::Abs(obj->Eta())<2.5){
       if( obj -> GetMoment("isb_60") ){
	 if( m_opt -> BtagOP() == "60" ) isB = 1;
       }
       if( obj -> GetMoment("isb_70") ){
	 if( m_opt -> BtagOP() == "70" ) isB = 1;
       }
       if( obj -> GetMoment("isb_77") ){
	 if( m_opt -> BtagOP() == "77" ) isB = 1;
       }
       if( obj -> GetMoment("isb_85") ){
	 if( m_opt -> BtagOP() == "85" ) isB = 1;
       }
     }
     obj -> SetMoment("bjet",isB);
     if( isB == 1 ){
       m_outData -> o_bjets -> push_back(obj);
     } else {
       m_outData -> o_ljets -> push_back(obj);
     }
     m_outData -> o_jets -> push_back(obj);
    }
  }//loop over all jets

  //ordering first 4 jets by btagw
  AOVector jets_btagworder_4j = AnalysisUtils::SortObjectValues(*(m_outData->o_jets), "btagw", (m_outData->o_jets) -> size());
  m_outData -> o_jets_btagworder_4j -> insert ( m_outData -> o_jets_btagworder_4j -> begin(), jets_btagworder_4j.begin(), jets_btagworder_4j.end() );
  //If doing low-b regions, fill the low-b fake b's
  if( m_opt->DoLowBRegions() ){
    AOVector bjets_lowb_3b = AnalysisUtils::SortObjectValues(*(m_outData->o_jets), "btagw", 3 );
    AOVector bjets_lowb_4b = AnalysisUtils::SortObjectValues(*(m_outData->o_jets), "btagw", 4 );
    m_outData -> o_bjets_lowb_3b -> insert( m_outData -> o_bjets_lowb_3b -> begin(), bjets_lowb_3b.begin(), bjets_lowb_3b.end()  );
    m_outData -> o_bjets_lowb_4b -> insert( m_outData -> o_bjets_lowb_4b -> begin(), bjets_lowb_4b.begin(), bjets_lowb_4b.end()  );
  }
  m_outData -> o_jets_n       = m_outData -> o_jets  -> size();
  m_outData -> o_bjets_n      = m_outData -> o_bjets -> size();
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "==> After filling jets: signalJets vector  " << std::endl;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // MET
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%

  m_outData -> o_AO_met = new AnalysisObject();
  m_outData -> o_AO_met -> SetPtEtaPhiM( m_ntupData -> d_met_met, 0, m_ntupData -> d_met_phi, 0. );

  if(m_opt -> MsgLevel() == Debug::DEBUG){

    std::cout << "==============================" << std::endl;
    std::cout << "Summary of selected objects" << std::endl;
    std::cout << "==============================" << std::endl;
    std::cout << std::endl;
    std::cout << "----- Electrons -----" << std::endl;
    for ( const AnalysisObject* ele : *(m_outData->o_el) ) {
      std::cout << "   -> pt: " << ele->Pt() << "     eta: " << ele -> Eta() << std::endl;
    }
    std::cout << std::endl;
    std::cout << "----- Muons -----" << std::endl;
    for ( const AnalysisObject* muon : *(m_outData->o_mu) ) {
      std::cout << "   -> pt: " << muon->Pt() << "     eta: " << muon -> Eta() << std::endl;
    }
    std::cout << std::endl;
    std::cout << "----- Jets -----" << std::endl;
    for ( const AnalysisObject* jet : *(m_outData->o_jets) ) {
      std::cout << "   -> pt: " << jet->Pt() << "     eta: " << jet->Eta() << "    isB60: " << jet->GetMoment("isb_60");
      std::cout << "    isB70: " << jet->GetMoment("isb_70")  << "   isB85: " <<  jet->GetMoment("isb_85") << std::endl;
    }
    std::cout << std::endl;
    std::cout << "----- Reclustered jets -----" << std::endl;
  }
  return true;
}


//##########################################################
//
// Histograms utilities (handling of FCNC classification)
//
//##########################################################
//________________________________________________________________________
//
void FCNC_AnalysisTools::BookAllHistograms( const std::string &key/*, const bool split*/, const bool useSyst ){

  std::string name = key;
  m_outputMngr -> BookStandardTH1(name, useSyst);
  m_outputMngr -> BookStandardTH2(name, useSyst);

}

//________________________________________________________________________
//
void FCNC_AnalysisTools::FillAllHistograms( const std::string &key ){
    m_outputMngr -> FillStandardTH1(key);
    m_outputMngr -> FillStandardTH2(key);
}

//________________________________________________________________________
//
void FCNC_AnalysisTools::BookAllTH1DHistogramsHistManager( /*const bool split,*/ const std::string &name, const std::string &title, double binsize, double xlow, double xup, const std::string &key, const std::string &xtitle, const std::string &ytitle,int lw, int lc){

  std::string newname = name;
  m_outputMngr -> HistMngr() -> BookTH1D(newname,title,binsize,xlow,xup,key,xtitle,ytitle,lw,lc);
}

//________________________________________________________________________
//
void FCNC_AnalysisTools::BookAllTH1DHistogramsHistManager( /*const bool split,*/ const std::string &name, const std::string &title, int nbins, double* xedges, const std::string &key, const std::string &xtitle, const std::string &ytitle,int lw, int lc){

    std::string newname = name;
    m_outputMngr -> HistMngr() -> BookTH1D(newname,title,nbins,xedges,key,xtitle,ytitle,lw,lc);
}

//________________________________________________________________________
//
void FCNC_AnalysisTools::FillAllTH1DHistogramsHistManager( const std::string &key,double val, double wgt ){

    m_outputMngr -> HistMngr() -> FillTH1D(key,val,wgt);
}

//____________________________________________________________________________________
//
//c.f. issue https://gitlab.cern.ch/htx/FCNC-Analysis/issues/15
bool FCNC_AnalysisTools::UpdateRegionDependentWeight( ){
  //this line might be removed afterwards
  //m_weightMngr -> SetTtbarGeneratorSystematics(region_name);
  m_weightMngr -> ComputeAllWeights();
  return true;
}

//____________________________________________________________________________________
//
bool FCNC_AnalysisTools::PassBTagRequirement( const int btag_req, const bool isIncl ){

  if( !m_opt -> DoTRF() || m_opt->IsData() ||
      (m_opt -> StrSampleName().find("QCD") != std::string::npos) ){
    if(!isIncl){
      if( m_outData->o_bjets_n != btag_req ) return false;
      return true;
    } else {
      if( m_outData->o_bjets_n < btag_req ) return false;
      return true;
    }
  } else {
    if( ( m_outData -> o_TRF_bjets_n == btag_req ) && ( m_outData -> o_TRF_isIncl == isIncl ) ){
      //the TRF weights are already computed in this configuration ... not need to spend time redoing it
      return true;
    }

    // Calling the UpdateBTagging functon from TRFManager allows to update the
    // TRFWeigths to the proper ones, clear and refill the m_outData->o_bjets
    // vector with the current permutation.
    m_trfMngr -> UpdateBTagging( isIncl, btag_req );
    // The compute BTagVariables function is designed to compute all the
    // variables needed by the user that depend on b-tagged jets, to make sure
    // the proper permutation is indeed used when calling PassBTagRequirement.
    this -> ComputeBTagVariables();
    // Updating the OutputData to contain the latest loaded b-tagging configuration
    m_outData -> o_TRF_bjets_n = btag_req;
    m_outData -> o_TRF_isIncl = isIncl;
  }
  return true;
}

//____________________________________________________________________________________
//
bool FCNC_AnalysisTools::ComputeAllVariables(){
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Entering in FCNC_AnalysisTools::ComputeAllVariables()" << std::endl;

  //
  // Filling the output variables (kinematics)
  //
  m_outData -> o_TopHeavyFlavorFilterFlag = m_ntupData -> d_TopHeavyFlavorFilterFlag;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_TopHeavyFlavorFilterFlag (" << m_outData -> o_TopHeavyFlavorFilterFlag << ")"  << std::endl;
  m_outData -> o_pileup_mu    = m_ntupData    -> d_mu;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_pileup_mu (" << m_outData -> o_pileup_mu << ")"  << std::endl;
  m_outData -> o_meff         = m_varComputer -> GetMeff( *(m_outData->o_jets), *(m_outData->o_el), *(m_outData->o_mu), m_outData->o_AO_met );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_meff (" << m_outData -> o_meff << ")"  << std::endl;
  m_outData -> o_met          = m_outData     -> o_AO_met -> Pt();
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_met (" << m_outData -> o_met << ")"  << std::endl;
  m_outData -> o_mtwl         = m_varComputer -> GetMTw( *(m_outData->o_el), *(m_outData->o_mu), m_outData->o_AO_met );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mtwl (" << m_outData -> o_mtwl << ")"  << std::endl;
  m_outData -> o_ptwl         = m_varComputer -> GetPTw( *(m_outData->o_el), *(m_outData->o_mu), m_outData->o_AO_met );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_ptwl (" << m_outData -> o_ptwl << ")"  << std::endl;
  m_outData -> o_hthad        = m_varComputer -> GetHtHad( *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_hthad (" << m_outData -> o_hthad << ")"  << std::endl;
  m_outData -> o_met_sig      = m_varComputer -> GetMetSignificance( m_outData->o_AO_met->Pt(), m_outData -> o_hthad );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_met_sig (" << m_outData -> o_met_sig << ")"  << std::endl;

  m_outData -> o_dRmin_ejets  = m_varComputer -> GetMindR( *(m_outData->o_jets), *(m_outData->o_el) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_ejets (" << m_outData -> o_dRmin_ejets << ")"  << std::endl;
  m_outData -> o_dRmin_mujets = m_varComputer -> GetMindR( *(m_outData->o_jets), *(m_outData->o_mu) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_mujets (" << m_outData -> o_dRmin_mujets << ")"  << std::endl;
  m_outData -> o_dRmin_jetjet = m_varComputer -> GetMindR( *(m_outData->o_jets), *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_jetjet (" << m_outData -> o_dRmin_jetjet << ")"  << std::endl;
  m_outData -> o_dPhi_lepmet  = m_outData -> o_selLep ? m_outData -> o_selLep -> DeltaPhi( *(m_outData->o_AO_met) ) : -1;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_lepmet (" << m_outData -> o_dPhi_lepmet << ")"  << std::endl;
  m_outData -> o_dPhi_jetmet  = TMath::Abs(m_varComputer -> GetMindPhi( m_outData->o_AO_met, *(m_outData->o_jets), 4 ));
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_jetmet (" << m_outData -> o_dPhi_jetmet << ")"  << std::endl;
  m_outData -> o_dPhi_jetmet5  = TMath::Abs(m_varComputer -> GetMindPhi( m_outData->o_AO_met, *(m_outData->o_jets), 5 ));
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_jetmet (" << m_outData -> o_dPhi_jetmet5 << ")"  << std::endl;
  m_outData -> o_dPhi_jetmet6  = TMath::Abs(m_varComputer -> GetMindPhi( m_outData->o_AO_met, *(m_outData->o_jets), 6 ));
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_jetmet (" << m_outData -> o_dPhi_jetmet6 << ")"  << std::endl;
  m_outData -> o_dPhi_jetmet7  = TMath::Abs(m_varComputer -> GetMindPhi( m_outData->o_AO_met, *(m_outData->o_jets), 7 ));
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_jetmet (" << m_outData -> o_dPhi_jetmet7 << ")"  << std::endl;
  m_outData -> o_dPhi_lepjet  = m_varComputer -> GetMindPhi( m_outData->o_selLep, *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_lepjet (" << m_outData -> o_dPhi_lepjet << ")"  << std::endl;
  m_outData -> o_jets40_n  = m_varComputer -> GetNjets40GeV( *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_jets40_n (" << m_outData -> o_jets40_n << ")"  << std::endl;
  m_outData -> o_centrality = m_varComputer -> GetCentrality( *(m_outData->o_jets), *(m_outData->o_el), *(m_outData->o_mu) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_centrality (" << m_outData -> o_centrality << ")"  << std::endl;
  m_outData ->  o_dRaverage_jetjet = m_varComputer -> GetAveragedR( *(m_outData->o_jets), *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_dRaverage_jetjet  (" << m_outData ->  o_dRaverage_jetjet << ")"  << std::endl;
  m_outData -> o_mjj_leading_jets = m_varComputer -> GetMjjLeadingJets( *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_mjj_leading_jets (" << m_outData -> o_mjj_leading_jets << ")" << std::endl;
  m_outData -> o_mjj_maxdR = m_varComputer -> GetMjjMaxDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_mjj_maxdR (" << m_outData -> o_mjj_maxdR << ")" << std::endl;
  m_outData -> o_mjj_mindR = m_varComputer -> GetMjjMinDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_mjj_mindR (" << m_outData -> o_mjj_mindR << ")" << std::endl;
  m_outData -> o_dPhijj_leading_jets = m_varComputer -> GetDphijjLeadingJets( *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhijj_leading_jets (" << m_outData -> o_dPhijj_leading_jets << ")" << std::endl;
  m_outData -> o_dPhijj_mindR = m_varComputer -> GetDphijjMinDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhijj_mindR (" << m_outData -> o_dPhijj_mindR << ")" << std::endl;
  m_outData -> o_dPhijj_maxdR = m_varComputer -> GetDphijjMaxDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhijj_maxdR (" << m_outData -> o_dPhijj_maxdR << ")" << std::endl;
  m_outData -> o_dEtajj_leading_jets = m_varComputer -> GetDetajjLeadingJets( *(m_outData->o_jets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtajj_leading_jets (" << m_outData -> o_dEtajj_leading_jets << ")" << std::endl;
  m_outData -> o_dEtajj_mindR = m_varComputer -> GetDetajjMinDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtajj_mindR (" << m_outData -> o_dEtajj_mindR << ")" << std::endl;
  m_outData -> o_dEtajj_maxdR = m_varComputer -> GetDetajjMaxDr( *(m_outData->o_jets) ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtajj_maxdR (" << m_outData -> o_dEtajj_maxdR << ")" << std::endl;
  m_outData -> o_pseudo_mass = m_varComputer -> GetPseudoHplusMass( m_ntupData -> d_mcChannelNumber ) ;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_pseudo_mass (" << m_outData -> o_pseudo_mass << ")" << std::endl;
  m_outData -> o_btagw_discrete_bord = m_varComputer -> GetBtagwDiscrete( *(m_outData -> o_jets_btagworder_4j), m_opt->BtagAlg() );
  m_outData -> o_btagw_discrete = m_varComputer -> GetBtagwDiscrete( *(m_outData -> o_jets), m_opt->BtagAlg() );



  if(m_outData -> o_is_ttbar && m_opt->InputTree()=="nominal_Loose")
    {
        m_outData -> o_nom_weight_ISR_UP = m_varComputer -> GetISRSystUp( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights));
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_nom_weight_ISR_UP (" << m_outData -> o_nom_weight_ISR_UP << ")" << std::endl;
        m_outData -> o_nom_weight_ISR_DOWN = m_varComputer -> GetISRSystDown( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights));
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_nom_weight_ISR_DOWN (" << m_outData -> o_nom_weight_ISR_DOWN << ")" << std::endl;
        m_outData -> o_nom_weight_FSR_UP = m_varComputer -> GetFSRSystUp( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights));
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_nom_weight_FSR_UP (" << m_outData -> o_nom_weight_FSR_UP << ")" << std::endl;
        m_outData -> o_nom_weight_FSR_DOWN = m_varComputer -> GetFSRSystDown( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights));
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_nom_weight_FSR_DOWN (" << m_outData -> o_nom_weight_FSR_DOWN << ")" << std::endl;

        m_outData -> o_nom_weight_index_1   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 1  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_1  ("<<m_outData -> o_nom_weight_index_1  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_2   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 2  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_2  ("<<m_outData -> o_nom_weight_index_2  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_3   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 3  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_3  ("<<m_outData -> o_nom_weight_index_3  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_4   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 4  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_4  ("<<m_outData -> o_nom_weight_index_4  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_5   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 5  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_5  ("<<m_outData -> o_nom_weight_index_5  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_6   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 6  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_6  ("<<m_outData -> o_nom_weight_index_6  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_7   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 7  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_7  ("<<m_outData -> o_nom_weight_index_7  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_8   = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 8  );
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_8  ("<<m_outData -> o_nom_weight_index_8  <<")"<< std::endl;
        m_outData -> o_nom_weight_index_193 = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 193);
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_193("<<m_outData -> o_nom_weight_index_193<<")"<< std::endl;
        m_outData -> o_nom_weight_index_194 = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 194);
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_194("<<m_outData -> o_nom_weight_index_194<<")"<< std::endl;
        m_outData -> o_nom_weight_index_198 = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 198);
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_198("<<m_outData -> o_nom_weight_index_198<<")"<< std::endl;
        m_outData -> o_nom_weight_index_199 = m_varComputer -> GetIndividualWeight( m_ntupData -> d_mcChannelNumber, *(m_ntupData->d_mc_generator_weights), 199);
        if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "->After m_outData -> o_nom_weight_index_199("<<m_outData -> o_nom_weight_index_199<<")"<< std::endl;
    }

  this -> ComputeBTagVariables();

  this -> ConvertToGeVAll();

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Leaving FCNC_AnalysisTools::ComputeAllVariables()" << std::endl;

  return true;
}

//____________________________________________________________________________________
//
bool FCNC_AnalysisTools::ComputeBTagVariables() {

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Entering in FCNC_AnalysisTools::ComputeBTagVariables()" << std::endl;

  //
  // Filling only the b-tag sensitive kinematic variables
  //
  m_outData -> o_mbb_mindR        = m_varComputer -> GetMbb( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mbb_mindR (" << m_outData -> o_mbb_mindR << ")"  << std::endl;
  m_outData -> o_bjets_n          = m_outData     -> o_bjets_n;
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_bjets_n (" << m_outData -> o_bjets_n << ")"  << std::endl;
  m_outData -> o_dRmin_mubjets    = m_varComputer -> GetMindR( *(m_outData->o_bjets), *(m_outData->o_mu) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_mubjets (" << m_outData -> o_dRmin_mubjets << ")"  << std::endl;
  m_outData -> o_dRmin_ebjets     = m_varComputer -> GetMindR( *(m_outData->o_bjets), *(m_outData->o_el) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_ebjets (" << m_outData -> o_dRmin_ebjets << ")"  << std::endl;
  m_outData -> o_dRmin_bjetbjet   = m_varComputer -> GetMindR( *(m_outData->o_bjets), *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_bjetbjet (" << m_outData -> o_dRmin_bjetbjet << ")"  << std::endl;
  m_outData -> o_dPhi_lepbjet     = m_varComputer -> GetMindPhi( m_outData->o_selLep, *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dPhi_lepbjet (" << m_outData -> o_dPhi_lepbjet << ")"  << std::endl;
  m_outData -> o_mTbmin           = m_varComputer -> GetMTbmin( *(m_outData->o_bjets), m_outData -> o_AO_met );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mTbmin (" << m_outData -> o_mTbmin << ")"  << std::endl;
  m_outData -> o_mbb_leading_bjets= m_varComputer -> GetMbbLeadingBjets( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mbb_leading_bjets (" << m_outData -> o_mbb_leading_bjets << ")"  << std::endl;
  m_outData -> o_mbb_softest_bjets= m_varComputer -> GetMbbSoftestBjets( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mbb_softest_bjets (" << m_outData -> o_mbb_softest_bjets << ")"  << std::endl;
  m_outData ->  o_dRaverage_bjetbjet = m_varComputer -> GetAveragedR( *(m_outData->o_bjets), *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_dRaverage_bjetbjet  (" << m_outData ->  o_dRaverage_bjetbjet << ")"  << std::endl;
  m_outData -> o_mbb_maxdR        =m_varComputer -> GetMbbMaxDr( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_mbb_maxdR (" << m_outData -> o_mbb_maxdR << ")" << std::endl;
  m_outData -> o_mjb_mindR        =m_varComputer -> GetMjbMinDr( *(m_outData->o_jets_btagworder_4j));
  if (m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_mjb_mindR (" << m_outData -> o_mjb_mindR << ")" << std::endl;
  m_outData -> o_mjb_maxdR        =m_varComputer -> GetMjbMaxDr( *(m_outData->o_jets_btagworder_4j));
  if (m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_mjb_maxdR (" << m_outData -> o_mjb_maxdR << ")" << std::endl;
  m_outData -> o_mjb_leading_bjet =m_varComputer -> GetMjbLeadingBjet( *(m_outData->o_jets_btagworder_4j));
  if (m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData ->  o_mjb_leading_bjet (" << m_outData -> o_mjb_leading_bjet << ")" << std::endl;
  m_outData -> o_dPhibb_leading_bjets = m_varComputer -> GetDphibbLeadingBjets( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhibb_leading_bjets (" << m_outData -> o_dPhibb_leading_bjets << ")" << std::endl;
  m_outData -> o_dPhibb_mindR = m_varComputer -> GetDphibbMinDr( *(m_outData -> o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhibb_mindR (" << m_outData -> o_dPhibb_mindR << ")" << std::endl;
  m_outData -> o_dPhibb_maxdR = m_varComputer -> GetDphibbMaxDr( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dPhibb_maxdR (" << m_outData ->o_dPhibb_maxdR << ")" << std::endl;
  m_outData -> o_dEtabb_leading_bjets = m_varComputer -> GetDetabbLeadingBjets( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtabb_leading_bjets (" << m_outData -> o_dEtabb_leading_bjets << ")" << std::endl;
  m_outData -> o_dEtabb_mindR = m_varComputer -> GetDetabbMinDr( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtabb_mindR (" << m_outData -> o_dEtabb_mindR << ")" << std::endl;
  m_outData -> o_dEtabb_maxdR = m_varComputer -> GetDetabbMaxDr( *(m_outData->o_bjets) );
  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    ->After m_outData -> o_dEtabb_maxdR (" << m_outData ->o_dEtabb_maxdR << ")" << std::endl;


  if(m_opt->DoLowBRegions()){
    //Fill fake low-b variables with the fake low-b vectors
    m_outData -> o_dRmin_bjetbjet_lowb_3b   = m_varComputer -> GetMindR( *(m_outData->o_bjets_lowb_3b), *(m_outData->o_bjets_lowb_3b) );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_bjetbjet_lowb_3b ("
    << m_outData -> o_dRmin_bjetbjet_lowb_3b << ")"  << std::endl;
    m_outData -> o_dRmin_bjetbjet_lowb_4b   = m_varComputer -> GetMindR( *(m_outData->o_bjets_lowb_4b), *(m_outData->o_bjets_lowb_4b) );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_dRmin_bjetbjet_lowb_4b ("
    << m_outData -> o_dRmin_bjetbjet_lowb_4b << ")"  << std::endl;

    m_outData -> o_mTbmin_lowb_3b           = m_varComputer -> GetMTbmin( *(m_outData->o_bjets_lowb_3b), m_outData -> o_AO_met );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mTbmin_lowb_3b ("
    << m_outData -> o_mTbmin_lowb_3b << ")"  << std::endl;
    m_outData -> o_mTbmin_lowb_4b           = m_varComputer -> GetMTbmin( *(m_outData->o_bjets_lowb_4b), m_outData -> o_AO_met );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mTbmin_lowb_4b ("
    << m_outData -> o_mTbmin_lowb_4b << ")"  << std::endl;

    m_outData -> o_mbb_mindR_lowb_3b        = m_varComputer -> GetMbb( *(m_outData->o_bjets_lowb_3b) );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mbb_mindR_lowb_3b ("
    << m_outData -> o_mbb_mindR_lowb_3b << ")"  << std::endl;
    m_outData -> o_mbb_mindR_lowb_4b        = m_varComputer -> GetMbb( *(m_outData->o_bjets_lowb_4b) );
    if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "    -> After m_outData -> o_mbb_mindR_lowb_4b ("
    << m_outData -> o_mbb_mindR_lowb_4b << ")"  << std::endl;
  }//DoLowBRegions

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Exiting in FCNC_AnalysisTools::ComputeBTagVariables()" << std::endl;



  return true;
}



void FCNC_AnalysisTools::ConvertToGeVAll() {

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Entering in FCNC_AnalysisTools::ConvertToGeVAll()" << std::endl;

  //Per event variables
  m_outData -> o_meff   = m_outData -> o_meff * mev_to_gev;
  m_outData -> o_met    = m_outData -> o_met * mev_to_gev;
  m_outData -> o_mtwl   = m_outData -> o_mtwl * mev_to_gev;
  m_outData -> o_hthad  = m_outData -> o_hthad * mev_to_gev;

  //Invariant massed
  m_outData -> o_mbb_mindR                 =   m_outData -> o_mbb_mindR * mev_to_gev;
  m_outData -> o_mbb_mindR_lowb_3b         =   m_outData -> o_mbb_mindR_lowb_3b * mev_to_gev;
  m_outData -> o_mbb_mindR_lowb_4b         =   m_outData -> o_mbb_mindR_lowb_4b * mev_to_gev;
  m_outData -> o_mbb_leading_bjets         =   m_outData -> o_mbb_leading_bjets * mev_to_gev;
  m_outData -> o_mbb_softest_bjets         =   m_outData -> o_mbb_softest_bjets * mev_to_gev;
  m_outData -> o_J_lepton_invariant_mass   =   m_outData -> o_J_lepton_invariant_mass * mev_to_gev;
  m_outData -> o_J_leadingb_invariant_mass =   m_outData -> o_J_leadingb_invariant_mass * mev_to_gev;
  m_outData -> o_J_J_invariant_mass        =   m_outData -> o_J_J_invariant_mass * mev_to_gev;
  m_outData -> o_mbb_maxdR                 =   m_outData -> o_mbb_maxdR * mev_to_gev;
  m_outData -> o_mjj_leading_jets          =   m_outData -> o_mjj_leading_jets * mev_to_gev;
  m_outData -> o_mjj_mindR                 =   m_outData -> o_mjj_mindR * mev_to_gev;
  m_outData -> o_mjj_maxdR                 =   m_outData -> o_mjj_maxdR * mev_to_gev;
  m_outData -> o_mjb_mindR                 =   m_outData -> o_mjb_mindR * mev_to_gev;
  m_outData -> o_mjb_maxdR                 =   m_outData -> o_mjb_maxdR * mev_to_gev;
  m_outData -> o_mjb_leading_bjet          =   m_outData -> o_mjb_leading_bjet * mev_to_gev;


  m_outData -> o_AO_met -> SetPtEtaPhiM( m_outData->o_AO_met->Pt()*mev_to_gev ,
					 m_outData->o_AO_met->Eta(),
					 m_outData->o_AO_met->Phi(),
					 m_outData->o_AO_met->M()*mev_to_gev
					 );

  for(unsigned int index=0; index<m_outData->o_jets->size(); index++)
    {
      m_outData->o_jets->at(index)->SetPtEtaPhiE(m_outData->o_jets->at(index)->Pt()*mev_to_gev,
						 m_outData->o_jets->at(index)->Eta(),
						 m_outData->o_jets->at(index)->Phi(),
						 m_outData->o_jets->at(index)->E()*mev_to_gev
						 );
    }

  for(unsigned int index=0; index<m_outData->o_el->size(); index++)
    {
      m_outData->o_el->at(index)->SetPtEtaPhiE(m_outData->o_el->at(index)->Pt()*mev_to_gev,
					       m_outData->o_el->at(index)->Eta(),
					       m_outData->o_el->at(index)->Phi(),
					       m_outData->o_el->at(index)->E()*mev_to_gev
					       );
    }

  for(unsigned int index=0; index<m_outData->o_mu->size(); index++)
    {
      m_outData->o_mu->at(index)->SetPtEtaPhiE(m_outData->o_mu->at(index)->Pt()*mev_to_gev,
					       m_outData->o_mu->at(index)->Eta(),
					       m_outData->o_mu->at(index)->Phi(),
					       m_outData->o_mu->at(index)->E()*mev_to_gev
					       );
    }

/*
to be implemented
//Jet variables
o_ljets(0),
o_bjets_lowb_3b(0),o_bjets_lowb_4b(0),
//Lepton variables
o_selLep(0),
//Truth variables
o_truth_top_pt(0),
o_AO_met
*/

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Exiting in FCNC_AnalysisTools::ConvertToGeVAll()" << std::endl;
  return;
}



/*
bool FCNC_AnalysisTools::ConvertToGeV() {

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Entering in FCNC_AnalysisTools::ConvertToGeV()" << std::endl;


//Event variables
o_meff(0), o_met(0), o_mtwl(0), o_ptwl(0), o_hthad(0),
//Jet variables
o_jets(0),o_bjets(0),o_ljets(0),
o_bjets_lowb_3b(0),o_bjets_lowb_4b(0),
//Lepton variables
o_el(0),o_mu(0),o_lep(0),o_selLep(0),
//MET
o_AO_met(0),

o_mbb_mindR(0), o_mbb_mindR_lowb_3b(0), o_mbb_mindR_lowb_4b(0),
o_mbb_leading_bjets(0), o_mbb_softest_bjets(0), o_J_lepton_invariant_mass(0), o_J_leadingb_invariant_mass(0), o_J_J_invariant_mass(0),

o_mbb_maxdR(0),
o_mjj_leading_jets(0),
o_mjj_mindR(0), o_mjj_maxdR(0) , o_dEtajj_mindR(0), o_dEtajj_maxdR(0),


//Truth variables
o_truth_top_pt(0),

  if(m_opt -> MsgLevel() == Debug::DEBUG) std::cout << "Exiting in FCNC_AnalysisTools::ConvertToGeV()" << std::endl;
}

*/
