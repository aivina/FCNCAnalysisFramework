### Test files needed to run offline productions 

The weights 

    list_weights- 

are outdated, kept for future checks, not currently maintained. 

The list of samples- are mainly from old iterations of ntuple production (main productions). The following are available

* samples_HtX4TopsNtuple: for old ntuples based on AnalysisTop, not used anymore

* samples_info_MBJ: for old ntuples based on SUSY tools, recent productions are still available on at3.  

Other outdated infomation 

* setup_commands.txt: old info on how to set things up

* ttbarSystematics.root: root file with parametrised ttbar uncertainties 

