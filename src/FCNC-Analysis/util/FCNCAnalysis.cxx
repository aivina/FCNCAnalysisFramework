/**
   Example usage:
   FCNCAnalysis --inputfile /var/tmp/gerbaudo/ntuples/user.lvalery.410000MC15b_7349564._000001.output_tree.root
 */
#include <iostream>
#include "TStopwatch.h"

#include "FCNC-Analysis/FCNC_Analysis.h"
#include "FCNC-Analysis/FCNC_Options.h"

int main(int argc, char** argv){
    
    TStopwatch *timer = new TStopwatch();
    timer -> Start();
    
    FCNC_Options* opt = new FCNC_Options();
    opt->ParseUserOpts(argc, argv);
    
    FCNC_Analysis ana(opt);
    ana.Begin();
    ana.Loop();
    ana.Terminate();
    
    timer -> Stop();
    
    std::cout << "Processing time: " << timer -> RealTime() << " s" << std::endl;
    
    return 0;
}
