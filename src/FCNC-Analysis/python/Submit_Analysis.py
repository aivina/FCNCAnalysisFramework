import os
import string
import time, getpass
import socket
import sys
import datetime
from Analysis_Samples import *

sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
from BatchTools import *
from Job import *
from Samples import *

##______________________________________________________________________________
## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()
##..............................................................................

##______________________________________________________________________________
## OPTIONS
nFilesSplit = 2 #number of files to merge in a single run of the code (chaining them)
nMerge = 2 #number of merged running of the code in a single job
channels = ["./.",]
sleep = 1
param_runQCD = False
param_runData = True
param_runTOPQ1Data = True
param_runSignal = True
param_runFCNC = False
param_runHpluslm = True
param_useSlices = False
param_useBFilter= False
param_useObjectSyst = True
param_useWeightSyst = True
param_onlyDumpSystHistos = False
param_produceTarBall = True
param_dryRun = False
param_batch = "condor"
param_queue = "at3"
param_inputDir="/nfs/at3/scratch2/orlando/FCNC-Full-Run2/prod_13-Jan-2018/" #place where to find the input files
#need to change this
param_outputDir = "/nfs/at3/scratch2/"+os.getenv("USER")+"/FCNC-offline/FCNC-Analysis_output_" #output repository
param_outputDirSuffix = "NOW"
userParams = []
if(len(sys.argv))>1:
    for x in sys.argv[1:]:
        splitted=x.split("=")
        if(len(splitted)!=2):
            printError("<!> The argument \"" + x + "\" has no equal signs ... Please check")
            sys.exit(-1)
        argument = splitted[0].upper().replace("--","")
        value = splitted[1]
        if(argument=="NFILESPLIT"):
            nFilesSplit = int(value)
        elif(argument=="NMERGE"):
            nMerge = int(value)
        elif(argument=="SLEEP"):
            sleep = int(value)
        elif(argument=="RUNDATA"):
            param_runData = (value.upper()=="TRUE")
        elif(argument=="RUNQCD"):
            param_runQCD = (value.upper()=="TRUE")
        elif(argument=="RUNTOPQ1DATA"):
            param_runTOPQ1Data = (value.upper()=="TRUE")
        elif(argument=="RUNSIGNALS"):
            param_runSignal = (value.upper()=="TRUE")
        elif(argument=="RUNFCNC"):
            param_runFCNC = (value.upper()=="TRUE")
        elif(argument=="RUNHPLUSLM"):
            param_runHpluslm = (value.upper()=="TRUE")
        elif(argument=="USESLICES"):
            param_useSlices = (value.upper()=="TRUE")
        elif(argument=="USEOBJECTSYST"):
            param_useObjectSyst = (value.upper()=="TRUE")
        elif(argument=="USEWEIGHTSYST"):
            param_useWeightSyst = (value.upper()=="TRUE")
        elif(argument=="ONLYDUMPSYSTHISTOS"):
            param_onlyDumpSystHistos = (value.upper()=="TRUE")
        elif(argument=="PRODUCETARBALL"):
            param_produceTarBall = (value.upper()=="TRUE")
        elif(argument=="DRYRUN"):
            param_dryRun = (value.upper()=="TRUE")
        elif(argument=="BATCH"):
            param_batch = value
        elif(argument=="QUEUE"):
            param_queue = value
        elif(argument=="INPUTDIR"):
            param_inputDir = value
        elif(argument=="OUTPUTDIR"):
            param_outputDir = value
        elif(argument=="OUTPUTDIRSUFFIX"):
            param_outputDirSuffix = value
        else:
            userParams += [{'arg':argument,'value':value}]
else:
    printError("<!> No arguments seen ... Aborting !")
    sys.exit(-1)

param_outputDirSuffix = param_outputDirSuffix.replace("NOW",now)
param_outputDir += param_outputDirSuffix
##..............................................................................


##______________________________________________________________________________
## Printing the options
print "nFilesSplit = ", nFilesSplit
print "nMerge = ", nMerge
print "param_runData = ", param_runData
print "param_runTOPQ1Data = ", param_runTOPQ1Data
print "param_runSignal = ", param_runSignal
print "param_runFCNC = ", param_runFCNC
print "param_runHpluslm = ", param_runHpluslm
print "param_useSlices = ", param_useSlices
print "param_useObjectSyst = ", param_useObjectSyst
print "param_useWeightSyst = ", param_useWeightSyst
print "param_onlyDumpSystHistos = ", param_onlyDumpSystHistos
print "param_produceTarBall = ", param_produceTarBall
print "param_dryRun = ", param_dryRun
print "param_batch = ",param_batch
print "param_inputDir = ", param_inputDir
print "param_outputDir = ", param_outputDir
for param in userParams:
    print param['arg'] + " = " + param['value']
##..............................................................................

##______________________________________________________________________________
## Defining the paths and the tarball
listFolder = param_outputDir + "/Lists_Analysis_" + param_outputDir.split("/")[len(param_outputDir.split("/"))-1] #name of the folder containing the file lists
scriptFolder = param_outputDir + "/Scripts_Analysis_" + param_outputDir.split("/")[len(param_outputDir.split("/"))-1] #name of the folder containing the scripts
##..............................................................................

##______________________________________________________________________________
## Creating usefull repositories
os.system("mkdir -p " + param_outputDir) #output files folder
os.system("mkdir -p " + listFolder) #list files folder
os.system("mkdir -p " + scriptFolder) #script files folder
##..............................................................................

##______________________________________________________________________________
## Creating tarball
tarballPath=""
if param_produceTarBall:
    tarballPath = param_outputDir + "/AnaCode_forBatch.tgz"
    if not os.path.exists(tarballPath):
        prepareTarBall(here+"/../../",tarballPath)
##..............................................................................

##______________________________________________________________________________
## Getting all samples and their associated weight/object systematics
Samples = []
if param_runQCD :
    Samples += GetQCDSamples(  )
if param_runData :
    if param_runTOPQ1Data:
        Samples += GetDataSamples( data_type = "TOPQ1" )

Samples += GetTtbarSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst, ttbarSystSamples = False, hfSplitted = True, useHTSlices = param_useSlices, useBFilter = param_useBFilter)
Samples += GetOtherSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst,
                             includeSignals = param_runSignal, 
                             includeFCNC = param_runFCNC, includeHpluslm = param_runHpluslm)
printGoodNews("--> All samples recovered")
##..............................................................................

##______________________________________________________________________________
## Loop over channels
printGoodNews("--> Performing the loop over samples and jobs")
nJobs = 0
for channel in channels:

    ##__________________________________________________________________________
    ## Loop over samples
    for sample in Samples:

        SName = sample['name'] # sample name
        SType = sample['sampleType'] # sample type (first argument of the getSamplesUncertainties())

        print("\n Printing sample name")
        print(SName)
        print("\n Printing sample type")
        print(SType)

        #Redifining the number of files for job 
        if( SType=="ttbarlight" or SType=="ttbarbb" or SType=="ttbarcc" or SType=="ttH" ) :
            nFilesSplit=6
        elif( SType=="W+jets22light" or SType=="W+jets22charm" or SType=="W+jets22beauty" or 
              SType=="Z+jets22light" or SType=="Z+jets22charm" or SType=="Z+jets22beauty" or 
              SType=="Dibosons") :
            nFilesSplit=50
        elif( SType=="Singletop" ) :
            nFilesSplit=20
        elif( SType=="topEW" or SType=="SM4tops" ) :
            nFilesSplit=8
        elif( SType=="Data" ) :
            nFilesSplit=10
        else : 
            nFilesSplit=2

        print("Number of splitted jobs %i",nFilesSplit)
        excluded = []
        joblist = getSampleJobs(sample,InputDir=param_inputDir+"/"+channel+"/",NFiles=nFilesSplit,
                                UseList=False,ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=False)
        if(not joblist):
            continue

        #Setting caracteristics for the JobSet object
        JOSet = JobSet(platform)
        JOSet.setBatch(param_batch)
        JOSet.setScriptDir(scriptFolder)
        JOSet.setLogDir(param_outputDir)
        JOSet.setTarBall(tarballPath)#tarball sent to batch (contains all executables)
        JOSet.setJobRecoveryFile(scriptFolder+"/JobCheck.chk")
        JOSet.setQueue(param_queue)
        JOSet.setSetUpCommand("rcSetup Base,2.4.43")

        ##______________________________________________________________________
        ## Loop over jobs for this sample (multiple files or systematics)
        for iJob in range(len(joblist)):

            ## Declare the Job object (one job = one code running once)
            jO = Job(platform)

            ## Name of the executable you want to run
            jO.setExecutable("FCNCAnalysis")
            jO.setDebug(False)

            name  = SType
            name += "_" + SName
            name += "_"+joblist[iJob]['objSyst']+"_"+`iJob` #name of the job

            jO.setName(name)

            # Settings of the jobs (inputs, outputs, ...)
            OFileName = "out"+jO.execName+"_"+name+".root"
            jO.addOption("outputFile",OFileName) #name of the output file
            jO.addOption("inputFile",joblist[iJob]['filelist']) #name of the input file (already got from ls)
            jO.addOption("textFileList","false")
            jO.addOption("sampleName",sample['sampleType'])

            # Weight systematics
            if param_useWeightSyst and joblist[iJob]['objSyst'].upper()=="NOMINAL_LOOSE" and SType.upper().find("DATA")==-1:
                jO.addOption("computeWeightSys","true")
            else:
                jO.addOption("computeWeightSys","false")
                
            jO.setOutDir(param_outputDir)
            jO.addOption("sampleID",SName)

            # Input tree name (might change for systematics)
            jO.addOption("inputTree",joblist[iJob]['objectTree'])

            # isData flag
            if(sample['sampleType'].upper().find("DATA")>-1):
                jO.addOption("isData","true")
            else:
                jO.addOption("isData","false")

            #Adding further options set by the user
            for param in userParams:
                jO.addOption(param['arg'],param['value'])

            ## SKIPPING ALREADY PROCESSED FILES
            if(os.path.exists(param_outputDir+"/"+OFileName)):
                printWarning("=> Already processed: skipping")
                continue

            JOSet.addJob(jO)
            if ( JOSet.size()==nMerge or joblist[iJob]['objSyst'].upper()=="NOMINAL_LOOSE" ):
                JOSet.writeScript()
                if not param_dryRun:
                    JOSet.submitSet()
                JOSet.clear()

        if(JOSet.size()>0):
            JOSet.writeScript()
            if not param_dryRun:
                JOSet.submitSet()
            JOSet.clear()
        os.system("sleep "+`sleep`)
##..............................................................................
