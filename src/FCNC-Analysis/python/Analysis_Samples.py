#!/bin/python
import os
import sys

sys.path.append( os.getenv("ROOTCOREBIN") + "/python/IFAETopFramework/" )
from BatchTools import *
from Samples import *

##______________________________________________________________________
##
## Object systematics
##
##Nominal
CommonObjectSystematics =  []
CommonObjectSystematics += [getSystematics(name="nominal_Loose",nameUp="nominal_Loose",oneSided=True)] # the nominal is considered as a systematic variation

##Electron
CommonObjectSystematics += [getSystematics(name="EG_RESOLUTION_ALL",nameUp="EG_RESOLUTION_ALL__1up_Loose",nameDown="EG_RESOLUTION_ALL__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="EG_SCALE_ALL",nameUp="EG_SCALE_ALL__1up_Loose",nameDown="EG_SCALE_ALL__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="EG_SCALE_AF2",nameUp="EG_SCALE_AF2__1up_Loose",nameDown="EG_SCALE_AF2__1down_Loose",oneSided=False)]

##Jet energy scale 
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_BJES_Response",nameUp="CategoryReduction_JET_BJES_Response__1up_Loose",
                                           nameDown="CategoryReduction_JET_BJES_Response__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Detector1",nameUp="CategoryReduction_JET_EffectiveNP_Detector1__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Detector1__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Detector2",nameUp="CategoryReduction_JET_EffectiveNP_Detector2__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Detector2__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Mixed1",nameUp="CategoryReduction_JET_EffectiveNP_Mixed1__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Mixed1__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Mixed2",nameUp="CategoryReduction_JET_EffectiveNP_Mixed2__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Mixed2__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Mixed3",nameUp="CategoryReduction_JET_EffectiveNP_Mixed3__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Mixed3__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Modelling1",nameUp="CategoryReduction_JET_EffectiveNP_Modelling1__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Modelling1__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Modelling2",nameUp="CategoryReduction_JET_EffectiveNP_Modelling2__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Modelling2__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Modelling3",nameUp="CategoryReduction_JET_EffectiveNP_Modelling3__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Modelling3__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Modelling4",nameUp="CategoryReduction_JET_EffectiveNP_Modelling4__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Modelling4__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical1",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical1__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical1__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical2",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical2__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical2__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical3",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical3__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical3__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical4",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical4__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical4__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical5",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical5__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical5__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EffectiveNP_Statistical6",
                                           nameUp="CategoryReduction_JET_EffectiveNP_Statistical6__1up_Loose",
                                           nameDown="CategoryReduction_JET_EffectiveNP_Statistical6__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EtaIntercalibration_Modelling",
                                           nameUp="CategoryReduction_JET_EtaIntercalibration_Modelling__1up_Loose",
                                           nameDown="CategoryReduction_JET_EtaIntercalibration_Modelling__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EtaIntercalibration_NonClosure_highE",
                                           nameUp="CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up_Loose",
                                           nameDown="CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta",
                                           nameUp="CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up_Loose",
                                           nameDown="CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta",
                                           nameUp="CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up_Loose",
                                           nameDown="CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_EtaIntercalibration_TotalStat",
                                           nameUp="CategoryReduction_JET_EtaIntercalibration_TotalStat__1up_Loose",
                                           nameDown="CategoryReduction_JET_EtaIntercalibration_TotalStat__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Flavor_Composition",
                                           nameUp="CategoryReduction_JET_Flavor_Composition__1up_Loose",
                                           nameDown="CategoryReduction_JET_Flavor_Composition__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Flavor_Response",
                                           nameUp="CategoryReduction_JET_Flavor_Response__1up_Loose",
                                           nameDown="CategoryReduction_JET_Flavor_Response__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Pileup_OffsetMu",
                                           nameUp="CategoryReduction_JET_Pileup_OffsetMu__1up_Loose",
                                           nameDown="CategoryReduction_JET_Pileup_OffsetMu__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Pileup_OffsetNPV",
                                           nameUp="CategoryReduction_JET_Pileup_OffsetNPV__1up_Loose",
                                           nameDown="CategoryReduction_JET_Pileup_OffsetNPV__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Pileup_PtTerm",
                                           nameUp="CategoryReduction_JET_Pileup_PtTerm__1up_Loose",
                                           nameDown="CategoryReduction_JET_Pileup_PtTerm__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_Pileup_RhoTopology",
                                           nameUp="CategoryReduction_JET_Pileup_RhoTopology__1up_Loose",
                                           nameDown="CategoryReduction_JET_Pileup_RhoTopology__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_PunchThrough_AFII",
                                           nameUp="CategoryReduction_JET_PunchThrough_AFII__1up_Loose",
                                           nameDown="CategoryReduction_JET_PunchThrough_AFII__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_RelativeNonClosure_AFII",
                                           nameUp="CategoryReduction_JET_RelativeNonClosure_AFII__1up_Loose",
                                           nameDown="CategoryReduction_JET_RelativeNonClosure_AFII__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_SingleParticle_HighPt",
                                           nameUp="CategoryReduction_JET_SingleParticle_HighPt__1up_Loose",
                                           nameDown="CategoryReduction_JET_SingleParticle_HighPt__1down_Loose",oneSided=False)]

#JER 
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_DataVsMC_AFII__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_DataVsMC_AFII__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_1__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_1__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_2__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_2__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_3__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_3__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_4__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_4__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_5__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_5__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_6__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_6__1up_Loose",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up_Loose",
                                           nameUp="CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up_Loose",oneSided=True)]
#MET
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_ResoPara",nameUp="MET_SoftTrk_ResoPara",nameDown="",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_ResoPerp",nameUp="MET_SoftTrk_ResoPerp",nameDown="",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_Scale",nameUp="MET_SoftTrk_ScaleUp",nameDown="MET_SoftTrk_ScaleDown",oneSided=False)]

#Muon
CommonObjectSystematics += [getSystematics(name="MUON_ID",nameUp="MUON_ID__1up_Loose",nameDown="MUON_ID__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_MS",nameUp="MUON_MS__1up_Loose",nameDown="MUON_MS__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SCALE",nameUp="MUON_SCALE__1up_Loose",nameDown="MUON_SCALE__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SAGITTA_RESBIAS",nameUp="MUON_SAGITTA_RESBIAS__1up_Loose",nameDown="MUON_SAGITTA_RESBIAS__1down_Loose",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SAGITTA_RHO",nameUp="MUON_SAGITTA_RHO__1up_Loose",nameDown="MUON_SAGITTA_RHO__1down_Loose",oneSided=False)]

##______________________________________________________________________
##
def GetTtbarSamples( useWeightSyst=False, useObjectSyst=False, hfSplitted=True, ttbarSystSamples=True, useHTSlices = False, useBFilter = True):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples = []
    ttbarTypes = ["ttbarlight","ttbarbb","ttbarcc"]
    if not hfSplitted:
        ttbarTypes = ["ttbar_lep"]
    for ttbarType in ttbarTypes:
        Samples     += [getSampleUncertainties(ttbarType,    "410470.", ObjectSystematics, WeightSystematics)]
        #the following ttbar samples are all outdated
        if useHTSlices:
            Samples     += [getSampleUncertainties(ttbarType,    "407009.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "407010.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "407011.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "407012.", ObjectSystematics, WeightSystematics)]
        if useBFilter:
            Samples     += [getSampleUncertainties(ttbarType,    "411073.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "411074.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "411075.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "411076.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "411077.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "411078.", ObjectSystematics, WeightSystematics)]
        if ttbarSystSamples:
            # Commenting this out
            #Samples     += [getSampleUncertainties(ttbarType+"Sherpa",    "410250.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            #Samples     += [getSampleUncertainties(ttbarType+"Sherpa",    "410251.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            #Samples     += [getSampleUncertainties(ttbarType+"Sherpa",    "410252.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            #Samples     += [getSampleUncertainties(ttbarType+"HdumpVar",  "410480.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            #Samples     += [getSampleUncertainties(ttbarType+"HdumpVar",  "410482.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "410557.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "410558.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8","410464.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8","410465.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowP8_ttbb","411178.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowP8_ttbb","411179.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowP8_ttbb","411180.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            if useHTSlices:
                Samples     += [getSampleUncertainties(ttbarType+"radHi",   "407029.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",   "407030.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",   "407031.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",   "407032.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",  "407033.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",  "407034.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",  "407035.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",  "407036.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",  "407037.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",  "407038.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",  "407039.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",  "407040.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
            if useBFilter:
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411082.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411083.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411084.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411085.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411086.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowH7",     "411087.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412066.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412067.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412068.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412069.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412070.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"noShWMCPy8",   "412071.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    return Samples

    ##______________________________________________________________________
##
def GetOtherSamples ( useWeightSyst=False, useObjectSyst=False,
                      includeFCNC=True, includeHpluslm=True, includeSignals=True, includeSingletopSystSamples=True, includeWtocbSystSamples=True):
    Samples =  []
    Samples += GetWSamplesSherpa22( useWeightSyst, useObjectSyst )
    Samples += GetZSamplesSherpa22( useWeightSyst, useObjectSyst )
    Samples += GetSingleTopSamples( useWeightSyst, useObjectSyst, SingletopSystSamples=includeSingletopSystSamples )
    Samples += GetTopEWSamples( useWeightSyst, useObjectSyst )
    Samples += GetTtHSamples( useWeightSyst, useObjectSyst )
    Samples += GetDibosonSamples( useWeightSyst, useObjectSyst )
    # Not in use anymore 
    #Samples += Get4TopsSamples( useWeightSyst, useObjectSyst )
    Samples += GetWcbSamples( useWeightSyst, useObjectSyst, WtocbSystSamples=includeWtocbSystSamples)
    if includeSignals:
        if includeFCNC:
            Samples += GetFCNCSamples( useWeightSyst, useObjectSyst )
        if includeHpluslm:
            Samples += GetHpluslmSamples( useWeightSyst, useObjectSyst)
    return Samples

##_____________________________________________________________________
##
def GetWcbSamples (useWeightSyst=False, useObjectSyst=False, WtocbSystSamples=False, name = "Wtocb"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]

    Samples    =  []
    Samples    += [getSampleUncertainties(name, "600642.", ObjectSystematics, WeightSystematics)]
    Samples    += [getSampleUncertainties(name, "600643.", ObjectSystematics, WeightSystematics)]
    if WtocbSystSamples:
        Samples    += [getSampleUncertainties(name+"PowH7", "600644", ObjectSystematics, WeightSystematics)]
        Samples    += [getSampleUncertainties(name+"PowH7", "600645", ObjectSystematics, WeightSystematics)]
        Samples    += [getSampleUncertainties(name+"noShWMCPy8", "500565", ObjectSystematics, WeightSystematics)]
        Samples    += [getSampleUncertainties(name+"noShWMCPy8", "500566", ObjectSystematics, WeightSystematics)]
    
    return Samples
##---------------------------------------------------------------------
##
def GetWSamplesSherpa22( useWeightSyst=False, useObjectSyst=False, name = "W+jets22"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]

    Samples     =  []

    Samples     += [getSampleUncertainties(name+"light" ,"364156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364158.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364159.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364160.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364161.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364162.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364163.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364164.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364165.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364166.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364167.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364168.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364169.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364170.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364171.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364172.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364173.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364174.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364175.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364176.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364178.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364179.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364180.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364181.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364182.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364183.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364184.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364185.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364186.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364187.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364188.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364189.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364190.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364191.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364192.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364193.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364194.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364195.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364196.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light" ,"364197.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties(name+"charm" ,"364156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364158.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364159.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364160.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364161.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364162.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364163.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364164.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364165.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364166.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364167.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364168.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364169.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364170.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364171.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364172.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364173.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364174.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364175.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364176.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364178.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364179.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364180.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364181.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364182.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364183.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364184.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364185.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364186.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364187.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364188.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364189.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364190.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364191.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364192.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364193.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364194.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364195.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364196.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm" ,"364197.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties(name+"beauty" ,"364156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364158.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364159.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364160.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364161.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364162.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364163.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364164.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364165.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364166.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364167.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364168.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364169.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364170.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364171.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364172.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364173.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364174.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364175.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364176.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364178.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364179.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364180.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364181.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364182.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364183.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364184.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364185.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364186.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364187.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364188.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364189.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364190.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364191.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364192.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364193.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364194.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364195.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364196.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty" ,"364197.", ObjectSystematics , WeightSystematics)]

    return Samples

##______________________________________________________________________
##
def GetZSamplesSherpa22( useWeightSyst=False, useObjectSyst=False, name = "Z+jets22"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []

    Samples     += [getSampleUncertainties(name+"light","364100.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364101.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364102.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364103.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364104.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364105.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364106.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364107.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364108.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364109.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364110.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364111.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364112.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364113.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364114.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364115.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364117.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364118.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364119.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364120.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364121.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364122.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364123.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364124.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364125.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364126.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364127.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364128.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364129.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364130.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364131.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364132.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364133.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364134.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364135.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364136.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364137.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364138.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364139.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364140.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"light","364141.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties(name+"charm","364100.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364101.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364102.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364103.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364104.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364105.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364106.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364107.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364108.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364109.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364110.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364111.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364112.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364113.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364114.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364115.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364117.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364118.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364119.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364120.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364121.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364122.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364123.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364124.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364125.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364126.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364127.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364128.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364129.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364130.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364131.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364132.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364133.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364134.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364135.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364136.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364137.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364138.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364139.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364140.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"charm","364141.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties(name+"beauty","364100.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364101.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364102.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364103.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364104.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364105.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364106.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364107.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364108.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364109.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364110.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364111.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364112.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364113.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364114.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364115.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364117.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364118.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364119.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364120.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364121.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364122.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364123.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364124.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364125.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364126.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364127.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364128.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364129.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364130.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364131.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364132.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364133.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364134.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364135.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364136.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364137.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364138.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364139.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364140.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"beauty","364141.", ObjectSystematics , WeightSystematics)]

    return Samples

##______________________________________________________________________
##
def GetSingleTopSamples( useWeightSyst=False, useObjectSyst=False, name = "Singletop",style="simple", SingletopSystSamples=True):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    if style == "simple":
        Samples     += [getSampleUncertainties(name,"410644.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410645.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410646.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410647.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410658.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410659.", ObjectSystematics , WeightSystematics)]
    else:
        Samples     += [getSampleUncertainties(name+"tchan" ,"410658.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchan" ,"410659.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"Wtprod","410646.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"Wtprod","410647.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schan" ,"410644.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schan" ,"410645.", ObjectSystematics , WeightSystematics)]
    if SingletopSystSamples:
        #t-channel
        Samples     += [getSampleUncertainties(name+"tchanaMCPy8","412004.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanPowH7","411033.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanPowH7","411032.", ObjectSystematics , WeightSystematics)]
        # Not sure what this was, never seen this sample anywhere 
        #Samples     += [getSampleUncertainties(name+"tchanPowH7","410048.", ObjectSystematics , WeightSystematics)]
        #Wt-channel
        Samples     += [getSampleUncertainties(name+"WtDiagSub","410654.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtDiagSub","410655.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodPowHer7","411036.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodPowHer7","411037.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodMCPy8","412002.", ObjectSystematics , WeightSystematics)]
        #s-channel
        Samples     += [getSampleUncertainties(name+"schanaMCPy8","412005.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schanPowH7","411034.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schanPowH7","411035.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetDibosonSamples( useWeightSyst=False, useObjectSyst=False, name = "Dibosons"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    #no high-MET samples included
    Samples     += [getSampleUncertainties(name,"363356.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363358.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363359.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363360.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363489.", ObjectSystematics , WeightSystematics)]

    return Samples

##______________________________________________________________________
##
def GetTopEWSamples( useWeightSyst=False, useObjectSyst=False, name = "topEW"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    #missing high MET samples and ttVV.
    Samples     += [getSampleUncertainties(name,"410155.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410218.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410219.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410220.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetTtHSamples( useWeightSyst=False, useObjectSyst=False, name = "ttH"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"346344.",      ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name+"tH","346230.", ObjectSystematics , WeightSystematics)]
    # Alternative samples
    Samples     += [getSampleUncertainties(name+"PowH7",     "346347.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(name+"noShWMCPy8","346444.", [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    return Samples

##______________________________________________________________________
##
def Get4TopsSamples( useWeightSyst=False, useObjectSyst=False, name = "SM4tops"):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"412043.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetFCNCSamples( useWeightSyst=False, useObjectSyst=False ):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    Samples     += [getSampleUncertainties("uHbWm","411232.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("uHbWp","411231.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("cHbWm","411230.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("cHbWp","411229.", ObjectSystematics , WeightSystematics)]
    return Samples

##_____________________________________________________________________
##
def GetHpluslmSamples( useWeightSyst=False, useObjectSyst=False):
    ObjectSystematics = []
    WeightSystematics = []

    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal_Loose",nameUp="",oneSided=True)]
    Samples     =  []
    Samples     += [getSampleUncertainties("Hpluscb_60" , "451316.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_70" , "451317.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_80" , "450005.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_90" , "450006.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_100", "450007.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_110", "450008.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_120", "450009.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_130", "450010.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_140", "450011.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_150", "450012.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hpluscb_160", "450013.", ObjectSystematics , WeightSystematics)]
    return Samples

##_____________________________________________________________________
##
def GetDataSamples( sampleName = "Data", data_type = "TOPQ1" ):
    Samples     =  []
    Samples     += [getSampleUncertainties(sampleName,"data15_13TeV.periodAllYear."+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName,"data16_13TeV.periodAllYear."+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName,"data17_13TeV.periodAllYear."+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName,"data18_13TeV.periodAllYear."+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    return Samples;

##______________________________________________________________________
##
def GetQCDSamples( data_type = "TOPQ1" ):
    Samples     =  []
    Samples     += [getSampleUncertainties("QCDE","QCD.DAOD_"+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties("QCDMU","QCD.DAOD_"+data_type+".",[getSystematics(name="nominal_Loose",nameUp="",oneSided=True)] ,[])]
    return Samples;
